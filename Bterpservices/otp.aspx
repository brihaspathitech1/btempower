﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="otp.aspx.cs" Inherits="Bterpservices.otp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:GridView ID="grdProduct_Details" runat="server" AutoGenerateColumns="false" class="table table-bordered table-striped"  >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
        
           <Columns>
                 <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                <asp:BoundField DataField="otp" HeaderText="OTP" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                
           </Columns>
       </asp:GridView>
        </div>
    </form>
</body>
</html>
