﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace inventory.DataAccessLayer
{
    class Message
    {

        #region default Constructor
        public Message()
        {
        }
        #endregion

        public void sendSingleNotification(string deviceId, Notification noti)
        {

            var jsonData = new
            {
                to = deviceId,
                notification = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendTopicNotification(string topic, Notification noti)
        {

            var jsonData = new
            {
                to = "/topics/" + topic,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };


            /*var jsonData = new
          {
              to = "/topics/" + topic,
              notification = new
              {
                  body = noti.body,
                  title = noti.title

              }
          };*/

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendMultipleNotification(string[] registrationIds, Notification noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }
        public void sendMultiNotification(string[] registrationIds, Notification noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }


        public void OnLogRequest(Object source, EventArgs e)
        {
            //custom logging logic can go here
        }
        public void sendNotification(string[] registrationIds, Notification noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }

        public void PushNotification(string json)
        {
            try
            {


                var applicationID = "AAAARIXWWH4:APA91bEgoIt92AN_r0d7fvKF_QV4HF0QeJ-EOPrCqr0aWWU3bryUY6LTOPk3ZAPQniuDc1ioMz18TmC_SWY8D-NE26OAbHc4DDi456gEnN-k7_4xvvY7C4pmBTZirPMDfNeIe5Qp_3tl";
                var senderId = "294303193214";
                //var applicationID = "AAAA1wYahDs:APA91bGfMgB9RVP5GbiwFkYLY4EBgKMQhjIGzVXd39FvCyH0QBewbQifbXHTbAHYWZjvkux7yAUcpgmC6zkldzza0YuaH6yp3qkfjHVOdSTHeA9_N3SO6s_THp3IH_1OyxBZFcBUzpEE";
                //var senderId = "923520369723";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";


                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }

        }

        public void sendMultiNotifi(string[] registrationIds, Notification noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    id = noti.id,
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotificationtest(json);
        }

        public void PushNotificationtest(string json)
        {
            try
            {


                var applicationID = "AAAAeNa1bd0:APA91bFNOu22_tvwYvl81x2OZ4ABxvcovZjO3cZC6IB9s-krkRWYIvKGjo0TIVgmZLRoONa0O-4jzPr-dzNDhyYumZp2mrYaR1fe_lWoca95yWQ7O0dm7qAej6I3Zi5dBK6RomMSfzNv";
                var senderId = "518998289885";
                //var applicationID = "AAAA1wYahDs:APA91bGfMgB9RVP5GbiwFkYLY4EBgKMQhjIGzVXd39FvCyH0QBewbQifbXHTbAHYWZjvkux7yAUcpgmC6zkldzza0YuaH6yp3qkfjHVOdSTHeA9_N3SO6s_THp3IH_1OyxBZFcBUzpEE";
                //var senderId = "923520369723";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";


                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }

        }
    }
    
    class Notification
    {
        public int id { get; set; }

        public string title { get; set; }

        public string body { get; set; }

        public string image { get; set; }

        public int code { get; set; }
    }
}
