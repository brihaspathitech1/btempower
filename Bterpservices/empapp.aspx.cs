﻿using inventory.DataAccessLayer;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.InventoryHelper;
using inventory.webservices.v1.include;
using inventory.webservices.v1.model;

namespace Bterpservices
{
    public partial class empapp : System.Web.UI.Page
    {
        string qty;
        string rate1;
        string Label10;
        string description1;
        string Label15;
        string description2;
        string rate2;

        string Label12;
        string description4;
        string rate4;

        string Label13;
        string description5;
        string rate5;

        string Label14;
        string description6;
        string rate6;

        string Label16;
        string description7;
        string rate7;


        string amount;
        string amount1;
        string amount2;
        string amount3;
        string amount4;

        string amount5;
        string amount6;
        string amount7;


        string sgst9;
        string cgst9;
        string sgst14;
        string cgst14;

        string subtotal;
        string FixedAomount;
        string rate;
        string i;

        string qty1;
        string qty2;
        string qty3;
        string qty4;
        string qty5;
        string qty6;
        string qty7;

        // gst calculation

        //cameraamount gst18%


        double cameraamountgst18;

        // cameradvr8channel gst18%
        double ccameradvr8channelgst18;

        // two tb gst 18%
        double twotbgst18;

        //installationcharge gst 18%
        double installationcharge18;

        // total gst18% amount
        double totalgst18amount;

        // calculating sgst9% 
        double sgst9percentageamount;
        //calculating cgst 9%
        double cgst9percentageamount;

        // sgst9 = sgst9percentageamount.ToString();
        //  cgst9= cgst9percentageamount.ToString();

        //  gst28% calculation

        double poweradaptergst28amount;
        double BNC28amount;
        double powerconnectorsgst28amount;
        double threeplusonegst28amount;

        //total gst28% amount 

        double totalgst28amount;

        //calculating sgst14%
        double sgst14percentageamount;
        //calculating cgst14%
        double cgst14percentageamount;


        string exception4portpoe;
        string exception4portpoedesc;
        string exception4portpoerate;
        string qtyforexception4port;
        string exception4portpoeamount;


        string qtyforexception8port;
        string exception8portpoeamount;
        string exception8portpoerate;

        string exception8portpoe;
        string exception8portpoedesc;


        string Label11;
        string description3;
        string rate3;


        #region Navigator
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1200000;
            timer.Elapsed += timer_Elapsed;
            timer.Start();

            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "login":
                            login();
                            break;
                        case "profileimage":
                            profileimage();
                            break;
                        case "forgot_password":
                            forgot_password();
                            break;

                        case "otp_verification":
                            otp_verification();
                            break;

                        case "create_password":
                            create_password();
                            break;

                        case "monthreport":
                            monthreport();
                            break;

                        case "daily":
                            daily();
                            break;

                        case "twodates":
                            twodates();
                            break;

                        case "leaves":
                            leaves();
                            break;

                        case "leavedetails":
                            leavedetails();
                            break;

                        case "paylist":
                            paylist();
                            break;

                        case "payslip":
                            payslip();
                            break;

                        case "contacts":
                            contacts();
                            break;

                        case "attendance_status":
                            attendance_status();
                            break;

                        case "attendance_req":
                            attendance_req();
                            break;
                        case "outpunchreq":
                            outpunch_req();
                            break;
                        case "outpunchstatus":
                            outpunch_status();
                            break;



                        case "request_data":
                            request_data();
                            break;

                        case "mark_attendance":
                            mark_attendance();
                            break;
                        case "mark_attendance1":
                            mark_attendance1();
                            break;

                        case "list_data":
                            list_data();
                            break;

                        case "req_price":
                            req_price();
                            break;

                        case "quotation":
                            quotation();
                            break;
                        case "sendmail":
                            sendmail();
                            break;

                        case "pdf":
                            pdf();
                            break;
                        case "update_token":
                            token();
                            break;

                        case "push":
                            push();
                            break;
                        case "broadcast":
                            broadcast();
                            break;
                        case "apply_leave":
                            applyleave();
                            break;

                        case "leaveslist":
                            leaveslist();
                            break;
                        case "balanceleaves":
                            balanceleaves();
                            break;
                        case "leave_request":
                            leave_request();
                            break;
                        case "mark_leave":
                            updateleavestatus();
                            break;
                        case "mark_leave1":
                            updateleavestatusreject();
                            break;
                        case "Approved_leave":
                            Approved_leave();
                            break;
                        case "Employee_id":
                            Employee_id();
                            break;
                        case "pendingcount":
                            pendingcount();
                            break;
                        case "permission":
                            permission();
                            break;
                        case "permissiondisplay":
                            permissiondisplay();
                            break;
                        case "permissionaprove":
                            permissionaprove();
                            break;
                        case "suggestiondisplay":
                            suggestiondisplay();
                            break;
                        case "displaypermission":
                            displaypermission();
                            break;
                        case "deptbind":
                            dept();
                            break;
                        case "employeenames":
                            employeenames();
                            break;
                        case "suggest":
                            suggestion();
                            break;
                        //interview reg  services

                        case "addcandidates":
                            addcandid();
                            break;
                        case "deptdisplay":
                            deptdisplay();
                            break;
                        case "canddisplay":
                            canddisplay();
                            break;
                        case "statusofcandid":
                            statusofcandid();
                            break;
                        case "selectedlist":
                            Selectedlistofcandid();
                            break;
                        case "Rejectedlist":
                            Rejectedlistofcandid();
                            break;
                        case "holdlist":
                            Holdlistofcandid();
                            break;
                        case "remarks":
                            remarks();
                            break;
                        case "regform":
                            selectedcandidinsert();
                            break;
                        case "statusreason":
                            statusreason();
                            break;

                        //products Information
                        case "productinfo":
                            productinfo();
                            break;

                        //services for technicians to display camera details
                        case "camerabind":
                            hint();
                            break;
                        case "cameralist":
                            cameralist();
                            break;
                        case "technadditems":
                            additems();
                            break;
                        case "displayitems":
                            displayitems();
                            break;
                        case "countnpriceupdate":
                            count();
                            break;
                        case "deleteitem":
                            deleteitem();
                            break;
                        case "totalprice":
                            totalgstprice();
                            break;
                        case "cartcount":
                            cartcount();
                            break;
                        case "deletemultipleitems":
                            deletemultiple();
                            break;
                        case "customerdetails":
                            customerdetails();
                            break;
                        case "candidatesratingdisplay":
                            candidatesrating();
                            break;
                        case "position":
                            position();
                            break;
                        case "searchproducts":
                            search();
                            break;
                        case "order":
                            order();
                            break;
                        case "customerinfo":
                            customerinfo();
                            break;
                        case "orderdisplay":
                            ordersdisplay();
                            break;
                        case "orderitems":
                            orderitemsdisplay();
                            break;

                        case "totalgst":
                            totalgst();
                            break;
                        case "editorderitems":
                            editorderitems();
                            break;
                        case "editcartcount":
                            editcartcount();
                            break;
                        case "addorderitems":
                            addorderitems();
                            break;
                        case "deleteorderitem":
                            deleteorderitem();
                            break;
                        case "updatecount":
                            updatecount();
                            break;
                        case "signature":
                            signature();
                            break;
                        case "RaiseTicket":
                            RaiseTicket();
                            break;
                        case "custservice":
                            custservice();
                            break;

                        case "customerreview":
                            customerreview();
                            break;
                        case "verify_otp":
                            verify_otp();
                            break;
                        case "resend_otp":
                            resend_otp();
                            break;
                        case "emptest":
                            emptest();
                            break;
                        case "events":
                            events();
                            break;
                        case "postvisitor":
                            postvisitor();
                            break;
                        case "postvisitorsdisplay":
                            postvisitorsdisplay();
                            break;
                        case "mobilereg":
                            mobilereg();
                            break;
                        case "postvisitorimg":
                            postvisitorimg();
                            break;
                        case "logout":
                            Logout();
                            break;
                        case "allempleavelist":
                            allempleavelist();
                            break;




                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
                #endregion
            }
        }


        #region webservices

        protected void push()
        {
            DataSet ds = Inventroy_Queries.SelectCommon("select * from Employees where EmployeeId='472'");
            string id = ds.Tables[0].Rows[0]["token"].ToString();

            string deviceId = id;
            Notification obj = new Notification();
            obj.title = "Order Confirmed";
            obj.body = "Your order with reference number  is succesfully placed.Total Order Value ";
            obj.image = "";
            obj.code = 2;


            Message msg = new Message();
            msg.sendTopicNotification("news", obj);
        }
        public void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (DateTime.Now.ToString("h:mm") == "10:15")
            {
                String conns = ConfigurationManager.ConnectionStrings["BtrakConnection"].ConnectionString;
                SqlConnection conn1 = new SqlConnection(conns);
                conn1.Open();
                SqlCommand comm1 = new SqlCommand(" DECLARE	@return_value int EXEC	@return_value = [dbo].[Proc_fullattendance]	@StartDate='2019-07-03'", conn1);
                comm1.ExecuteNonQuery();
                conn1.Close();


                string employeename, mobile;
                String con1 = ConfigurationManager.ConnectionStrings["BtrakConnection"].ConnectionString;
                SqlConnection con = new SqlConnection(con1);
                SqlCommand cmd1 = new SqlCommand("select e.EmployeName,e.CellNo from AttendanceLogsID1111 a inner join Employees e on e.EmployeeId=a.EmployeeID where a.IsAbsent='P(NOP)' and e.EmployeeId in(684,697)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataSet ds = new DataSet();
                da.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    employeename = ds.Tables[0].Rows[i]["EmployeName"].ToString();
                    mobile = ds.Tables[0].Rows[i]["CellNo"].ToString();

                    WebRequest request = null;
                    HttpWebResponse response = null;
                    String userid = "BTRAK";
                    String passwd = "841090";
                    string smstext = "Dear " + employeename + ", Today you didn't set out punch";
                    String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
                    request = WebRequest.Create(url);
                    response = (HttpWebResponse)request.GetResponse();
                }

            }
            else
            {

            }


        }
        protected void Employee_id()
        {
            string empid = Request.Params["employee_id"].ToString().Trim();
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"employeeId\":" + empid + "}");
        }
        protected void emptest()
        {
            string name = "Mahesh";
            string name1 = "Veera";
            string name2 = "Aravind";
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"Mahesh\",\"name\":\"Veera\",\"name\":\"Aravind\"}");

        }

        protected void login()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string password = Request.Params["password"].ToString().Trim();


            DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,convert(varchar,cast(a.DOJ as datetime),107) as DOJ,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department,c.password,C.OTP  from Employees a inner join Department b on a.DeptId=b.DeptId inner join register c on cast(a.EmployeeId as varchar) =c.name where a.EmployeeId='" + employeeid + "' and c.password='" + password + "' and status='1'");
            //SqlCommand cmd = new SqlCommand("select a.EmployeeId,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department,c.password  from Employees a inner join Department b on a.DeptId=b.DeptId inner join register c on a.CompanyEmail=c.Admin where a.EmployeeId='"+ employeeid + "'", cn);

            //DataSet ds = DataQueries.SelectCommon("select name,phone,email,Desigantion,department from Employees where EmployeeId='" + employeeid + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["EmployeName"].ToString().Trim();
                string phone = ds.Tables[0].Rows[0]["cellno"].ToString().Trim();
                string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                string department = ds.Tables[0].Rows[0]["department"].ToString().Trim();
                string designation = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string doj = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                string dept_id = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"phone\":\"" + phone + "\",\"email\":\"" + email + "\",\"department\":\"" + department + "\",\"designation\":\"" + designation + "\",\"doj\":\"" + doj + "\",\"deptid\":" + dept_id + "}");

            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"Invalid UserName Or Password\"}");
            }
        }

        protected void token()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            string token = Request.Params["token"].ToString();
            string tk = string.Empty;
            string tk1 = string.Empty;
            Inventroy_Queries.UpdateCommon("insert into tokenstbl (empid,token) values ('" + employee_id + "','" + token + "')");
            DataSet ds = Inventroy_Queries.SelectCommon("select token,token1 from employees where employeeid='" + employee_id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                tk = ds.Tables[0].Rows[0]["token"].ToString();
                tk1 = ds.Tables[0].Rows[0]["token1"].ToString();
            }
            if (tk == "")
            {
                Inventroy_Queries.UpdateCommon("update Employees set token='" + token + "' where EmployeeId='" + employee_id + "'");
            }
            else if (tk == token)
            {

            }
            else if (tk1 == "")

            {
                Inventroy_Queries.UpdateCommon("update Employees set token1='" + token + "' where EmployeeId='" + employee_id + "'");
            }
            else if (tk1 != "")
            {
                Inventroy_Queries.UpdateCommon("update Employees set token1='" + token + "' where EmployeeId='" + employee_id + "'");
            }
            else
            {
                Inventroy_Queries.UpdateCommon("update Employees set token1='" + token + "' where EmployeeId='" + employee_id + "'");
            }

            Response.Write("{\"error\": false ,\"message\":\"success\"}");
        }

        protected void contacts()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            // DataSet ds = DataQueries.SelectCommon(" select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'");
            SqlCommand cmd = new SqlCommand("select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }

        protected void profileimage()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select [EmpImage] from employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                byte[] imgBytes = (byte[])ds.Tables[0].Rows[0]["EmpImage"];

                // If you want convert to a bitmap file
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(imgBytes);
                string imgString = Convert.ToBase64String(imgBytes);
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"Photo\":\"" + imgString + "\" }");

            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"No Image Found\"}");
            }
        }

        protected void forgot_password()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department,c.password,C.OTP  from Employees a inner join Department b on a.DeptId=b.DeptId inner join register c on a.CompanyEmail=c.Admin where a.EmployeeId='" + employeeid + "'");

            if (ds.Tables[0].Rows.Count != 0)
            {
                string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                Random random = new Random();
                int OTP = random.Next(100000, 999999);
                DataQueries.InsertCommon("update register set otp='" + OTP + "' where name='" + employeeid + "'");
                sendmail(email, "Your OTP To set password is '" + OTP + "' ", "OTP");
                Response.Write("{\"error\": false,\"message\":\"Success\",\"email\":\"" + email + "\"}");

            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Invalid_Email\"}");

            }
        }

        protected void otp_verification()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string otp = Request.Params["otp"].ToString().Trim();


            DataSet ds = DataQueries.SelectCommon("select  otp from register where Name='" + employeeid + "'");

            string otpdb = ds.Tables[0].Rows[0]["otp"].ToString().Trim();
            if (otp == otpdb)
            {

                Response.Write("{\"error\": false,\"message\":\"Success\"}");
            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Invalid OTP\"}");
            }

        }

        protected void create_password()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string new_password = Request.Params["new_password"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select name,designation  from register where name='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataQueries.InsertCommon("update register set password='" + new_password + "' where name='" + employeeid + "'");
                Response.Write("{\"error\": false,\"message\":\"Success\"}");

            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Failed To Update\"}");
            }


        }

        protected void monthreport()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_MonthlydatesAndroid", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false,\"message\":\"Success\",\"data\":" + JSONresult + "}");
            }


            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }



        }

        protected void daily()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Dailydates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);

                if (dt.Rows.Count > 0)
                {

                    string Shift = dt.Rows[0]["Shift"].ToString();
                    string InTime = dt.Rows[0]["In Time"].ToString();
                    string Outtime = dt.Rows[0]["Out Time"].ToString();
                    string Duration = dt.Rows[0]["Duration"].ToString();
                    string ot = dt.Rows[0]["OT"].ToString();
                    string status = dt.Rows[0]["Status"].ToString();
                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"Shift\":\"" + Shift + "\",\"InTime\":\"" + InTime + "\",\"OutTime\":\"" + Outtime + "\",\"Duration\":\"" + Duration + "\",\"OT\":\"" + ot + "\",\"Status\":\"" + status + "\"}");
                }
                else
                {
                    Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
                }

            }
        }

        protected void twodates()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string StartDate = Request.Params["StartDate"].ToString().Trim();
            string EndDate = Request.Params["EndDate"].ToString().Trim();
            double empid = double.Parse(employeeid);
            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Twodates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("TwoDatesReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }
        }


        protected void permission()
        {
            string empid = Request.Params["employee_id"].ToString();
            string text = Request.Params["permissiontext"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataQueries.InsertCommon(" insert into permission(date,ptext,empid) values('" + date + "','" + text.Replace("'", "''") + "','" + empid + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            EmployeeOperation operation = new EmployeeOperation();
            int empID = Convert.ToInt32(empid);
            EmployeeItem employee1 = operation.getEmployee(empID);
            Notification notification = new Notification();
            if (empid == "502")
            {
                supp_token1(notification);
                supp_token111(notification);

            }

            else
            {

                EmployeeItem employee = operation.getEmployee(empID);
                notification.title = employee.name + "Requested for Permission";
                notification.body = text;
                notification.code = 13;
                emp_tokens("502", notification);
                emp_tokensss("502", notification);
            }



        }
        private void supp_token(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='502' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();

            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }

        private void supp_tok(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='502' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();

            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        private void supp_token1(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='1' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        private void supp_token111(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='1' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        protected void permissiondisplay()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd ");
            string empid = Request.Params["empid"].ToString();
            if (empid == "502")
            {
                DataSet ds = DataQueries.SelectCommon("select p.ptext,p.date,e.employename,e.employeeid,d.department from permission p inner join employees e on p.empid=e.Employeeid inner join department d on e.deptid=d.deptid where  CONVERT(VARCHAR(10), date, 111)='" + date + "' and  e.employeeid != '502'");
                DataTable table = ds.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }
            else
            {
                DataSet ds = DataQueries.SelectCommon("select p.ptext,p.date,e.employename,e.employeeid,d.department from permission p inner join employees e on p.empid=e.Employeeid inner join department d on e.deptid=d.deptid where  CONVERT(VARCHAR(10), date, 111)='" + date + "' and e.employeeid = '502'");
                DataTable table = ds.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }




        }
        protected void permissionaprove()
        {
            string empid = Request.Params["empid"].ToString();
            string text = Request.Params["status"].ToString();
            DataQueries.UpdateCommon("update permission set status ='" + text + "' where empid='" + empid + "'");

            EmployeeOperation operation = new EmployeeOperation();

            Notification notification1 = new Notification();

            notification1.title = "Permission Approved";
            notification1.body = text;
            notification1.code = 13;
            emp_tokens(empid, notification1);
            emp_tokensss(empid, notification1);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }

        protected void suggestiondisplay()
        {
            string dept = Request.Params["dept"].ToString();
            string name = Request.Params["employeename"].ToString();
            string department = string.Empty;
            string empname = string.Empty;

            DataSet ds = DataQueries.SelectCommon("select  dept,name from suggestion order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                department = ds.Tables[0].Rows[0]["dept"].ToString();
                empname = ds.Tables[0].Rows[0]["name"].ToString();
            }
            if (department == "Select Department")
            {
                DataSet dss = DataQueries.SelectCommon("select title,description,cast(date as date) from suggestion where name='" + name + "' order by id desc");
                DataTable table = dss.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }

            else if (empname == "Head of the Department")
            {
                DataSet dss = DataQueries.SelectCommon("select title,description,cast(date as date) from suggestion where dept='" + dept + "' order by id desc");
                DataTable table = dss.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }
            else if (department != "Select Department" && empname != "Head of the Department")
            {
                DataSet dss = DataQueries.SelectCommon("select title,description,cast(date as date) from suggestion where dept='" + dept + "' or name='" + name + "' order by id desc");
                DataTable table = dss.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);

            }

        }


        protected void displaypermission()
        {
            string empid = Request.Params["empid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from permission where empid='" + empid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }



        protected void dept()
        {
            DataSet ds = DataQueries.SelectCommon("select distinct d.department from Department d inner join employees e on d.DeptId=e.DeptId where d.CompanyId=1");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void suggestion()

        {
            string dept = Request.Params["dept"].ToString();
            string name = Request.Params["name"].ToString();
            string title = Request.Params["title"].ToString();
            string description = Request.Params["description"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


            DataQueries.InsertCommon("insert into suggestion(dept,name,title,description,date) values('" + dept + "','" + name + "','" + title + "','" + description + "','" + date + "')");

            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void employeenames()
        {
            DataSet ds = DataQueries.SelectCommon("select EmployeeID,EmployeName from Employees where employeeid in (1,2,123,126,144,339,355,502)");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        protected void addcandid()
        {

            string name = Request.Params["name"].ToString();
            string email = Request.Params["email"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string jobposition = Request.Params["jposition"].ToString();
            string dept = Request.Params["dept"].ToString();
            string exp = Request.Params["experiance"].ToString();
            string relevantexp = Request.Params["relevantexp"].ToString();
            //string previouscompany = Request.Params["previouscompany"].ToString();
            string currentcompany = Request.Params["ccompany"].ToString();
            string cctc = Request.Params["cctc"].ToString();
            string ectc = Request.Params["salaryexp"].ToString();

            string notice = Request.Params["notice"].ToString();
            string callfrom = Request.Params["callfrom"].ToString();
            string referredby = Request.Params["reference"].ToString();
            string address = Request.Params["address"].ToString();
            string image = Request.Params["image"].ToString();
            string base64 = image;

            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/candidatespics/") + name + mobile + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/candidatespics/" + name + mobile + ".jpg";
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataQueries.InsertCommon("insert into Candidates(name,email,mobile,jposition,experiance,relevantexp,ccompany,cctc,salaryexp,notice,callfrom,reference,address,date,visible,dept,hrrating,stestrating,techrating,photo) values('" + name.Replace("'", "''") + "','" + email.Replace("'", "''") + "','" + mobile.Replace("'", "''") + "','" + jobposition.Replace("'", "''") + "','" + exp.Replace("'", "''") + "','" + relevantexp.Replace("'", "''") + "','" + currentcompany.Replace("'", "''") + "','" + cctc.Replace("'", "''") + "','" + ectc.Replace("'", "''") + "','" + notice.Replace("'", "''") + "','" + callfrom.Replace("'", "''") + "','" + referredby.Replace("'", "''") + "','" + address.Replace("'", "''") + "','" + date.Replace("'", "''") + "','1','" + dept.Replace("'", "''") + "','0.0','0.0','0.0','" + path + "')");
            Notification notification1 = new Notification();
            notification1.title = name;
            notification1.body = " has came For a Interview ";
            notification1.code = 13;
            supp_token(notification1);
            supp_tok(notification1);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        protected void canddisplay()
        {

            //DataSet ds = DataQueries.SelectCommon("select id,name,email,mobile,jposition,experiance,relevantexp,previouscompany,ccompany,cctc,salaryexp,notice,callfrom,reference,address,date,education, case when hrrating ='0.0' and stestrating='0.0' and techrating='0.0' then '0' when hrrating!='0.0' and stestrating!='0.0' and  techrating!='0.0'  then '2' else '1'   end rating  from Candidates where visible='1' order by id desc");
            DataSet ds = DataQueries.SelectCommon("select id,name,email,mobile,jposition,experiance,relevantexp,previouscompany,ccompany,cctc,salaryexp,notice,callfrom,reference,address,date,education,photo,statusreason,case when Status ='Selected'  then 'Selected' when Status='Hold'  then 'Hold' when Status='Rejected' then 'Rejected' else 'None'   end rating  from Candidates where visible='1' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);




        }


        protected void statusofcandid()
        {
            string id = Request.Params["id"].ToString();
            string status = Request.Params["status"].ToString();
            string hr = Request.Params["hrtest"].ToString();
            string hrrating = Request.Params["hrrating"].ToString();
            string stest = Request.Params["systemtest"].ToString();
            string srating = Request.Params["stestrating"].ToString();
            string techtest = Request.Params["technicaltest"].ToString();
            string techrating = Request.Params["techrating"].ToString();
            DataQueries.UpdateCommon("update candidates set status='" + status + "',hrtest='" + hr + "',hrrating='" + hrrating + "',systemtest='" + stest + "',stestrating='" + srating + "',technicaltest='" + techtest + "',techrating='" + techrating + "' where id='" + id + "'");
            Response.Write("{\"error\": false,\"message\":\"success\"}");


        }


        protected void deptdisplay()
        {
            DataSet ds = DataQueries.SelectCommon("select * from depts ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"dept\": " + JSONresult + "}";
            Response.Write(result);


        }

        protected void position()
        {
            string dept = Request.Params["dept"].ToString();
            DataSet ds = DataQueries.SelectCommon("select jobposition from jobpositions j inner join depts d on j.deptid=d.id where d.dept='" + dept + "' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"position\": " + JSONresult + "}";
            Response.Write(result);

        }
        protected void Selectedlistofcandid()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name,jposition,mobile,notice,date,salaryexp,ccompany,reference,address,email,experiance,previouscompany,cctc,callfrom,relevantexp from candidates where  visible='1' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);

            Response.Write(JSONresult);


        }
        protected void Rejectedlistofcandid()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name,jposition,mobile,notice,date,salaryexp,ccompany,reference,address,email,experiance,previouscompany,cctc,callfrom,relevantexp from candidates where status='Rejected' and visible='1' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void Holdlistofcandid()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name,jposition,mobile,notice,date,salaryexp,ccompany,reference,address,email,experiance,previouscompany,cctc,callfrom,relevantexp from candidates where status='Hold' and visible='1'  order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void selectedcandidinsert()
        {

            string name = Request.Params["Employename"].ToString();
            string email = Request.Params["email"].ToString();
            string cellno = Request.Params["cellno"].ToString();
            string salary = Request.Params["salary"].ToString();
            string education = Request.Params["education"].ToString();
            string yearofcompletion = Request.Params["yearofcompletion"].ToString();
            string presentadd = Request.Params["presentadd"].ToString();
            string permanentadd = Request.Params["permanentadd"].ToString();
            string BloodGroup = Request.Params["BloodGroup"].ToString();
            string DOB = Request.Params["DOB"].ToString();
            string maritalstatus = Request.Params["maritalstatus"].ToString();
            string workexp = Request.Params["workexp"].ToString();
            string previouscompany = Request.Params["previouscompany"].ToString();
            string Bankaccountno = Request.Params["Bankaccountno"].ToString();
            string Bankname = Request.Params["Bankname"].ToString();
            string IFSC = Request.Params["IFSC"].ToString();
            string emergencycntctname = Request.Params["emergencycntctname"].ToString();
            string emergencycontactno = Request.Params["emergencycontactno"].ToString();
            string relation = Request.Params["relation"].ToString();
            string address = Request.Params["address"].ToString();
            //string Photo = Request.Params["Photo"].ToString().Trim();
            //string base64 = Photo;
            //byte[] bytes1 = Convert.FromBase64String(base64);
            //File.WriteAllBytes(Server.MapPath("~/candidatespics/") + name + ".jpg", bytes1); //write to a temp location.
            //string path = "http://emp.bterp.in/candidatespics/" + name + ".jpg";
            DataQueries.InsertCommon1("insert into selectedcandidlist(Employename,email,cellno,salary,education,yearofcompletion,presentadd,permanentadd,BloodGroup,DOB,maritalstatus,workexp,previouscompany,Bankaccountno,Bankname,IFSC,emergencycntctname,emergencycontactno,relation,address) values('" + name.Replace("'", "''") + "','" + email.Replace("'", "''") + "','" + cellno.Replace("'", "''") + "','" + salary.Replace("'", "''") + "','" + education.Replace("'", "''") + "','" + yearofcompletion.Replace("'", "''") + "','" + presentadd.Replace("'", "''") + "','" + permanentadd.Replace("'", "''") + "','" + BloodGroup.Replace("'", "''") + "','" + DOB.Replace("'", "''") + "','" + maritalstatus.Replace("'", "''") + "','" + workexp.Replace("'", "''") + "','" + previouscompany.Replace("'", "''") + "','" + Bankaccountno.Replace("'", "''") + "','" + Bankname.Replace("'", "''") + "','" + IFSC.Replace("'", "''") + "','" + emergencycntctname.Replace("'", "''") + "','" + emergencycontactno.Replace("'", "''") + "','" + relation.Replace("'", "''") + "','" + address.Replace("'", "''") + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }

        protected void remarks()
        {
            //string hr = Request.Params["hrtest"].ToString();
            //string hrrating = Request.Params["hrrating"].ToString();
            //string stest = Request.Params["systemtest"].ToString();
            //string srating = Request.Params["stestrating"].ToString();
            //string techtest = Request.Params["technicaltest"].ToString();
            //string techrating = Request.Params["techrating"].ToString();
            //string id = Request.Params["id"].ToString();
            //DataQueries.UpdateCommon("update candidates set hrtest='" + hr + "',hrrating='" + hrrating + "',systemtest='" + stest + "',stestrating='" + srating + "',technicaltest='" + techtest + "',techrating='" + techrating + "' where id='" + id + "'");
            //Response.Write("{\"error\": false,\"message\":\"success\"}");

        }

        protected void candidatesrating()
        {
            string id = Request.Params["id"].ToString();
            string hrtest = string.Empty;
            string hrrating = string.Empty;
            string stestrating = string.Empty;
            string systemtest = string.Empty;
            string technicaltest = string.Empty;
            string techrating = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select hrtest,hrrating,stestrating,systemtest,technicaltest,techrating from candidates where id='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                hrtest = ds.Tables[0].Rows[0]["hrtest"].ToString().Trim();
                hrrating = ds.Tables[0].Rows[0]["hrrating"].ToString().Trim();
                stestrating = ds.Tables[0].Rows[0]["stestrating"].ToString().Trim();
                systemtest = ds.Tables[0].Rows[0]["systemtest"].ToString().Trim();
                technicaltest = ds.Tables[0].Rows[0]["technicaltest"].ToString().Trim();
                techrating = ds.Tables[0].Rows[0]["techrating"].ToString().Trim();



            }
            if (hrrating != "" && stestrating == "" && techrating == "")
            {

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"hrtest\":\"" + hrtest + "\",\"hrrating\":\"" + hrrating + "\",\"stestrating\":\"0.0\",\"systemtest\":\"" + systemtest + "\",\"technicaltest\":\"" + technicaltest + "\",\"techrating\":\"0.0\"}");
            }
            else if (stestrating != "" && hrrating == "" && techrating == "")
            {
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"hrtest\":\"" + hrtest + "\",\"hrrating\":\"0.0\",\"stestrating\":\"" + stestrating + "\",\"systemtest\":\"" + systemtest + "\",\"technicaltest\":\"" + technicaltest + "\",\"techrating\":\"0.0\"}");
            }
            else if (techrating != "" && hrrating == "" && stestrating == "")

            {
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"hrtest\":\"" + hrtest + "\",\"hrrating\":\"0.0\",\"stestrating\":\"0.0\",\"systemtest\":\"" + systemtest + "\",\"technicaltest\":\"" + technicaltest + "\",\"techrating\":\"" + techrating + "\"}");
            }
            else if (techrating == "" && hrrating == "" && stestrating == "")
            {
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"hrtest\":\"" + hrtest + "\",\"hrrating\":\"0.0\",\"stestrating\":\"0.0\",\"systemtest\":\"" + systemtest + "\",\"technicaltest\":\"" + technicaltest + "\",\"techrating\":\"0.0\"}");
            }


            else
            {
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"hrtest\":\"" + hrtest + "\",\"hrrating\":\"" + hrrating + "\",\"stestrating\":\"" + stestrating + "\",\"systemtest\":\"" + systemtest + "\",\"technicaltest\":\"" + technicaltest + "\",\"techrating\":\"" + techrating + "\"}");
            }




        }
        protected void statusreason()
        {
            string mobile = Request.Params["mobile"].ToString();
            string reason = Request.Params["reason"].ToString();
            DataQueries.UpdateCommon("update candidates set statusreason='" + reason + "' where mobile='" + mobile + "'");
            Response.Write("{\"error\": false,\"message\":\"success\"}");


        }


        //products Information
        protected void productinfo()
        {
            string pname = Request.Params["productname"].ToString();
            string descp = Request.Params["description"].ToString();
            string qty = Request.Params["quantity"].ToString();
            string price = Request.Params["price"].ToString();
            DataQueries.InsertCommon("insert into productsinformation(productname,description,quantity,price) values('" + pname + "','" + descp + "','" + qty + "','" + price + "')");
            Response.Write("{\"error\": false,\"message\":\"success\"}");
        }
        protected void leaves()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            SqlCommand cmd = new SqlCommand("Proc_LeavesAndroid", con);
            SqlCommand cmd1 = new SqlCommand("select AppliedDate,FromDate,ToDate,Duration,ReasontoApply,Status,StatusDate,cl,lop from attendance.LeavesStatus where employeedid='" + employeeid + "'", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@employeedid", employeeid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable("Leaves");
            DataTable dt1 = new DataTable("LeaveRecords");
            da.Fill(dt);
            da1.Fill(dt1);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            string JSONresult1 = DataTableToJsonWithJsonNet(dt1);
            if (dt.Rows.Count > 0)
            {
                int BalanceLeaves = Convert.ToInt32(dt.Rows[0]["balanceleaves"].ToString());
                int Used = Convert.ToInt32(dt.Rows[0]["used"].ToString());
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"Remaining\": " + BalanceLeaves + ",\"Used\":" + Used + ",\"History\": " + JSONresult1 + "}");
            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }

        }

        protected void leavedetails()
        {
            string employee_id = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            SqlCommand cmd = new SqlCommand("select applieddate,fromdate,todate,duration,status,reasontoapply from attendance.LeavesStatus where employeedid='" + employee_id + "'and status='approved'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }

        protected void applyleave()
        {
            string ErrorMessage = "";
            string noofdays = "";
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string leave_type = Request.Params["leave_type"].ToString();
            string begin_date = Request.Params["begin_date"].ToString();
            string end_date = Request.Params["end_date"].ToString();
            string reason = Request.Params["reason"].ToString();
            string employee_id = Request.Params["employee_id"].ToString();
            int empID = Convert.ToInt32(employee_id);

            string status;
            string depthead_id;
            DataSet ds1 = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + employee_id + "'");
            DataSet ds2 = DataQueries.SelectCommon("Select CompanyId,DeptId from Employees where EmployeeId='" + employee_id + "'");


            if (ds2.Tables[0].Rows.Count > 0)
            {
                string DeptId = ds2.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string CompanyId = ds2.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                ViewState["DeptId"] = DeptId;
                ViewState["CompanyId"] = CompanyId;

            }

            //VP Sir
            if (ViewState["DeptId"].ToString() == "15" || ViewState["DeptId"].ToString() == "16" || ViewState["DeptId"].ToString() == "17" || ViewState["DeptId"].ToString() == "18" || ViewState["CompanyId"].ToString() == "10" || ViewState["CompanyId"].ToString() == "14" || ViewState["CompanyId"].ToString() == "36" || ViewState["CompanyId"].ToString() == "39")
            {

                status = "OnManagerDesk";
                depthead_id = "123";
            }

            //Hyma Madam
            else if (ViewState["DeptId"].ToString() == "14" || ViewState["DeptId"].ToString() == "56" || ViewState["DeptId"].ToString() == "51")
            {

                //status = "OnManagerDesk";
                //depthead_id = "102";
                status = "OnHRDesk";
                depthead_id = "502";
            }

            //Murali Sir
            //339 prasad sir
            else if (employee_id == "339" || ViewState["DeptId"].ToString() == "20" || ViewState["DeptId"].ToString() == "34" || ViewState["DeptId"].ToString() == "46" || ViewState["DeptId"].ToString() == "39")
            {

                status = "OnManagerDesk";
                depthead_id = "144";
            }
            //Murali Sir
            else if (ViewState["DeptId"].ToString() == "19")
            {

                status = "OnManagerDesk";
                depthead_id = "1";
            }


            else if (ds1.Tables[0].Rows.Count != 0)
            {
                status = "OnHRDesk";
                depthead_id = "502";
            }
            else
            {
                status = "OnHRDesk";
                depthead_id = "502";
            }

            DateTime FromYear = Convert.ToDateTime(begin_date);
            DateTime staringdate = FromYear;
            DateTime ToYear = Convert.ToDateTime(end_date);
            DateTime endingdate = ToYear;
            endingdate = endingdate.AddDays(1);
            TimeSpan objTimeSpan = ToYear - FromYear;
            int duration = 0;

            double Days = Convert.ToDouble(objTimeSpan.TotalDays) + 1;

            if (Days > 0)
            {
                if (FromYear.Year != ToYear.Year)
                {
                    ErrorMessage += "Staring and Ending Date of leave Should have same Year..!";
                    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");
                }
                else if ((FromYear < System.DateTime.Now || ToYear < System.DateTime.Now) && leave_type != "Sick leave")
                {


                    ErrorMessage += "Wrong selection of Leave Date..!";
                    Response.Write("{\"error\": true,\"status\":\"" + ErrorMessage + "\"}");

                }
                else
                {

                    DataSet Holidays = DataQueries.SelectCommon("select HolidayDate from Holidays where HolidayDate between '" + begin_date + "' and '" + end_date + "'");
                    if (staringdate != endingdate)
                    {
                        if (staringdate.DayOfWeek != DayOfWeek.Sunday)
                        {
                            if (Holidays.Tables[0].Rows.Count == 0)
                            { duration++; }
                            else
                            {
                                for (int i = 0; i < Holidays.Tables[0].Rows.Count; i++)
                                {
                                    string s1 = DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()).ToString();
                                    if (DateTime.Parse(Holidays.Tables[0].Rows[i][0].ToString()) != staringdate)
                                    {
                                        duration++;
                                    }
                                }
                            }
                        }
                        staringdate = staringdate.AddDays(1);
                    }
                    noofdays = Convert.ToString(duration);
                    Double count = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year('" + begin_date + "')");
                    count = 12 - count;

                    if (Convert.ToInt16(noofdays) > count)
                    {
                        string str;
                        str = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate";
                        str += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                        str += " where l.EmployeedId='" + employee_id + "' order by l.LeaveId";
                        DataSet dsro1 = DataQueries.SelectCommon(str);
                        if (dsro1.Tables[0].Rows.Count != 0)
                        {
                            Response.Write("{\"error\": true,\"status\":\"Sorry..!You can Apply Only 12 Casual Leave Per Year..!\"}");
                        }
                        return;
                    }

                    string s;
                    s = "select l.LeaveId,e.EmployeName,l.Status,l.AppliedDate,l.CL,l.LOP,l.FromDate,l.ToDate,l.Duration,l.BalanceLeaves,l.ReasontoApply,l.StatusDate";
                    s += " from Employees e inner join DeptMaster d on d.DeptId=e.DeptId inner join LeavesStatus l on l.EmployeedID = e.EmployeeId";
                    s += " where l.EmployeedId='" + employee_id + "' and (l.Status='OnHRDesk' or l.Status='OnManagerDesk') and Month(l.FromDate)=Month(Getdate())";

                    DataSet dsro = DataQueries.SelectCommon(s);
                    if (dsro.Tables[0].Rows.Count != 0)
                    {
                        Response.Write("{\"error\": true,\"status\":\"Already Pending Leave,You can't Apply..!\"}");
                        //return;
                    }
                    else
                    {
                        int LOP, CL;
                        int apply = Convert.ToInt16(noofdays);
                        Double AlreadyApplied = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year('" + begin_date + "')");
                        int month = DateTime.Parse(end_date).Month;
                        int canapply = month - (int)AlreadyApplied;
                        if (apply > canapply)
                        {
                            LOP = apply - canapply;
                            CL = canapply;
                        }
                        else
                        {
                            CL = apply;
                            LOP = 0;
                        }
                        DataQueries.InsertCommon("insert into LeavesStatus values('" + employee_id + "','" + date + "','" + begin_date + "','" + end_date + "','" + noofdays + "','" + count.ToString() + "','" + reason + "','" + status + "','','" + date + "','" + CL + "','" + LOP + "','','" + depthead_id + "')");
                        Response.Write("{\"error\": false,\"status\":\"Leave applied successfully\"}");


                        EmployeeOperation operation = new EmployeeOperation();
                        EmployeeItem employee = operation.getEmployee(empID);
                        Notification notification = new Notification();
                        notification.title = employee.name + " leave request is on Your desk";
                        notification.body = reason;
                        notification.code = 13;
                        emp_tokens(depthead_id, notification);
                        emp_tokensss(depthead_id, notification);

                    }
                }
            }
            else
            {
                ErrorMessage += "Wrong selection of Leave Ending Date..!";
                Response.Write("{\"error\": true,\"status\":" + ErrorMessage + "}");
            }
        }




        protected void leaveslist()
        {
            string employee_id = Request.Params["employee_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select ManagerId from DeptMaster where ManagerId='" + employee_id + "'");
            if (ds.Tables[0].Rows.Count != 0)
            {
                string s;
                if (ds.Tables[0].Rows[0][0].ToString() == "502")
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='OnHRDesk'";
                }
                else if (ds.Tables[0].Rows[0][0].ToString() == "1")
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='OnManagerDesk' and e.employeeId='502'";
                }


                else if (ds.Tables[0].Rows[0][0].ToString() == "102")
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='OnManagerDesk' and l.depthead_id='102'";
                }
                else if (ds.Tables[0].Rows[0][0].ToString() == "144")
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='OnManagerDesk' and l.depthead_id='144'";
                }
                else if (ds.Tables[0].Rows[0][0].ToString() == "123")
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='OnManagerDesk' and l.depthead_id='123'";
                }

                else
                {
                    s = "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid";
                    s += " from Employees e inner join DeptMaster d on d.deptId=e.deptId";
                    s += " inner join LeavesStatus l on l.employeedID = e.employeeId where d.managerId='" + employee_id + "' and l.status='OnManagerDesk'";
                }
                DataSet ds1 = DataQueries.SelectCommon(s);
                DataTable table = ds1.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);

            }
            else
            {
                string s;
                s = "select l.leaveid,e.employename,l.status,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.statusdate as updateon";
                s += " from Employees e inner join LeavesStatus l on l.employeedID = e.employeeId";
                s += " where l.employeedId='" + employee_id + "' order by l.leaveId desc";

                DataSet dsro = DataQueries.SelectCommon(s);
                DataTable table = dsro.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);


            }
        }


        protected void Approved_leave()
        {

            string s;
            s = "with cte as (";
            s += "select e.employename,l.applieddate,l.cl,l.lop,l.fromdate,l.todate,l.duration,l.balanceleaves,l.reasontoapply,l.leaveid,l.approvedby from Employees e";
            s += " inner join LeavesStatus l on l.employeedID = e.employeeId where l.status='approved'";
            s += ") select *,(select EmployeName from Employees where EmployeeId=cte.approvedby) as hrid from cte order by leaveid desc";
            DataSet ds1 = DataQueries.SelectCommon(s);
            DataTable table = ds1.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void balanceleaves()
        {
            string employee_id = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            Double count = DataQueries.SelectCount("select sum(CL) from LeavesStatus where EmployeedID='" + employee_id + "' and Status='Approved' and Year(FromDate)=Year(Getdate())");
            count = 12 - count;

            Response.Write("{\"error\": false,\"status\":" + count + "}");

        }

        protected void leave_request()
        {

            string employee_id = Request.Params["employee_id"].ToString();
            string s = string.Empty;
            if (employee_id == "123")
            {
                s = "Select l.EmployeedID as employee_id,e.EmployeName as name,l.LeaveId as leaveId,l.AppliedDate as appliedDate,l.CL as cl,l.LOP as lop,l.FromDate as fromDate,l.ToDate as toDate,l.Duration as duration,l.Status as status,l.ReasontoApply as reason from LeavesStatus l inner join employees e on l.EmployeedID=e.EmployeeId where l.Status='OnManagerDesk' and depthead_id='123'";
            }
            else if (employee_id == "102")
            {
                s = "Select l.EmployeedID as employee_id,e.EmployeName as name,l.LeaveId as leaveId,l.AppliedDate as appliedDate,l.CL as cl,l.LOP as lop,l.FromDate as fromDate,l.ToDate as toDate,l.Duration as duration,l.Status as status,l.ReasontoApply as reason from LeavesStatus l inner join employees e on l.EmployeedID=e.EmployeeId where l.Status='OnManagerDesk' and depthead_id='102'";
            }
            else if (employee_id == "144")
            {
                s = "Select l.EmployeedID as employee_id,e.EmployeName as name,l.LeaveId as leaveId,l.AppliedDate as appliedDate,l.CL as cl,l.LOP as lop,l.FromDate as fromDate,l.ToDate as toDate,l.Duration as duration,l.Status as status,l.ReasontoApply as reason from LeavesStatus l inner join employees e on l.EmployeedID=e.EmployeeId where l.Status='OnManagerDesk' and depthead_id='144'";
            }
            else if (employee_id == "1")
            {
                s = "Select l.EmployeedID as employee_id,e.EmployeName as name,l.LeaveId as leaveId,l.AppliedDate as appliedDate,l.CL as cl,l.LOP as lop,l.FromDate as fromDate,l.ToDate as toDate,l.Duration as duration,l.Status as status,l.ReasontoApply as reason from LeavesStatus l inner join employees e on l.EmployeedID=e.EmployeeId where l.Status='OnManagerDesk' and depthead_id='1'";
            }
            else if (employee_id == "678" || employee_id == "502")
            {
                s = "Select l.EmployeedID as employee_id,e.EmployeName as name,l.LeaveId as leaveId,l.AppliedDate as appliedDate,l.CL as cl,l.LOP as lop,l.FromDate as fromDate,l.ToDate as toDate,l.Duration as duration,l.Status as status,l.ReasontoApply as reason from LeavesStatus l inner join employees e on l.EmployeedID=e.EmployeeId where l.Status='OnHRDesk'";
            }
            DataSet ds1 = DataQueries.SelectCommon(s);
            DataTable table = ds1.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void updateleavestatus()
        {
            string employee_id = Request.Params["hr"].ToString();
            string leave_id = Request.Params["leave_id"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select EmployeedId,ReasontoApply from LeavesStatus where LeaveId='" + leave_id + "'");
            string Empployee = ds.Tables[0].Rows[0]["EmployeedId"].ToString();
            string reason = ds.Tables[0].Rows[0]["ReasontoApply"].ToString();
            int empID = Convert.ToInt32(Empployee);

            //string status = Request.Params["status"].ToString();
            if (employee_id == "123" || employee_id == "102" || employee_id == "144")
            {
                DataQueries.UpdateCommon("Update LeavesStatus set Status='OnHRDesk',approvedby='" + employee_id + "' where LeaveId='" + leave_id + "'");
                Response.Write("{\"error\": false,\"status\":\"updated successfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);

                Notification notification = new Notification();

                notification.title = employee.name + " leave request is on your desk";
                notification.body = reason;
                //notification.image = path;
                notification.code = 13;
                hr_tokens(notification);
                hr_tok(notification);
                Notification notification1 = new Notification();

                notification1.title = employee.name + " leave request is on HR desk";
                notification1.body = reason;
                //notification.image = path;
                notification1.code = 13;
                emp_tokens(Empployee, notification1);
                emp_tokensss(Empployee, notification1);
            }
            else if (employee_id == "502" || employee_id == "1")
            {
                DataQueries.UpdateCommon("Update LeavesStatus set Status='Approved',approvedby='" + employee_id + "' where LeaveId='" + leave_id + "'");
                Response.Write("{\"error\": false,\"status\":\"updated successfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);
                Notification notification1 = new Notification();

                notification1.title = "Leave approved successfully";
                notification1.body = reason;
                notification1.code = 13;
                emp_tokens(Empployee, notification1);
                emp_tokensss(Empployee, notification1);
            }
        }


        protected void updateleavestatusreject()
        {

            string employee_id = Request.Params["hr"].ToString();
            string leave_id = Request.Params["leave_id"].ToString();

            DataSet ds = DataQueries.SelectCommon("Select EmployeedId,ReasontoApply from LeavesStatus where LeaveId='" + leave_id + "'");
            string Empployee = ds.Tables[0].Rows[0]["EmployeedId"].ToString();
            string reason = ds.Tables[0].Rows[0]["ReasontoApply"].ToString();
            int empID = Convert.ToInt32(Empployee);
            if (employee_id == "123" || employee_id == "102" || employee_id == "144")
            {
                DataQueries.UpdateCommon("Update LeavesStatus set Status='Rejected',approvedby='" + employee_id + "' where LeaveId='" + leave_id + "'");
                Response.Write("{\"error\": false,\"status\":\"updated successfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);

                Notification notification = new Notification();

                Notification notification1 = new Notification();

                notification1.title = employee.name + " leave request is Rejected";
                notification1.body = reason;
                //notification.image = path;
                notification1.code = 13;
                emp_tokens(Empployee, notification1);
                emp_tokensss(Empployee, notification1);
            }
            else if (employee_id == "502")
            {
                DataQueries.UpdateCommon("Update LeavesStatus set Status='Rejected',approvedby='" + employee_id + "' where LeaveId='" + leave_id + "'");
                Response.Write("{\"error\": false,\"status\":\"updated successfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);
                Notification notification1 = new Notification();

                notification1.title = "Leave Rejected";
                notification1.body = reason;
                notification1.code = 13;
                emp_tokens(Empployee, notification1);
                emp_tokensss(Empployee, notification1);
            }
        }



        //end of leaves

        //start payroll
        protected void paylist()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select a.Year,a.Month,(cast(b.salary as float)/12) as total,a.ttotal,a.present,a.workingdays,a.Name,b.EmployeeCodeInDevice from Payslip a left outer join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "' order by payslip desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }


        protected void payslip()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string month = Request.Params["month"].ToString().Trim();

            string year = Request.Params["year"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select (cast(Deducation1 as decimal)+cast(latelogindetuction as decimal)) as Deducation1,cast(lopdeduction as decimal) as lopdeduction1,Salary/12 as monthsal,* from Payslip a inner join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "' and month='" + month + "' and year='" + year + "' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string department = ds.Tables[0].Rows[0]["Departrment"].ToString().Trim();
                string designation = ds.Tables[0].Rows[0]["Designation"].ToString().Trim();
                string empnum = ds.Tables[0].Rows[0]["EmpNo"].ToString().Trim();
                string dob = ds.Tables[0].Rows[0]["DOB"].ToString().Trim();
                string doJ = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                string workingdays = ds.Tables[0].Rows[0]["Workingdays"].ToString().Trim();
                string paybledays = ds.Tables[0].Rows[0]["Paybledays"].ToString().Trim();
                string pfno = ds.Tables[0].Rows[0]["PFNo"].ToString().Trim();
                string basic = ds.Tables[0].Rows[0]["Basic"].ToString().Trim();
                string hra = ds.Tables[0].Rows[0]["HRA"].ToString().Trim();
                string da = ds.Tables[0].Rows[0]["DA"].ToString().Trim();
                string pf = ds.Tables[0].Rows[0]["PF"].ToString().Trim();
                string ca = ds.Tables[0].Rows[0]["CA"].ToString().Trim();
                string esi = ds.Tables[0].Rows[0]["ESI"].ToString().Trim();
                string ea = ds.Tables[0].Rows[0]["EA"].ToString().Trim();
                string asa = ds.Tables[0].Rows[0]["ASa"].ToString().Trim();
                string it = ds.Tables[0].Rows[0]["IT"].ToString().Trim();
                string paydayd = ds.Tables[0].Rows[0]["lopdeduction1"].ToString().Trim();
                string total = ds.Tables[0].Rows[0]["monthsal"].ToString().Trim();
                string deducation = ds.Tables[0].Rows[0]["Deducation1"].ToString().Trim();
                string ttotal = ds.Tables[0].Rows[0]["TTotal"].ToString().Trim();
                string amountwords = ds.Tables[0].Rows[0]["Amountwords"].ToString().Trim();
                string latelogin = ds.Tables[0].Rows[0]["latelogin"].ToString().Trim();
                string latelogindeduction = ds.Tables[0].Rows[0]["latelogindetuction"].ToString().Trim();
                double lt = Math.Round(Convert.ToDouble(latelogindeduction));
                string lop = ds.Tables[0].Rows[0]["lop"].ToString().Trim();
                string cl = ds.Tables[0].Rows[0]["cl"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"department\":\"" + name + "\",\"designation\":\"" + designation + "\",\"empnum\":\"" + empnum + "\",\"dob\":\"" + dob + "\",\"doj\":\"" + doJ + "\",\"workingdays\":\"" + workingdays + "\",\"paybledays\":\"" + paybledays + "\",\"pfno\":\"" + pfno + "\",\"basic\":\"" + basic + "\",\"hra\":\"" + hra + "\",\"da\":\"" + da + "\",\"pf\":\"" + pf + "\",\"ca\":\"" + ca + "\",\"esi\":\"" + esi + "\",\"ea\":\"" + ea + "\",\"asa\":\"" + asa + "\",\"it\":\"" + it + "\",\"LoseofPay\":\"" + paydayd + "\",\"total\":\"" + total + "\",\"deducation\":\"" + deducation + "\",\"ttotal\":\"" + ttotal + "\",\"amountwords\":\"" + amountwords + "\",\"latelogin\":\"" + latelogin + "\",\"latelogindetuction\":\"" + lt + "\",\"cl\":\"" + cl + "\",\"lop\":\"" + lop + "\"}");
            }


            else

            {
                Response.Write("{\"error\": True ,\"message\":\"No Records Found\"}");
            }
        }

        protected void attendance_status()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and punchtype='In Punch'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string reason = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                string time = ds.Tables[0].Rows[0]["time"].ToString().Trim();
                string status = ds.Tables[0].Rows[0]["status"].ToString().Trim();
                string latitude = ds.Tables[0].Rows[0]["latitude"].ToString().Trim();
                string longitude = ds.Tables[0].Rows[0]["longitude"].ToString().Trim();
                string image = ds.Tables[0].Rows[0]["image"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"reason\":\"" + reason + "\",\"time\":\"" + time + "\",\"status\":\"" + status + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"image\":\"" + image + "\"}");


            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"No Records Found\"}");
            }
        }

        protected void outpunch_status()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and punchtype='Out Punch'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string reason = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                string time = ds.Tables[0].Rows[0]["time"].ToString().Trim();
                string status = ds.Tables[0].Rows[0]["status"].ToString().Trim();
                string latitude = ds.Tables[0].Rows[0]["latitude"].ToString().Trim();
                string longitude = ds.Tables[0].Rows[0]["longitude"].ToString().Trim();
                string image = ds.Tables[0].Rows[0]["image"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"reason\":\"" + reason + "\",\"time\":\"" + time + "\",\"status\":\"" + status + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"image\":\"" + image + "\"}");


            }
            else
            {

                Response.Write("{\"error\": true ,\"message\":\"No Records Found\"}");
            }
        }

        //protected void attendance_req()
        //{


        //    string employeeid = Request.Params["employee_id"].ToString().Trim();
        //    string reason = Request.Params["reason"].ToString().Trim();
        //    string latitude = Request.Params["latitude"].ToString().Trim();
        //    string longitude = Request.Params["longitude"].ToString().Trim();
        //    string Image = Request.Params["image"].ToString();



        //    string date = System.DateTime.Now.ToString("yyyy/MM/dd");
        //    string date12 = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
        //    string name = employeeid + "_" + date;
        //    string filename = name;
        //    string base64 = Image;
        //    byte[] bytes1 = Convert.FromBase64String(base64);
        //    File.WriteAllBytes(Server.MapPath("~/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
        //    string path = "http://emp.bterp.in/attendanceimages/" + filename + ".jpg";

        //    DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
        //    if (ds.Tables[0].Rows.Count == 1)
        //    {


        //        Response.Write("{\"error\": true ,\"message\":\"Already Request Sent\"}");
        //    }
        //    else
        //    {
        //        DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status,image) values ('" + reason + "','" + System.DateTime.Now.ToString() + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0','" + path.ToString() + "')");
        //        Response.Write("{\"error\": false,\"message\":\"Request Submitted Successsfully\"}");

        //    }


        //    hr_tokens();

        //}

        protected void attendance_req()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string reason = Request.Params["reason"].ToString().Trim();
            string latitude = Request.Params["latitude"].ToString().Trim();
            string longitude = Request.Params["longitude"].ToString().Trim();
            string Image = Request.Params["image"].ToString();

            int empID = Convert.ToInt32(employeeid);

            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            string date12 = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            string name = employeeid + "_" + date;
            string filename = name + "inpunch";
            string base64 = Image;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/attendanceimages/" + filename + ".jpg";

            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "' and punchtype='In Punch'");
            if (ds.Tables[0].Rows.Count == 1)
            {


                Response.Write("{\"error\": true ,\"message\":\"Already Request Sent\"}");
            }

            else
            {
                DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status,image,punchtype) values ('" + reason + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0','" + path.ToString() + "','In Punch')");
                Response.Write("{\"error\": false,\"message\":\"Request Submitted Successsfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);

                Notification notification = new Notification();

                notification.title = employee.name + " send attendance request";
                notification.body = reason;
                notification.image = path;
                notification.code = 13;
                hr_tokens(notification);
                hr_tok(notification);
            }

        }
        protected void outpunch_req()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string reason = Request.Params["reason"].ToString().Trim();
            string latitude = Request.Params["latitude"].ToString().Trim();
            string longitude = Request.Params["longitude"].ToString().Trim();
            string Image = Request.Params["image"].ToString();

            int empID = Convert.ToInt32(employeeid);

            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            string date12 = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            string name = employeeid + "_" + date;
            string filename = name + "outpunch";
            string base64 = Image;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/attendanceimages/" + filename + ".jpg";

            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'  and punchtype='Out Punch'");
            if (ds.Tables[0].Rows.Count == 1)
            {


                Response.Write("{\"error\": true ,\"message\":\"Already Request Sent\"}");
            }



            else
            {
                DataQueries.InsertCommon("insert into RequestAttendance (Reason,Time,Employee_Id,Latitude,Longitude,Markby,status,image,punchtype) values ('" + reason.Replace("'", "''") + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + employeeid + "','" + latitude + "','" + longitude + "','" + employeeid + "','0','" + path.ToString() + "','Out Punch')");
                Response.Write("{\"error\": false,\"message\":\"Request Submitted Successsfully\"}");

                EmployeeOperation operation = new EmployeeOperation();
                EmployeeItem employee = operation.getEmployee(empID);

                Notification notification = new Notification();

                notification.title = employee.name + " send attendance request";
                notification.body = reason;
                notification.image = path;
                notification.code = 13;
                hr_tokens(notification);
                hr_tok(notification);
            }

        }

        protected void request_data()
        {

            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where  convert(nvarchar(10),Time,126)='" + date + "'");
            DataSet ds = DataQueries.SelectCommon("select a.employeeid as employee_id,a.desigantion,a.employename as name,a.deptid as department_id,b.Reason as reason,b.Time as time,b.Latitude as latitude,b.Longitude as longitude,b.Status as status,b.image,c.department from employees a inner join RequestAttendance b on a.employeeid=b.employee_id inner join department c on a.deptid=c.deptid where convert(nvarchar(10),Time,126)='" + date + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        //protected void mark_attendance()
        //{
        //    string employeeid = Request.Params["employee_id"].ToString().Trim();
        //    string hr = Request.Params["hr"].ToString().Trim();
        //    string date = System.DateTime.Now.ToString("yyyy/MM/dd");


        //    string date1 = DateTime.Now.ToString("yyyy-MM-dd");
        //    string aa = " 09:30";

        //    String a = date1 + aa;
        //    DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + a + "','OutDoorEntry')");
        //    DataQueries.InsertCommon("update  RequestAttendance set status='1', markby='" + hr + "' where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
        //    Response.Write("{\"error\": false,\"message\":\"Marked Successsfully\"}");



        //}
        protected void mark_attendance()
        {

            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string hr = Request.Params["hr"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");


            DataSet ds = DataQueries.SelectCommon("Select  substring(cast(cast(time as time) as varchar),1,5) as Time  from RequestAttendance where Employee_Id='" + employeeid + "' and cast(time as date)='" + date + "' and Status='0'");

            string Time = ds.Tables[0].Rows[0]["Time"].ToString();
            string date2 = DateTime.Today.ToString("yyyy-MM-dd") + " " + Time;

            string date1 = DateTime.Now.ToString("yyyy-MM-dd");

            DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + employeeid + "','" + date2 + "','OutDoorEntry')");
            DataQueries.InsertCommon("update  RequestAttendance set status='1', markby='" + hr + "' where Employee_Id='" + employeeid + "' and cast(time as date)='" + date + "'");

            string time = date1 + " 09:41:00";



            if (DateTime.Parse(date2) > DateTime.Parse(time))
            {
                DataSet ds3 = DataQueries.SelectCommon("select * from tblattendance where cast(DateOfTransaction as date)='" + date + "' and userid='" + employeeid + "'");
                if (ds3.Tables[0].Rows.Count == 1)
                {

                    DataSet ds1 = DataQueries.SelectCommon("SELECT EmployeName,CompanyEmail from Employees where EmployeeId='" + employeeid + "'");
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        DataQueries.InsertCommon("EXEC Proc_Moomthlytotalreport @StartDate ='" + date + "',	@Basic = 'Totalreport'");
                        DataSet ds2 = DataQueries.SelectCommon("select isnull(count(EmployeeID),'0') as count from AttendanceLogspayrol where cast(InTime as time)>cast('09:41' as time) and (DetailedStatus!='A' and Weekoff='') and  EmployeeID='" + employeeid + "'");
                        MailMessage mail = new MailMessage();
                        MailAddress SendCC = new MailAddress("hr@brihaspathi.com");
                        // MailAddress SendCC1 = new MailAddress("dotnettrainee1@brihaspathi.com");
                        mail.CC.Add(SendCC);
                        // mail.CC.Add(SendCC1);
                        mail.From = new MailAddress("noreply1@brihaspathi.com", "Late Logins Alert");
                        mail.To.Add("" + ds1.Tables[0].Rows[0]["CompanyEmail"] + "");
                        mail.IsBodyHtml = true;
                        mail.Subject = "Late Logins Alert";
                        string count = ds2.Tables[0].Rows[0]["count"].ToString();
                        string msg = string.Empty;
                        if (count == "1")
                        {

                            msg = "Dear Ms/Mr " + ds1.Tables[0].Rows[0]["EmployeName"] + ",<br/><br/>Alert about late login<br/>Today your Late Time :" + date2 + "<br/>This Month your Late logins count:" + count + ",<br/>You are advised to come on time<br/><br/>Regards,<br/><b>HR.</b>";
                        }
                        else if (count == "2")
                        {
                            msg = "Dear Ms/Mr " + ds1.Tables[0].Rows[0]["EmployeName"] + ",<br/><br/>Again reminding about late logins<br/>Today your Late Time :" + date2 + "<br/>This Month your Late logins count:" + count + ",<br/>You are advised to come on time<br/><br/>Regards,<br/><b>HR.</b>";
                        }
                        else if (count == "3")
                        {
                            msg = "Dear Ms/Mr " + ds1.Tables[0].Rows[0]["EmployeName"] + ",<br/><br/>Again reminding about late logins. Your late logins limit has been exceeded.<br/>Today your Late Time :" + date2 + "<br/>This Month your Late logins count:" + count + ",<br/>You are advised to come on time<br/><br/>Regards,<br/><b>HR.</b>";
                        }
                        else if (int.Parse(count) > 3)
                        {
                            msg = "Dear Ms/Mr " + ds1.Tables[0].Rows[0]["EmployeName"] + ",<br/><br/>Your late logins limit has been exceeded.<br/>Today your Late Time :" + date2 + "<br/>This Month your Late logins count:" + count + ",<br/>You are hereby informed to provide a valid reason to HR about your late coming.<br/><b>HR.</b>";
                        }
                        mail.Body = msg;
                        mail.Priority = MailPriority.High;
                        SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                        smtp.Credentials = new System.Net.NetworkCredential("noreply1@brihaspathi.com", "noreply123");
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }

                }
                else
                {
                }
            }

            Response.Write("{\"error\": false,\"message\":\"Marked Successsfully\"}");

            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee = operation.getEmployee(Convert.ToInt32(hr));

            Notification notification = new Notification();
            notification.title = "Attendance marked successfully";
            notification.body = "Your attendance mark by @" + employee.name;
            notification.image = "";
            notification.code = 4;
            sendNotification(employeeid, notification);
        }

        protected void mark_attendance1()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string hr = Request.Params["hr"].ToString().Trim();
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            DataQueries.InsertCommon("update  RequestAttendance set status='2', markby='" + hr + "' where Employee_Id='" + employeeid + "' and convert(nvarchar(10),Time,126)='" + date + "'");
            Response.Write("{\"error\": false,\"message\":\"Attendance Rejected\"}");

            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee = operation.getEmployee(Convert.ToInt32(hr));

            Notification notification = new Notification();
            notification.title = "Attendance Rejected";
            notification.body = "Your attendance Rejected by @" + employee.name;
            notification.image = "";
            notification.code = 4;

            sendNotification(employeeid, notification);

            // sendNotify(employeeid, notification);
        }
        protected void list_data()
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
            // DataSet ds = DataQueries.SelectCommon(" select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'");
            SqlCommand cmd = new SqlCommand("select * from Q_CL", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");


        }

        protected void req_price()
        {

            string model_no = Request.Params["model_no"].ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
            // DataSet ds = DataQueries.SelectCommon(" select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'");
            SqlCommand cmd = new SqlCommand("select isnull([CustomerPrice],0) as cp from [AddItem] where modelno='" + model_no + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            /*string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult +"}");*/
            if (ds.Tables[0].Rows.Count == 1)
            {
                string price = ds.Tables[0].Rows[0]["cp"].ToString();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"price\":" + price + "}");
            }
            else
            {

            }


        }

        protected void quotation()
        {

            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();

            if (type == "IP")
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }


                }
                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();



                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();


                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();



                        // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row1 + "}");
                        //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row2 + "}");


                    }
                }
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }












            /*********************************************HD*************************************************/







            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 8 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv4 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 16 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }


                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + amount7 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }
            else
            {

                Response.Write("{\"error\":true,\"message\":\"Invalid camera quantity\"}");


            }


        }

        protected void pdf()
        {

            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();
            string client_name = Request.Params["client_name"].ToString();
            string email = Request.Params["email"].ToString();
            string address = Request.Params["address"].ToString();



            if (type == "IP" && Convert.ToInt32(volume) <= 32)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")

                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }


                }

                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();

                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();


                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }


                }
            }


            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 8 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv4 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 16 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }

                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }
            // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row1 + "}");
            //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row2 + "}");
            //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            using (StringWriter sw = new StringWriter())

            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //string companyName = "Brihaspathi Technologies Pvt Ltd";
                    //int orderNo = 2303;





                    StringBuilder sb = new StringBuilder();
                    sb.Append("<h3 align='center' style='margin-bottom:5px;'><b>Estimate</b></h3>");

                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td colspan='2'><h5 style='font-weight:bold;text-align:right;margin-bottom:50%'>Date:" + DateTime.Now.ToString("dd/MM/yyyy") + ".</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Name:" + client_name + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Address:" + address + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Email:" + email + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");





                    sb.Append("<table border='1' cellspacing='2' cellpadding='5'>");
                    sb.Append("<tr bgcolor='#cecece'>");

                    sb.Append("<th>");

                    sb.Append("Item");

                    sb.Append("</th>");

                    sb.Append("<th>");


                    sb.Append("Description");


                    sb.Append("</th>");
                    sb.Append("<th>");

                    sb.Append("Quantity");

                    sb.Append("</th>");


                    sb.Append("<th>");

                    sb.Append("Rate");

                    sb.Append("</th>");

                    sb.Append("<th>");

                    sb.Append("Amount");

                    sb.Append("</th>");
                    sb.Append("</tr>");






                    //ROW1
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'> " + product + " </td>");

                    sb.Append("<td style='font-size:9px'> " + product + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + qty + " </td>");
                    sb.Append("<td style='font-size:9px'> " + price + " </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'>  " + amount + "  </td>");

                    sb.Append("</tr>");

                    //ROW2
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label10 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description1 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + qty1 + "</td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + rate1 + "  </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'>  " + amount1 + "  </td>");

                    sb.Append("</tr>");
                    //ROW3

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label15 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description2 + "   </td>");

                    sb.Append("<td style='font-size:9px;width:30px'> " + qty2 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate2 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount2 + "   </td>");
                    sb.Append("</tr>");




                    //ROW4


                    if (Convert.ToInt32(volume) >= 21 || Convert.ToInt32(volume) >= 17)
                    {
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception4portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception4port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception4portpoeamount + "   </td>");
                        sb.Append("</tr>");
                    }
                    else
                    {

                    }

                    //ROW5
                    if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)

                    {
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception8portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception8port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception8portpoeamount + "   </td>");
                        sb.Append("</tr>");

                    }
                    else
                    {

                    }


                    //ROW6

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label12 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description4 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty4 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate4 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount4 + "   </td>");
                    sb.Append("</tr>");


                    //ROW6



                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label13 + "  </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description5 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty5 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate5 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount5 + "   </td>");
                    sb.Append("</tr>");


                    //ROW5

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label14 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description6 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty6 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate6 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount6 + "   </td>");
                    sb.Append("</tr>");


                    //Installation charge

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label16 + "  </td>");


                    sb.Append("<td style='font-size:9px;border: none;'> " + description7 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty7 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate7 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount7 + "   </td>");
                    sb.Append("</tr>");


                    sb.Append("</table>");



                    sb.Append("<table>");

                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SUB TOTAL</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + subtotal + "</td>");

                    sb.Append("</tr>");



                    /* if (!(Discount.Text == ""))
                     {
                         sb.Append("<tr>");
                         sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>DiscountAmount" + Discount.Text + "%</td>");
                         sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + discountamount.Text + "</td>");

                         sb.Append("</tr>");
                     }*/


                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SGST 9% </td>");
                    sb.Append("<td   style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 9% </td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>sgst14 %</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 14 %</td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>Fixed Amount</td>");

                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + FixedAomount + "</td>");

                    sb.Append("</tr>");

                    //sb.Append("<tr>");
                    //if (!((Discount.Text) == ""))
                    //{
                    //    sb.Append("<td style='font-size:9px'> DISCOUNT</td>");
                    //    sb.Append("<td style='font-size:9px'>DISCOUNT % </td>");
                    //    sb.Append("<td style='font-size:9px'>" + Discount.Text + "</td>");
                    //    sb.Append("<td style='font-size:9px'>" + FINALL.Text + "</td>");
                    //}
                    //sb.Append("</tr>");
                    sb.Append("</table>");

                    sb.Append("<table>");
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<p style='font-size:4px'>");
                    sb.Append("<b style='font-size:20px'>Terms and Conditions</b>");
                    sb.Append("<h6>Payment Terms:70% advance with p.o and balance 30% on delivery of goods and installation</h6>");
                    sb.Append("<h6>Product Warranty: Two years from the date of installation. Warranty not applicable to physical damages and electrical burns. No warranty for power adaptors.</h6>");
                    sb.Append("<h6>Service: 2 Free service visits in 1 year. Service charges Rs.550 + taxes applicable for every additional visit.Free online support for 2 years.</h6>");
                    sb.Append("<h6>Delivery:One Week from the date of P.O With  70% Advance payment</h6>");
                    sb.Append("<h6>Quotation validty:Two weeks from the </h6>");
                    sb.Append("<h6>Any Civil/Electrical Work will be done by you at your cost as per  our  specifications.</h6>");
                    sb.Append("<h6> Power Point/Supply at every location to be provided.</h6>");
                    sb.Append("<h6>Cable should be as per actual and extra charges during installation should be paid by customer.</h6>");
                    sb.Append("</p>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                    StringReader sr = new StringReader(sb.ToString());

                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {

                        string ik = client_name.Replace(" ", "_");
                        string z = ik + "_" + DateTime.Now.ToString("dd/MM/yyyy");
                        string path = Server.MapPath("Files");
                        string filename = path + "/" + z + ".pdf";
                        i = "http://emp.bterp.in/files/" + z + ".pdf";

                        PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);


                        string imagePath = Server.MapPath("topp-hyd.jpg") + "";

                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                        image.Alignment = Element.ALIGN_TOP;

                        image.SetAbsolutePosition(30, 750);

                        image.ScaleToFit(550f, 550f);
                        image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;
                        // image.IndentationLeft = 9f;
                        //image.SpacingAfter = 9f;
                        //image.BorderWidthTop = 36f;

                        // set width and height .


                        // adding image to document
                        pdfDoc.Add(image);
                        //string fileName = Path.Combine(Server.MapPath("~/Images"), FileUpload1.FileName);
                        //FileUpload1.SaveAs(fileName); 

                        //pdfDoc.Add((new Paragraph("Name"+TextBox1.Text)));
                        /* Rectangle rect = new Rectangle(36, 108);
                         rect.enableBorderSide(1);
                         rect.enableBorderSide(2);
                         rect.enableBorderSide(4);
                         rect.enableBorderSide(8);
                         rect.setBorder(2);
                         rect.setBorderColor(BaseColor.BLACK);
                         pdfDoc.add(rect);*/
                        PdfContentByte content = writer.DirectContent;
                        iTextSharp.text.Rectangle rectangle = new iTextSharp.text.Rectangle(pdfDoc.PageSize);
                        rectangle.Left += pdfDoc.LeftMargin;
                        rectangle.Right -= pdfDoc.RightMargin;
                        rectangle.Top -= pdfDoc.TopMargin;
                        rectangle.Bottom += pdfDoc.BottomMargin;
                        // content.SetColorStroke(BaseColor.BLACK);
                        //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        content.Stroke();
                        /* var content = writer.DirectContent;
                         var pageBorderRect = new Rectangle(pdfDoc.PageSize);

                         pageBorderRect.Left += pdfDoc.LeftMargin;
                         pageBorderRect.Right -= pdfDoc.RightMargin;
                         pageBorderRect.Top -= pdfDoc.TopMargin;
                         pageBorderRect.Bottom += pdfDoc.BottomMargin;

                         content.SetColorStroke(BaseColor.BLACK);
                         content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width, pageBorderRect.Height);
                         content.Stroke();*/
                        pdfDoc.Close();



                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();





                        /*string FilePath = Server.MapPath("Files/" + z + ".pdf");
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);

                        if (FileBuffer != null)
                        {
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-length", FileBuffer.Length.ToString());
                            Response.BinaryWrite(FileBuffer);
                        }*/

                    }




                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"url\":\"" + i + "\"}");
                }



            }
        }
        protected void broadcast()
        {
            string title = Request.Params["title"].ToString();
            string description = Request.Params["description"].ToString();


            NotificationItem notification = new NotificationItem();

            notification.title = title;
            notification.body = description;
            notification.image = "";
            notification.code = 0;

            NotificationOperation msg = new NotificationOperation();
            msg.sendTopicNotification("news", notification);


            Response.Write("{\"error\": true ,\"message\":\"Send successfully.\"}");

        }
        protected void sendmail()
        {
            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();
            string client_name = Request.Params["client_name"].ToString();
            string email = Request.Params["email"].ToString();
            string address = Request.Params["address"].ToString();



            if (type == "IP" && Convert.ToInt32(volume) <= 32)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();


                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();





                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();



                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();




                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }


            }


            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV8CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV4CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV16CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }

                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //string companyName = "Brihaspathi Technologies Pvt Ltd";
                    //int orderNo = 2303;





                    StringBuilder sb = new StringBuilder();
                    sb.Append("<h3 align='center' style='margin-bottom:5px;'><b>Estimate</b></h3>");

                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td colspan='2'><h5 style='font-weight:bold;text-align:right;margin-bottom:50%'>Date:" + DateTime.Now.ToString("dd/MM/yyyy") + ".</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");


                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Name:" + client_name + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Address:" + address + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Email:" + email + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");



                    sb.Append("<table border='1' cellspacing='2' cellpadding='5'>");
                    sb.Append("<tr bgcolor='#cecece'>");

                    sb.Append("<th>");

                    sb.Append("Item");

                    sb.Append("</th>");

                    sb.Append("<th>");


                    sb.Append("Description");


                    sb.Append("</th>");
                    sb.Append("<th>");

                    sb.Append("Quantity");

                    sb.Append("</th>");


                    sb.Append("<th>");

                    sb.Append("Rate");

                    sb.Append("</th>");

                    sb.Append("<th>");

                    sb.Append("Amount");

                    sb.Append("</th>");
                    sb.Append("</tr>");






                    //ROW1
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'> " + product + " </td>");

                    sb.Append("<td style='font-size:9px'> " + product + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + volume + " </td>");
                    sb.Append("<td style='font-size:9px'> " + price + " </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'>  " + amount + "  </td>");

                    sb.Append("</tr>");

                    //ROW2
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label10 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description1 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + qty1 + "</td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + rate1 + "  </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'>  " + amount1 + "  </td>");

                    sb.Append("</tr>");
                    //ROW3

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label15 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description2 + "   </td>");

                    sb.Append("<td style='font-size:9px;width:30px'> " + qty2 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate2 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount2 + "   </td>");
                    sb.Append("</tr>");




                    //ROW4

                    if (Convert.ToInt32(volume) >= 21 || Convert.ToInt32(volume) >= 17)
                    {

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception4portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception4port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception4portpoeamount + "   </td>");
                        sb.Append("</tr>");
                    }
                    else
                    {

                    }


                    //ROW5
                    if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)

                    {

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception8portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception8port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception8portpoeamount + "   </td>");
                        sb.Append("</tr>");

                    }
                    else
                    {

                    }


                    //ROW6

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label12 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description4 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty4 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate4 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount4 + "   </td>");
                    sb.Append("</tr>");


                    //ROW6

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label13 + "  </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description5 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty5 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate5 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount5 + "   </td>");
                    sb.Append("</tr>");


                    //ROW5

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label14 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description6 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty6 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate6 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount6 + "   </td>");
                    sb.Append("</tr>");


                    //Installation charge

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label16 + "  </td>");


                    sb.Append("<td style='font-size:9px;border: none;'> " + description7 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty7 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate7 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount7 + "   </td>");
                    sb.Append("</tr>");


                    sb.Append("</table>");



                    sb.Append("<table>");

                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SUB TOTAL</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + subtotal + "</td>");

                    sb.Append("</tr>");

                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SGST 9% </td>");
                    sb.Append("<td   style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 9% </td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>sgst14 %</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 14 %</td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>Fixed Amount</td>");

                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + FixedAomount + "</td>");

                    sb.Append("</tr>");

                    sb.Append("</table>");

                    sb.Append("<table>");
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<p style='font-size:4px'>");
                    sb.Append("<b style='font-size:20px'>Terms and Conditions</b>");
                    sb.Append("<h6>Payment Terms:70% advance with p.o and balance 30% on delivery of goods and installation</h6>");

                    sb.Append("<h6>Warranty:One Year</h6>");
                    sb.Append("<h6>Delivery:One Week from the date of P.O With  70% Advance payment</h6>");
                    sb.Append("<h6>Quotation validty:Two weeks from the </h6>");
                    sb.Append("<h6>Any Civil/Electrical Work will be done by you at your cost as per  our  specifications.</h6>");
                    sb.Append("<h6> Power Point/Supply at every location to be provided.</h6>");
                    sb.Append("<h6>Cable should be as per actual and extra charges during installation should be paid by customer.</h6>");
                    sb.Append("</p>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                    StringReader sr = new StringReader(sb.ToString());




                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        /*string path = Server.MapPath("Files");
                        string filename = path + "/" + "Quickquotation" + ".Pdf";*/

                        string z = client_name + "_" + DateTime.Now.ToString("dd/MM/yyyy");
                        string path = Server.MapPath("Files");
                        string filename = path + "/" + z + ".pdf";
                        i = "http://emp.bterp.in/files/" + z + ".pdf";


                        PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);


                        string imagePath = Server.MapPath("topp-hyd.jpg") + "";

                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                        image.Alignment = Element.ALIGN_TOP;

                        image.SetAbsolutePosition(30, 750);

                        image.ScaleToFit(550f, 550f);
                        image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

                        pdfDoc.Add(image);






                        PdfContentByte content = writer.DirectContent;
                        iTextSharp.text.Rectangle rectangle = new iTextSharp.text.Rectangle(pdfDoc.PageSize);
                        rectangle.Left += pdfDoc.LeftMargin;
                        rectangle.Right -= pdfDoc.RightMargin;
                        rectangle.Top -= pdfDoc.TopMargin;
                        rectangle.Bottom += pdfDoc.BottomMargin;
                        //       content.SetColorStroke(BaseColor.BLACK);
                        //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        content.Stroke();

                        pdfDoc.Close();



                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();




                        MailMessage mm = new MailMessage("trainee@brihaspathi.com", email);
                        mm.Subject = "Quotation";
                        mm.Body = "Quick Quotation";
                        mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Quotation.pdf"));

                        mm.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential();
                        NetworkCred.UserName = "trainee@brihaspathi.com";
                        NetworkCred.Password = "yourself123";
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                        Response.Write("{\"error\": false ,\"message\":\"Mail sent Successfully\"}");


                    }

                }

            }

        }

        protected void pendingcount()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds;
            if (employeeid == "502")
            {
                ds = DataQueries.SelectCommon("select count(LeaveId) as count from LeavesStatus where Status='OnHRDesk'");

                if (ds.Tables[0].Rows.Count != 0)
                {
                    string count = ds.Tables[0].Rows[0]["count"].ToString().Trim();
                    Response.Write("{\"error\": false,\"count\":\"" + count + "\"}");
                }
            }
            else
            {
                ds = DataQueries.SelectCommon("select count(LeaveId) as count from LeavesStatus where Status='OnManagerDesk' and depthead_id='" + employeeid + "'");

                if (ds.Tables[0].Rows.Count != 0)
                {
                    string count = ds.Tables[0].Rows[0]["count"].ToString().Trim();
                    Response.Write("{\"error\": false,\"count\":\"" + count + "\"}");
                }

            }
        }


        #endregion

        #region localmethod

        public void sendmail(string email, string OTP, string subject)
        {

            MailMessage mail = new MailMessage("trainee@brihaspathi.com", email, subject, OTP);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.Credentials = new System.Net.NetworkCredential("trainee@brihaspathi.com", "yourself123");
            client.EnableSsl = true;
            client.Send(mail);

        }

        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        #endregion


        private void emp_tokens(string empid, Notification notification1)
        {
            string id = empid;
            DataSet tokens = Inventroy_Queries.SelectCommon("Select * from Employees where EmployeeId='" + empid + "' and status='1'");
            string value = tokens.Tables[0].Rows[0]["token"].ToString();
            Message msg = new Message();
            msg.sendSingleNotification(value, notification1);
        }

        private void emp_tokensss(string empid, Notification notification1)
        {
            string id = empid;
            DataSet tokens = Inventroy_Queries.SelectCommon("Select * from Employees where EmployeeId='" + empid + "' and status='1'");
            string value = tokens.Tables[0].Rows[0]["token1"].ToString();
            Message msg = new Message();
            msg.sendSingleNotification(value, notification1);
        }

        private void vp_tokens(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("Select * from Employees where deptid='13' and status='1'");
            string value = tokens.Tables[0].Rows[0]["token"].ToString();
            Message msg = new Message();
            msg.sendSingleNotification(value, notification);

        }
        private void hr_tokens(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='502' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        private void hr_tok(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='502' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }

        private void sendNotification(string employeeID, Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select token from Employees where EmployeeId='" + employeeID + "' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        private void sendNotify(string employeeID, Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select token from Employees where EmployeeId='" + employeeID + "' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }

        //camera list services for technicians 
        protected void hint()
        {
            string hint = Request.Params["hint"].ToString();

            DataSet ds = DataQueries.SelectCommon("select camera from cameradetails where camera like '%" + hint + "%' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"name\": " + JSONresult + "}");
        }

        protected void cameralist()
        {
            string cam = Request.Params["itemname"].ToString();
            string desc = string.Empty;
            string units = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select description,units from cameradetails where camera='" + cam + "' ");
            if (ds.Tables[0].Rows.Count > 0)
            {
                desc = ds.Tables[0].Rows[0]["description"].ToString();
                units = ds.Tables[0].Rows[0]["units"].ToString();

                Response.Write("{\"error\": false ,\"description\":\"" + desc + "\",\"units\":\"" + units + "\",\"itemname\":\"" + cam + "\"}");

            }
        }
        protected void additems()
        {
            //string brand = Request.Params["brand"].ToString();
            string techid = Request.Params["techid"].ToString();
            string name = Request.Params["itemname"].ToString();
            string desc = Request.Params["description"].ToString();
            string units = Request.Params["units"].ToString();
            string price = Request.Params["price"].ToString();
            string gstype = Request.Params["gst"].ToString();
            string id = Request.Params["id"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            double valueee = 0;
            if (gstype == "5%") { valueee = 0.05; }
            else if (gstype == "12%") { valueee = 0.12; }
            else if (gstype == "18%") { valueee = 0.18; }
            else if (gstype == "28%") { valueee = 0.28; }
            else { valueee = 0; }

            double totllprice = Convert.ToDouble(price) * Convert.ToDouble(units);
            double Gsstprice = totllprice * valueee;
            double totalsprice = Gsstprice + totllprice;

            DataQueries.InsertCommon("insert into technicianadditems(date,techid,itemname,description,units,price,gst,status,gstprice,cid) values('" + date + "','" + techid + "','" + name + "','" + desc + "','" + units + "','" + price + "','" + gstype + "','1','" + totalsprice + "','" + id + "')");

            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        protected void displayitems()
        {
            string techid = Request.Params["techid"].ToString();
            string custid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select t.id,c.image,t.itemname,t.description,t.units,t.price,t.gst,t.gstprice,cd.name,cd.mobile from cameradetails c inner join technicianadditems t on c.camera=t.itemname inner join customerdata cd on t.cid=cd.cid where t.techid='" + techid + "' and cd.cid='" + custid + "' and t.status='1' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void order()
        {
            string techid = Request.Params["techid"].ToString();
            string id = Request.Params["cid"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            string oid = string.Empty;
            DataQueries.InsertCommon("insert into tblorder(date,cid,techid) values('" + date + "','" + id + "','" + techid + "')");

            DataSet ds = DataQueries.SelectCommon("select orderid from tblorder where techid='" + techid + "' and cid='" + id + "' order by orderid desc");
            if (ds.Tables[0].Rows.Count > 0)
            {

                oid = ds.Tables[0].Rows[0]["orderid"].ToString().Trim();


            }
            DataSet cartdata = DataQueries.SelectCommon("select * from technicianadditems where techid='" + techid + "' and cid='" + id + "'");

            int count1 = cartdata.Tables[0].Rows.Count;
            for (int i = 0; i < count1; i++)
            {

                string datee = cartdata.Tables[0].Rows[i]["date"].ToString().Trim();
                string techidd = cartdata.Tables[0].Rows[i]["techid"].ToString().Trim();
                string itemnamee = cartdata.Tables[0].Rows[i]["itemname"].ToString().Trim();
                string descc = cartdata.Tables[0].Rows[i]["description"].ToString().Trim();
                string unitss = cartdata.Tables[0].Rows[i]["units"].ToString().Trim();
                string pricee = cartdata.Tables[0].Rows[i]["price"].ToString().Trim();
                string gstt = cartdata.Tables[0].Rows[i]["gst"].ToString().Trim();
                string statuss = cartdata.Tables[0].Rows[i]["status"].ToString().Trim();
                string gstpricee = cartdata.Tables[0].Rows[i]["gstprice"].ToString().Trim();
                string cid = cartdata.Tables[0].Rows[i]["cid"].ToString().Trim();
                //string brand= cartdata.Tables[0].Rows[i]["brand"].ToString().Trim();

                DataQueries.InsertCommon("insert into tblorderitem(date,techid,itemname,description,units,price,gst,status,gstprice,cid,orderid) values('" + date + "','" + techid + "','" + itemnamee + "','" + descc + "','" + unitss + "','" + pricee + "','" + gstt + "','1','" + gstpricee + "','" + id + "','" + oid + "')");

            }
            // DataQueries.DeleteCommon("delete from technicianadditems where techid='" + techid + "' and cid='"+id+"'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void search()
        {
            string techid = Request.Params["techid"].ToString();
            string custid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select t.id,c.image,t.itemname,t.description,t.units,t.price,t.gst,t.gstprice,cd.name,cd.mobile from cameradetails c inner join tblorderitem t on c.camera=t.itemname inner join customerdata cd on t.cid=cd.id where t.techid='" + techid + "' and cid='" + custid + "'  ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void count()
        {
            string id = Request.Params["itemid"].ToString();
            string count = Request.Params["count"].ToString();
            string gstype = string.Empty;
            string price = string.Empty;
            string gprice = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select gst,price from technicianadditems where id='" + id + "' ");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gstype = ds.Tables[0].Rows[0]["gst"].ToString();
                price = ds.Tables[0].Rows[0]["price"].ToString();
            }
            double valueee = 0;
            if (gstype == "5%") { valueee = 0.05; }
            else if (gstype == "12%") { valueee = 0.12; }
            else if (gstype == "18%") { valueee = 0.18; }
            else if (gstype == "28%") { valueee = 0.28; }
            else if (gstype == "") { valueee = 0; }

            double totllprice = Convert.ToDouble(price) * Convert.ToDouble(count);
            double Gsstprice = totllprice * valueee;
            double totalsprice = Gsstprice + totllprice;
            DataQueries.InsertCommon("update technicianadditems set units='" + count + "' , gstprice='" + totalsprice + "' where id='" + id + "'");
            DataSet dts = DataQueries.SelectCommon("select gstprice from technicianadditems where id='" + id + "'  ");
            if (dts.Tables[0].Rows.Count > 0)
            {
                gprice = dts.Tables[0].Rows[0]["gstprice"].ToString();
            }
            Response.Write("{\"error\": false ,\"gstprice\":\"" + gprice + "\"}");

        }
        protected void deleteitem()
        {
            string id = Request.Params["id"].ToString();
            string cid = Request.Params["cid"].ToString();
            string techid = Request.Params["techid"].ToString();

            DataQueries.UpdateCommon("delete from  technicianadditems where id='" + id + "'   ");
            // DataQueries.DeleteCommon("delete from tblorderitem  where itemid='" + id + "' and cid='"+cid+"' and techid='"+techid+"' ");

            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void totalgstprice()
        {
            string techid = Request.Params["techid"].ToString();
            string id = Request.Params["cid"].ToString();
            string gstprice = string.Empty;

            DataSet dts = DataQueries.SelectCommon("select sum(cast(gstprice as float)) as gstprice from technicianadditems where techid='" + techid + "' and cid='" + id + "' and status='1' ");
            if (dts.Tables[0].Rows.Count > 0)
            {
                gstprice = dts.Tables[0].Rows[0]["gstprice"].ToString();
            }
            Response.Write("{\"error\": false ,\"gstprice\":\"" + gstprice + "\"}");
        }

        protected void cartcount()
        {
            string techid = Request.Params["techid"].ToString();
            string id = Request.Params["cid"].ToString();
            string count = string.Empty;
            DataSet dts = DataQueries.SelectCommon("select count(id) as count from technicianadditems where techid='" + techid + "' and cid='" + id + "' and status='1'");
            if (dts.Tables[0].Rows.Count > 0)
            {
                count = dts.Tables[0].Rows[0]["count"].ToString();
            }
            Response.Write("{\"error\": false ,\"count\":\"" + count + "\"}");
        }

        protected void deletemultiple()
        {
            string id = Request.Params["techid"].ToString();
            string cid = Request.Params["cid"].ToString();

            DataQueries.DeleteCommon("delete from technicianadditems where techid='" + id + "' and cid='" + cid + "'  ");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void customerdetails()
        {
            string id = Request.Params["techid"].ToString();
            string name = Request.Params["name"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string custid = string.Empty;
            string mob = string.Empty;
            string cid = string.Empty;
            DataSet dtss = DataQueries.SelectCommon("select cid,mobile from customerdata where mobile='" + mobile + "'");
            if (dtss.Tables[0].Rows.Count > 0)
            {
                mob = dtss.Tables[0].Rows[0]["mobile"].ToString();
                cid = dtss.Tables[0].Rows[0]["cid"].ToString();
            }


            if (mob == mobile)
            {
                Response.Write("{\"error\": false ,\"id\":\"" + cid + "\"}");
            }

            else
            {
                DataQueries.InsertCommon("insert into customerdata(techid,name,mobile) values('" + id + "','" + name + "','" + mobile + "')");
                DataSet dts = DataQueries.SelectCommon("select cid from customerdata where mobile='" + mobile + "'");
                if (dts.Tables[0].Rows.Count > 0)
                {
                    custid = dts.Tables[0].Rows[0]["cid"].ToString();
                }
                Response.Write("{\"error\": false ,\"id\":\"" + custid + "\"}");

            }

        }

        protected void customerinfo()
        {
            string techid = Request.Params["techid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select cid,mobile,name from customerdata  where techid='" + techid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void ordersdisplay()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select orderid,date,cid from tblorder where cid='" + cid + "' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void orderitemsdisplay()
        {
            string id = Request.Params["orderid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from tblorderitem where orderid='" + id + "' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void totalgst()

        {
            string id = Request.Params["orderid"].ToString();
            DataSet dts = DataQueries.SelectCommon("select sum(cast(gstprice as float)) as total from tblorderitem where orderid='" + id + "' ");
            if (dts.Tables[0].Rows.Count > 0)
            {
                string total = dts.Tables[0].Rows[0]["total"].ToString();
                Response.Write("{\"error\": false ,\"total\":\"" + total + "\"}");

            }

        }

        //for editing products in orders

        protected void editorderitems()
        {
            string oid = Request.Params["orderid"].ToString();
            DataSet ds = DataQueries.SelectCommon(" select t.id,c.image,t.itemname,t.description,t.units,t.price,t.gst,t.gstprice,t.date,cd.name,cd.mobile from cameradetails c inner join tblorderitem t on c.camera=t.itemname inner join customerdata cd on t.cid=cd.cid where t.orderid='" + oid + "' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void editcartcount()
        {

            string id = Request.Params["orderid"].ToString();
            string count = string.Empty;
            DataSet dts = DataQueries.SelectCommon("select count(id) as count from tblorderitem where orderid='" + id + "'");
            if (dts.Tables[0].Rows.Count > 0)
            {
                count = dts.Tables[0].Rows[0]["count"].ToString();
            }
            Response.Write("{\"error\": false ,\"count\":\"" + count + "\"}");
        }

        protected void addorderitems()
        {
            //string brand = Request.Params["brand"].ToString();
            string techid = Request.Params["techid"].ToString();
            string name = Request.Params["itemname"].ToString();
            string desc = Request.Params["description"].ToString();
            string units = Request.Params["units"].ToString();
            string price = Request.Params["price"].ToString();
            string gstype = Request.Params["gst"].ToString();
            string id = Request.Params["cid"].ToString();
            string oid = Request.Params["orderid"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");

            double valueee = 0;
            if (gstype == "5%") { valueee = 0.05; }
            else if (gstype == "12%") { valueee = 0.12; }
            else if (gstype == "18%") { valueee = 0.18; }
            else if (gstype == "28%") { valueee = 0.28; }
            else { valueee = 0; }

            double totllprice = Convert.ToDouble(price) * Convert.ToDouble(units);
            double Gsstprice = totllprice * valueee;
            double totalsprice = Gsstprice + totllprice;

            DataQueries.InsertCommon("insert into tblorderitem(date,techid,itemname,description,units,price,gst,status,gstprice,cid,orderid) values('" + date + "','" + techid + "','" + name + "','" + desc + "','" + units + "','" + price + "','" + gstype + "','1','" + totalsprice + "','" + id + "','" + oid + "')");
            Response.Write("{\"error\":false,\"message\":\"success\"}");


        }


        protected void deleteorderitem()
        {
            string id = Request.Params["id"].ToString();
            DataQueries.DeleteCommon("delete from tblorderitem where id='" + id + "'  ");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void updatecount()
        {
            string id = Request.Params["itemid"].ToString();
            string count = Request.Params["count"].ToString();
            string gstype = string.Empty;
            string price = string.Empty;
            string gprice = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select gst,price from tblorderitem where id='" + id + "' ");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gstype = ds.Tables[0].Rows[0]["gst"].ToString();
                price = ds.Tables[0].Rows[0]["price"].ToString();
            }
            double valueee = 0;
            if (gstype == "5%") { valueee = 0.05; }
            else if (gstype == "12%") { valueee = 0.12; }
            else if (gstype == "18%") { valueee = 0.18; }
            else if (gstype == "28%") { valueee = 0.28; }
            else if (gstype == "") { valueee = 0; }

            double totllprice = Convert.ToDouble(price) * Convert.ToDouble(count);
            double Gsstprice = totllprice * valueee;
            double totalsprice = Gsstprice + totllprice;
            DataQueries.InsertCommon("update tblorderitem set units='" + count + "' , gstprice='" + totalsprice + "' where id='" + id + "'");
            DataSet dts = DataQueries.SelectCommon("select gstprice from tblorderitem where id='" + id + "'  ");
            if (dts.Tables[0].Rows.Count > 0)
            {
                gprice = dts.Tables[0].Rows[0]["gstprice"].ToString();
            }
            Response.Write("{\"error\": false ,\"gstprice\":\"" + gprice + "\"}");
        }
        protected void signature()
        {
            string image = Request.Params["image"].ToString();
            string name = Request.Params["name"].ToString();
            string base64 = image;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/sign/") + name + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/sign/" + name + ".jpg";
            DataQueries.InsertCommon1("insert into sign(image) values ('" + path + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }



        //Support Services
        protected void RaiseTicket()
        {
            string emp11 = Request.Params["assign"].ToString();
            string emp = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select TicketNo,BM,mobileno,problem,descripation,tktaddress,priority from Newticket where assign='" + emp11 + "' and status1 is null");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }


        protected void custservice()
        {
            string id = string.Empty;
            string image = Request.Params["image"].ToString();
            string loc = Request.Params["loc"].ToString();
            string token = Request.Params["token"].ToString();
            string cname = Request.Params["cname"].ToString();
            string mobile = Request.Params["cmobile"].ToString();
            string type = Request.Params["servicetype"].ToString();
            string base64 = image;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/custserviveimages/") + cname + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/custserviveimages/" + cname + ".jpg";

            Random randomno = new Random();
            int OTPno = randomno.Next(1000, 9999);
            sendOTP(mobile, OTPno);
            DataQueries.InsertCommon1("insert into customerservice(image,loc,token,cname,cmobile,servicetype,otp) values('" + path + "','" + loc + "','" + token + "','" + cname + "','" + mobile + "','" + type + "','" + OTPno + "')");

            DataSet ds = DataQueries.SelectCommon1("select id from customerservice where cmobile='" + mobile + "' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = ds.Tables[0].Rows[0]["id"].ToString().Trim();
            }
            Response.Write("{\"error\": false ,\"id\":\"" + id + "\"}");


        }
        protected void customerreview()
        {
            string id = Request.Params["id"].ToString();
            string review = Request.Params["review"].ToString();
            string rating = Request.Params["rating"].ToString();
            string token = Request.Params["token"].ToString();
            string mob = string.Empty;
            DataQueries.UpdateCommon1("Update customerservice set review ='" + review + "',rating='" + rating + "' where id='" + id + "'");
            DataQueries.UpdateCommon1("update newticket set status1='0' where ticketno='" + token + "'");
            DataSet ds = DataQueries.SelectCommon1("select cmobile from customerservice where id='" + id + "' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                mob = ds.Tables[0].Rows[0]["cmobile"].ToString().Trim();
            }

            string result = "";
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            StringBuilder sp = new StringBuilder();
            sp.Append("Dear Sir,Please share your feedback about our service here  https://tinyurl.com/y3a8sg9f ");
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTV" +
                "" +
                "" +
                "IMS&to=91" + mob + "&text=" + sp.ToString() + ""; request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            Encoding ec = Encoding.GetEncoding("utf-8");
            StreamReader red = new System.IO.StreamReader(stream, ec);
            result = red.ReadToEnd();
            Console.WriteLine(result);
            red.Close();
            stream.Close();
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }


        public void sendOTP(string mobile, int OTP)
        {
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Welcome to Brihaspathi Technologies..! Your OTP is :" + OTP.ToString();
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
        }


        protected void verify_otp()
        {
            string mobile = Request.Params["cmobile"].ToString();
            string otp = Request.Params["otp"].ToString();
            DataSet ds = DataQueries.SelectCommon1("Select * from customerservice where cmobile='" + mobile + "' and otp='" + otp + "' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("{\"error\":\"false\",\"message\":\"Mobile number verified successfully\"}");
            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"Failed\"}");
            }
        }
        protected void resend_otp()
        {
            string id = string.Empty;
            string mobile = Request.Params["cmobile"].ToString();
            DataSet ds = DataQueries.SelectCommon1("Select * from customerservice where cmobile='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = ds.Tables[0].Rows[0]["id"].ToString();
            }
            Random randomno = new Random();
            int OTPno = randomno.Next(1000, 9999);
            sendOTP(mobile, OTPno);
            DataQueries.UpdateCommon1("Update customerservice set otp='" + OTPno + "' where id='" + id + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }


        protected void events()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            SqlCommand cmd = new SqlCommand("select HolidayName,convert(varchar(50),HolidayDate,106) as HolidayDate from Holidays where year(holidaydate)=year(getdate()) order by cast(HolidayDate as date) asc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write(JSONresult);
        }


        //post visitor data
        protected void postvisitor()
        {
            string vname = Request.Params["vname"].ToString();
            string empid = Request.Params["empid"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string email = Request.Params["email"].ToString();
            string purpose = Request.Params["purpose"].ToString();
            DataQueries.InsertCommon1("insert into advancevisitor(vname,empid,mobile,email,purpose) values ('" + vname + "','" + empid + "','" + mobile + "','" + email + "','" + purpose + "')");
            DataSet ds = DataQueries.SelectCommon1("select id from advancevisitor where mobile='" + mobile + "' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["id"].ToString().Trim();


                Response.Write("{\"error\": false ,\"message\":\"Success\",\"id\":\"" + id + "\"}");
            }

        }

        protected void postvisitorimg()
        {
            string id = Request.Params["id"].ToString();
            string image = Request.Params["image"].ToString();
            string base64 = image;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/sign/") + id + ".jpg", bytes1);
            string path = "http://emp.bterp.in/sign/" + id + ".jpg";
            DataQueries.UpdateCommon1("Update advancevisitor set image='" + path + "' where id='" + id + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void postvisitorsdisplay()
        {

            string mobile = Request.Params["mobile"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select a.*,e.name from advancevisitor a inner join tblEmployee e on a.empid=e.empid where a.mobile='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["vname"].ToString().Trim();
                string empname = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string mmobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
                string email = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                string purpose = ds.Tables[0].Rows[0]["purpose"].ToString().Trim();
                Response.Write("{\"error\": false ,\"vname\":\"" + name + "\",\"mobile\":\"" + mmobile + "\",\"email\":\"" + email + "\",\"purpose\":\"" + purpose + "\",\"name\":\"" + empname + "\"}");

            }

        }

        // chat services

        protected void mobilereg()
        {
            string mob = Request.Params["mobile"].ToString();
            DataQueries.InsertCommon1("insert into chatreg(mobile) values ('" + mob + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void Logout()
        {
            string empid = Request.Params["employee_id"].ToString();
            string token = Request.Params["token"].ToString();
            string tk = string.Empty;
            string tk1 = string.Empty;
            DataSet ds = Inventroy_Queries.SelectCommon("select token,token1 from employees where employeeid='" + empid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                tk = ds.Tables[0].Rows[0]["token"].ToString();
                tk1 = ds.Tables[0].Rows[0]["token1"].ToString();
            }
            if (tk == token)
            {
                Inventroy_Queries.UpdateCommon("update Employees set token=null where EmployeeId='" + empid + "'");
            }
            else if (tk1 == token)
            {
                Inventroy_Queries.UpdateCommon("update Employees set token1=null where EmployeeId='" + empid + "'");
            }
            Inventroy_Queries.UpdateCommon("delete from tokenstbl where empid='" + empid + "' and token='" + token + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");


        }
        protected void allempleavelist()
        {
            DataSet ds = DataQueries.SelectCommon("with cte as (select e.employename,cast(l.applieddate as varchar) as applieddate,cast(l.fromdate as varchar) as fromdate,cast(l.todate as varchar) as todate,l.reasontoapply,l.leaveid,l.status from Employees e inner join LeavesStatus l on l.employeedID = e.employeeId ) select *  from cte order by leaveid desc ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }



    }
}

