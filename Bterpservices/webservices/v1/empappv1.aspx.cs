﻿using inventory.DataAccessLayer;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.InventoryHelper;
using inventory.webservices.v1.include;
using inventory.webservices.v1.model;
using System.Web.Script.Serialization;

namespace inventory.webservices.v1
{
    public partial class empappv1 : System.Web.UI.Page
    {
        string qty;
        string rate1;
        string Label10;
        string description1;
        string Label15;
        string description2;
        string rate2;

        string Label12;
        string description4;
        string rate4;

        string Label13;
        string description5;
        string rate5;

        string Label14;
        string description6;
        string rate6;

        string Label16;
        string description7;
        string rate7;


        string amount;
        string amount1;
        string amount2;
        string amount3;
        string amount4;
        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
        string amount5;
        string amount6;
        string amount7;


        string sgst9;
        string cgst9;
        string sgst14;
        string cgst14;

        string subtotal;
        string FixedAomount;
        string rate;
        string i;

        string qty1;
        string qty2;
        string qty3;
        string qty4;
        string qty5;
        string qty6;
        string qty7;

        // gst calculation

        //cameraamount gst18%


        double cameraamountgst18;

        // cameradvr8channel gst18%
        double ccameradvr8channelgst18;

        // two tb gst 18%
        double twotbgst18;

        //installationcharge gst 18%
        double installationcharge18;

        // total gst18% amount
        double totalgst18amount;

        // calculating sgst9% 
        double sgst9percentageamount;
        //calculating cgst 9%
        double cgst9percentageamount;

        // sgst9 = sgst9percentageamount.ToString();
        //  cgst9= cgst9percentageamount.ToString();

        //  gst28% calculation

        double poweradaptergst28amount;
        double BNC28amount;
        double powerconnectorsgst28amount;
        double threeplusonegst28amount;

        //total gst28% amount 

        double totalgst28amount;

        //calculating sgst14%
        double sgst14percentageamount;
        //calculating cgst14%
        double cgst14percentageamount;


        string exception4portpoe;
        string exception4portpoedesc;
        string exception4portpoerate;
        string qtyforexception4port;
        string exception4portpoeamount;


        string qtyforexception8port;
        string exception8portpoeamount;
        string exception8portpoerate;

        string exception8portpoe;
        string exception8portpoedesc;


        string Label11;
        string description3;
        string rate3;


        #region Navigator
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod == "GET")
            {
                try
                {
                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "login":
                            login();
                            break;
                        case "profileimage":
                            profileimage();
                            break;
                        case "forgot_password":
                            forgot_password();
                            break;

                        case "otp_verification":
                            otp_verification();
                            break;

                        case "create_password":
                            create_password();
                            break;

                        case "monthreport":
                            monthreport();
                            break;

                        case "daily":
                            daily();
                            break;

                        case "twodates":
                            twodates();
                            break;

                        case "leaves":
                            leaves();
                            break;

                        case "leavedetails":
                            leavedetails();
                            break;

                        case "paylist":
                            paylist();
                            break;

                        case "payslip":
                            payslip();
                            break;

                        case "contacts":
                            contacts();
                            break;

                        case "attendance_status":
                            attendance_status();
                            break;

                        case "attendance_req":
                            attendance_req();
                            break;

                        case "request_data":
                            request_data();
                            break;

                        case "mark_attendance":
                            mark_attendance();
                            break;

                        case "list_data":
                            list_data();
                            break;

                        case "req_price":
                            req_price();
                            break;

                        case "quotation":
                            quotation();
                            break;
                        case "sendmail":
                            sendmail();
                            break;

                        case "pdf":
                            pdf();
                            break;
                        case "update_token":
                            token();
                            break;

                        case "push":
                            push();
                            break;

                        /*case "test_tok":
                            hr_tokens();
                            break;*/

                    }
                }
                catch (Exception ex)
                {
                    Response response = new Response();
                    response.errorCode = 404;
                    response.message = "Server error: " + ex;

                    var jsonSerialiser = new JavaScriptSerializer();
                    Response.Write(jsonSerialiser.Serialize(response));
                }
                #endregion
            }
        }

       
        #region webservices

        protected void push()
        {
            DataSet ds = Inventroy_Queries.SelectCommon("select * from Employees where EmployeeId='472'");
            string id = ds.Tables[0].Rows[0]["token"].ToString();

            string deviceId = id;
            Notification obj = new Notification();
            obj.title = "Order Confirmed";
            obj.body = "Your order with reference number  is succesfully placed.Total Order Value ";
            obj.image = "";
            obj.code =2;


            Message msg = new Message();
            msg.sendTopicNotification("news", obj);
        }

        protected void login()
        {
            int employeeid = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());
            string password = Request.Params["password"].ToString().Trim();
            string token = Request.Params["token"].ToString().Trim();

            EmployeeOperation operation = new EmployeeOperation();

            EmployeeItem employee = operation.authentication(employeeid, password, token);

            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(employee);

            Response.Write(JSONString);
        }

        protected void token()
        {
            int employeeid = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());
            string token = Request.Params["token"].ToString();

            EmployeeOperation operation = new EmployeeOperation();
            operation.updateToken(employeeid, token);

            Response response = new Response();
            response.errorCode = 200;
            response.message = "Update successfully";

            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(response);

            Response.Write(JSONString);
        
        }

        protected void contacts()
        {
            EmployeeOperation operation = new EmployeeOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.getEmployeeList()));
        }

        protected void profileimage()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());

            EmployeeOperation operation = new EmployeeOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.profileimage(empID)));
        }

        protected void forgot_password()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());

            EmployeeOperation operation = new EmployeeOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.forgotPassword(empID)));
        }

        protected void otp_verification()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());
            string otp = Request.Params["otp"].ToString().Trim();

            EmployeeOperation operation = new EmployeeOperation();
            Response response = new Response();

            if (operation.isValidOTP(empID, otp))
            {
                response.errorCode = 200;
                response.message = "Verify Sussceefully";
               
            }
            else
            {
                response.errorCode = 103;
                response.message = "OTP code is not valid";
            }

            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(response));
        }

        protected void create_password()
        {
            string new_password = Request.Params["new_password"].ToString().Trim();
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());

            EmployeeOperation operation = new EmployeeOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.createPassword(empID, new_password)));
        }

        protected void monthreport()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_MonthlydatesAndroid", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false,\"message\":\"Success\",\"data\":" + JSONresult + "}");
            }


            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }



        }

        protected void daily()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            double empid = double.Parse(employeeid);
            string MonthDate = Request.Params["MonthDate"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Dailydates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", MonthDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("MonthlyReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);

                if (dt.Rows.Count > 0)
                {

                    string Shift = dt.Rows[0]["Shift"].ToString();
                    string InTime = dt.Rows[0]["In Time"].ToString();
                    string Outtime = dt.Rows[0]["Out Time"].ToString();
                    string Duration = dt.Rows[0]["Duration"].ToString();
                    string ot = dt.Rows[0]["OT"].ToString();
                    string status = dt.Rows[0]["Status"].ToString();
                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"Shift\":\"" + Shift + "\",\"InTime\":\"" + InTime + "\",\"OutTime\":\"" + Outtime + "\",\"Duration\":\"" + Duration + "\",\"OT\":\"" + ot + "\",\"Status\":\"" + status + "\"}");
                }
                else
                {
                    Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
                }

            }
        }

        protected void twodates()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string StartDate = Request.Params["StartDate"].ToString().Trim();
            string EndDate = Request.Params["EndDate"].ToString().Trim();
            double empid = double.Parse(employeeid);
            DataSet ds = DataQueries.SelectCommon("select CompanyId,DeptId,Desigantion,Company from Employees where EmployeeId='" + employeeid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {

                string Company = ds.Tables[0].Rows[0]["CompanyId"].ToString().Trim();
                string Dept = ds.Tables[0].Rows[0]["DeptId"].ToString().Trim();
                string Type = "Basic";
                string Desigantion = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                string CompanyName = ds.Tables[0].Rows[0]["Company"].ToString().Trim();

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
                SqlCommand cmd = new SqlCommand("Proc_Twodates", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Company", Company);
                cmd.Parameters.AddWithValue("@Department", Dept);
                cmd.Parameters.AddWithValue("@EmpName", employeeid);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Basic", Type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("TwoDatesReport");
                da.Fill(dt);
                string JSONresult = DataTableToJsonWithJsonNet(dt);
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }
        }

        protected void leaves()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            SqlCommand cmd = new SqlCommand("Proc_LeavesAndroid", con);
            SqlCommand cmd1 = new SqlCommand("select AppliedDate,FromDate,ToDate,Duration,ReasontoApply,Status,StatusDate,cl,lop from attendance.LeavesStatus where employeedid='" + employeeid + "'", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@employeedid", employeeid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable("Leaves");
            DataTable dt1 = new DataTable("LeaveRecords");
            da.Fill(dt);
            da1.Fill(dt1);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            string JSONresult1 = DataTableToJsonWithJsonNet(dt1);
            if (dt.Rows.Count > 0)
            {
                int BalanceLeaves = Convert.ToInt32(dt.Rows[0]["balanceleaves"].ToString());
                int Used = Convert.ToInt32(dt.Rows[0]["used"].ToString());
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"Remaining\": " + BalanceLeaves + ",\"Used\":" + Used + ",\"History\": " + JSONresult1 + "}");
            }
            else
            {
                Response.Write("{\"error\": true,\"result\":\"NoRecordFound\"}");
            }

        }

        protected void leavedetails()
        {
            string employee_id = Request.Params["employee_id"].ToString().Trim();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BtrakConnection"].ToString());
            SqlCommand cmd = new SqlCommand("select applieddate,fromdate,todate,duration,status,reasontoapply from attendance.LeavesStatus where employeedid='" + employee_id + "'and status='approved'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }

        protected void paylist()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select a.Year,a.Month,a.total,a.ttotal,a.present,a.workingdays,a.Name,b.EmployeeCodeInDevice from Payslip a left outer join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }

        protected void payslip()
        {
            string employeeid = Request.Params["employee_id"].ToString().Trim();
            string month = Request.Params["month"].ToString().Trim();
            string year = Request.Params["year"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select * from Payslip a inner join Employees b on a.Name=b.EmployeName where EmployeeCodeInDevice='" + employeeid + "' and month='" + month + "' and year='" + year + "' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string department = ds.Tables[0].Rows[0]["Departrment"].ToString().Trim();
                string designation = ds.Tables[0].Rows[0]["Designation"].ToString().Trim();
                string empnum = ds.Tables[0].Rows[0]["EmpNo"].ToString().Trim();
                string dob = ds.Tables[0].Rows[0]["DOB"].ToString().Trim();
                string doJ = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                string workingdays = ds.Tables[0].Rows[0]["Workingdays"].ToString().Trim();
                string paybledays = ds.Tables[0].Rows[0]["Paybledays"].ToString().Trim();
                string pfno = ds.Tables[0].Rows[0]["PFNo"].ToString().Trim();
                string basic = ds.Tables[0].Rows[0]["Basic"].ToString().Trim();
                string hra = ds.Tables[0].Rows[0]["HRA"].ToString().Trim();
                string da = ds.Tables[0].Rows[0]["DA"].ToString().Trim();
                string pf = ds.Tables[0].Rows[0]["PF"].ToString().Trim();
                string ca = ds.Tables[0].Rows[0]["CA"].ToString().Trim();
                string esi = ds.Tables[0].Rows[0]["ESI"].ToString().Trim();
                string ea = ds.Tables[0].Rows[0]["EA"].ToString().Trim();
                string asa = ds.Tables[0].Rows[0]["ASa"].ToString().Trim();
                string it = ds.Tables[0].Rows[0]["IT"].ToString().Trim();
                string paydayd = ds.Tables[0].Rows[0]["paydayd"].ToString().Trim();
                string total = ds.Tables[0].Rows[0]["Total"].ToString().Trim();
                string deducation = ds.Tables[0].Rows[0]["Deducation"].ToString().Trim();
                string ttotal = ds.Tables[0].Rows[0]["TTotal"].ToString().Trim();
                string amountwords = ds.Tables[0].Rows[0]["Amountwords"].ToString().Trim();


                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"department\":\"" + name + "\",\"designation\":\"" + designation + "\",\"empnum\":\"" + empnum + "\",\"dob\":\"" + dob + "\",\"doj\":\"" + doJ + "\",\"workingdays\":\"" + workingdays + "\",\"paybledays\":\"" + paybledays + "\",\"pfno\":\"" + pfno + "\",\"basic\":\"" + basic + "\",\"hra\":\"" + hra + "\",\"da\":\"" + da + "\",\"pf\":\"" + pf + "\",\"ca\":\"" + ca + "\",\"esi\":\"" + esi + "\",\"ea\":\"" + ea + "\",\"asa\":\"" + asa + "\",\"it\":\"" + it + "\",\"LoseofPay\":\"" + paydayd + "\",\"total\":\"" + total + "\",\"deducation\":\"" + deducation + "\",\"ttotal\":\"" + ttotal + "\",\"amountwords\":\"" + amountwords + "\"}");

            }
            else
            {

                Response.Write("{\"error\": True ,\"message\":\"No Records Found\"}");

            }



        }

        protected void attendance_status()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());

            AttendanceOperation operation = new AttendanceOperation();

            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.getAttendanceRequest(empID)));
        }

        protected void attendance_req()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());
            string reason = Request.Params["reason"].ToString().Trim();
            string latitude = Request.Params["latitude"].ToString().Trim();
            string longitude = Request.Params["longitude"].ToString().Trim();
            string image = Request.Params["image"].ToString();


            AttendanceOperation operation = new AttendanceOperation();
            AttendanceRequest request = operation.setAttendanceRequest(empID, reason, latitude, longitude, image);
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(request));

            hr_tokens(reason);

        }

        protected void request_data()
        {
            AttendanceOperation operation = new AttendanceOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.getRequestList()));
        }

        protected void mark_attendance()
        {
            int empID = Convert.ToInt32(Request.Params["employee_id"].ToString().Trim());
            int hrID = Convert.ToInt32(Request.Params["hr"].ToString().Trim());

            AttendanceOperation operation = new AttendanceOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            Response.Write(jsonSerialiser.Serialize(operation.markAttendance(empID, hrID)));
        }

        protected void list_data()
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
            // DataSet ds = DataQueries.SelectCommon(" select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'");
            SqlCommand cmd = new SqlCommand("select * from Q_CL", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Leaves");
            da.Fill(dt);
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");


        }

        protected void req_price()
        {

            string model_no = Request.Params["model_no"].ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
            // DataSet ds = DataQueries.SelectCommon(" select [EmployeName],[CellNo],[CompanyEmail] from [attendance].[dbo].[Employees] where [status]='1'");
            SqlCommand cmd = new SqlCommand("select isnull([CustomerPrice],0) as cp from [AddItem] where modelno='" + model_no + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            /*string JSONresult = DataTableToJsonWithJsonNet(dt);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult +"}");*/
            if (ds.Tables[0].Rows.Count == 1)
            {
                string price = ds.Tables[0].Rows[0]["cp"].ToString();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"price\":" + price + "}");
            }
            else
            {

            }


        }

        protected void quotation()
        {

            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();

            if (type == "IP")
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }


                }
                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();



                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();


                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();



                        // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row1 + "}");
                        //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row2 + "}");


                    }
                }
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }












            /*********************************************HD*************************************************/







            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 8 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv4 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='Unv 16 Channel Dvr'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }


                Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + amount7 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }
            else
            {

                Response.Write("{\"error\":true,\"message\":\"Invalid camera quantity\"}");


            }


        }

        protected void pdf()
        {

            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();
            string client_name = Request.Params["client_name"].ToString();
            string email = Request.Params["email"].ToString();
            string address = Request.Params["address"].ToString();



            if (type == "IP" && Convert.ToInt32(volume) <= 32)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")

                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }


                }

                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();

                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();


                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }


                }
            }


            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV8CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV4CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV16CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }

                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }
            // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row1 + "}");
            //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + row2 + "}");
            //   Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            using (StringWriter sw = new StringWriter())

            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    //string companyName = "Brihaspathi Technologies Pvt Ltd";
                    //int orderNo = 2303;





                    StringBuilder sb = new StringBuilder();
                    sb.Append("<h3 align='center' style='margin-bottom:5px;'><b>Estimate</b></h3>");

                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td colspan='2'><h5 style='font-weight:bold;text-align:right;margin-bottom:50%'>Date:" + DateTime.Now.ToString("dd/MM/yyyy") + ".</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                    sb.Append("<table  border='0'>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Name:" + client_name + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Address:" + address + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr style='border:none'>");
                    sb.Append("<td><h5 style='font-weight:bold'>Email:" + email + "</h5></td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");





                    sb.Append("<table border='1' cellspacing='2' cellpadding='5'>");
                    sb.Append("<tr bgcolor='#cecece'>");

                    sb.Append("<th>");

                    sb.Append("Item");

                    sb.Append("</th>");

                    sb.Append("<th>");


                    sb.Append("Description");


                    sb.Append("</th>");
                    sb.Append("<th>");

                    sb.Append("Quantity");

                    sb.Append("</th>");


                    sb.Append("<th>");

                    sb.Append("Rate");

                    sb.Append("</th>");

                    sb.Append("<th>");

                    sb.Append("Amount");

                    sb.Append("</th>");
                    sb.Append("</tr>");






                    //ROW1
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'> " + product + " </td>");

                    sb.Append("<td style='font-size:9px'> " + product + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + qty + " </td>");
                    sb.Append("<td style='font-size:9px'> " + price + " </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'>  " + amount + "  </td>");

                    sb.Append("</tr>");

                    //ROW2
                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label10 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description1 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + qty1 + "</td>");

                    sb.Append("<td style='font-size:9px;border: none;'>  " + rate1 + "  </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'>  " + amount1 + "  </td>");

                    sb.Append("</tr>");
                    //ROW3

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label15 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description2 + "   </td>");

                    sb.Append("<td style='font-size:9px;width:30px'> " + qty2 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate2 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount2 + "   </td>");
                    sb.Append("</tr>");




                    //ROW4


                    if (Convert.ToInt32(volume) >= 21 || Convert.ToInt32(volume) >= 17)
                    {
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception4portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception4port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception4portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception4portpoeamount + "   </td>");
                        sb.Append("</tr>");
                    }
                    else
                    {

                    }

                    //ROW5
                    if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)

                    {
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + exception8portpoe + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoedesc + "   </td>");

                        sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception8port + " </td>");

                        sb.Append("<td style='font-size:9px'> " + exception8portpoerate + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + exception8portpoeamount + "   </td>");
                        sb.Append("</tr>");

                    }
                    else
                    {

                    }


                    //ROW6

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label12 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description4 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty4 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate4 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount4 + "   </td>");
                    sb.Append("</tr>");


                    //ROW6

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label13 + "  </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + description5 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty5 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate5 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount5 + "   </td>");
                    sb.Append("</tr>");


                    //ROW5

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px'>" + Label14 + "  </td>");

                    sb.Append("<td style='font-size:9px'> " + description6 + "   </td>");

                    sb.Append("<td style='font-size:9px'> " + qty6 + " </td>");

                    sb.Append("<td style='font-size:9px'> " + rate6 + "   </td>");
                    sb.Append("<td style='font-size:9px;text-align:right'> " + amount6 + "   </td>");
                    sb.Append("</tr>");


                    //Installation charge

                    sb.Append("<tr>");

                    sb.Append("<td style='font-size:9px;border: none;'>" + Label16 + "  </td>");


                    sb.Append("<td style='font-size:9px;border: none;'> " + description7 + "   </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + qty7 + " </td>");

                    sb.Append("<td style='font-size:9px;border: none;'> " + rate7 + "   </td>");
                    sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount7 + "   </td>");
                    sb.Append("</tr>");


                    sb.Append("</table>");



                    sb.Append("<table>");

                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SUB TOTAL</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + subtotal + "</td>");

                    sb.Append("</tr>");



                    /* if (!(Discount.Text == ""))
                     {
                         sb.Append("<tr>");
                         sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>DiscountAmount" + Discount.Text + "%</td>");
                         sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + discountamount.Text + "</td>");

                         sb.Append("</tr>");
                     }*/


                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SGST 9% </td>");
                    sb.Append("<td   style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 9% </td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst9 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>sgst14 %</td>");
                    sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 14 %</td>");
                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst14 + "</td>");

                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>Fixed Amount</td>");

                    sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + FixedAomount + "</td>");

                    sb.Append("</tr>");

                    //sb.Append("<tr>");
                    //if (!((Discount.Text) == ""))
                    //{
                    //    sb.Append("<td style='font-size:9px'> DISCOUNT</td>");
                    //    sb.Append("<td style='font-size:9px'>DISCOUNT % </td>");
                    //    sb.Append("<td style='font-size:9px'>" + Discount.Text + "</td>");
                    //    sb.Append("<td style='font-size:9px'>" + FINALL.Text + "</td>");
                    //}
                    //sb.Append("</tr>");
                    sb.Append("</table>");

                    sb.Append("<table>");
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("<p style='font-size:4px'>");
                    sb.Append("<b style='font-size:20px'>Terms and Conditions</b>");
                    sb.Append("<h6>Payment Terms:70% advance with p.o and balance 30% on delivery of goods and installation</h6>");

                    sb.Append("<h6>Warranty:One Year</h6>");
                    sb.Append("<h6>Delivery:One Week from the date of P.O With  70% Advance payment</h6>");
                    sb.Append("<h6>Quotation validty:Two weeks from the </h6>");
                    sb.Append("<h6>Any Civil/Electrical Work will be done by you at your cost as per  our  specifications.</h6>");
                    sb.Append("<h6> Power Point/Supply at every location to be provided.</h6>");
                    sb.Append("<h6>Cable should be as per actual and extra charges during installation should be paid by customer.</h6>");
                    sb.Append("</p>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");

                    StringReader sr = new StringReader(sb.ToString());

                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {

                        string ik = client_name.Replace(" ", "_");
                        string z = ik + "_" + DateTime.Now.ToString("dd/MM/yyyy");
                        string path = Server.MapPath("Files");
                        string filename = path + "/" + z + ".pdf";
                        i = "http://emp.bterp.in/files/" + z + ".pdf";

                        PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);


                        string imagePath = Server.MapPath("topp-hyd.jpg") + "";

                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                        image.Alignment = Element.ALIGN_TOP;

                        image.SetAbsolutePosition(30, 750);

                        image.ScaleToFit(550f, 550f);
                        image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;
                        // image.IndentationLeft = 9f;
                        //image.SpacingAfter = 9f;
                        //image.BorderWidthTop = 36f;

                        // set width and height .


                        // adding image to document
                        pdfDoc.Add(image);





                        //string fileName = Path.Combine(Server.MapPath("~/Images"), FileUpload1.FileName);
                        //FileUpload1.SaveAs(fileName); 

                        //pdfDoc.Add((new Paragraph("Name"+TextBox1.Text)));
                        /* Rectangle rect = new Rectangle(36, 108);
                         rect.enableBorderSide(1);
                         rect.enableBorderSide(2);
                         rect.enableBorderSide(4);
                         rect.enableBorderSide(8);
                         rect.setBorder(2);
                         rect.setBorderColor(BaseColor.BLACK);
                         pdfDoc.add(rect);*/
                        PdfContentByte content = writer.DirectContent;
                        iTextSharp.text.Rectangle rectangle = new iTextSharp.text.Rectangle(pdfDoc.PageSize);
                        rectangle.Left += pdfDoc.LeftMargin;
                        rectangle.Right -= pdfDoc.RightMargin;
                        rectangle.Top -= pdfDoc.TopMargin;
                        rectangle.Bottom += pdfDoc.BottomMargin;
                    //    content.SetColorStroke(BaseColor.BLACK);
                        //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                        content.Stroke();
                        /* var content = writer.DirectContent;
                         var pageBorderRect = new Rectangle(pdfDoc.PageSize);

                         pageBorderRect.Left += pdfDoc.LeftMargin;
                         pageBorderRect.Right -= pdfDoc.RightMargin;
                         pageBorderRect.Top -= pdfDoc.TopMargin;
                         pageBorderRect.Bottom += pdfDoc.BottomMargin;

                         content.SetColorStroke(BaseColor.BLACK);
                         content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width, pageBorderRect.Height);
                         content.Stroke();*/
                        pdfDoc.Close();



                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();





                        /*string FilePath = Server.MapPath("Files/" + z + ".pdf");
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);

                        if (FileBuffer != null)
                        {
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-length", FileBuffer.Length.ToString());
                            Response.BinaryWrite(FileBuffer);
                        }*/

                    }




                    Response.Write("{\"error\": false ,\"message\":\"Success\",\"url\":\"" + i + "\"}");
                }
                

            
            }
        }

        protected void sendmail()
        {
            string volume = Request.Params["volume"].ToString();
            string price = Request.Params["price"].ToString();
            string product = Request.Params["product"].ToString();
            string type = Request.Params["type"].ToString();
            string client_name = Request.Params["client_name"].ToString();
            string email = Request.Params["email"].ToString();
            string address = Request.Params["address"].ToString();



            if (type == "IP" && Convert.ToInt32(volume) <= 32)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // 8 Chaneel
                    string rate;
                    rate = price;
                    // using (SqlConnection sqlConn3 = new SqlConnection(con))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV8CHANEELnVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();


                            }
                            dr.Close();
                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(250).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(250) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                    }
                }
                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    // 4 Chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    // rate.Text = price.Text;
                    // using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV4CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(200).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(200) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {
                    // 16 Chaneel


                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //rate.Text = price.Text;
                    //using (SqlConnection sqlConn3 = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV16CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();
                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();

                        }

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(qty) + Convert.ToDouble(qty)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(qty)).ToString();

                        amount = (Convert.ToDouble(qty) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        //amount4.Text = ((Convert.ToInt32(qty) + Convert.ToInt32(qty)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(qty) + Convert.ToDouble(qty)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(qty) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();


                        // gst calculation

                        //cameraamount gst18%


                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 17 && Convert.ToInt32(volume) <= 20)
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {


                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();

                            }
                            dr.Close();
                            con.Close();
                        }

                        SqlCommand cmd4 = new SqlCommand("select S2IPDESCR,S2IPAmount from IPCHANEEL2 where CHANNELDESC='UNV4CHANEELnVR'", con);

                        con.Open();

                        SqlDataReader dr1 = cmd4.ExecuteReader();

                        if (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();
                        }
                        dr1.Close();
                        con.Close();

                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (1 * Convert.ToDouble(exception4portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }
                else if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    {
                        using (SqlCommand sqlCmd3 = new SqlCommand())
                        {
                            sqlCmd3.CommandText = "SELECT * FROM IPCHANEEL2 where  CHANNELDESC='UNV32CHANEELNVR'";
                            sqlCmd3.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd3.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1IPDESCR"].ToString();
                                description2 = dr["S1IPDESCR"].ToString();
                                rate2 = double.Parse(dr["S1IPAmount"].ToString()).ToString();

                                Label12 = dr["S2IPDESCR"].ToString();
                                description4 = dr["S2IPDESCR"].ToString();

                                rate4 = double.Parse(dr["S2IPAmount"].ToString()).ToString();

                                Label13 = dr["S4IPDESCR"].ToString();
                                description5 = dr["S4IPDESCR"].ToString();
                                rate5 = double.Parse(dr["S4IPAmount"].ToString()).ToString();

                                Label14 = dr["S5IPDESCR"].ToString();
                                description6 = dr["S5IPDESCR"].ToString();
                                rate6 = double.Parse(dr["S5IPAmount"].ToString()).ToString();

                                Label16 = dr["S6IPDESCR"].ToString();
                                description7 = dr["S6IPDESCR"].ToString();
                                rate7 = double.Parse(dr["S6IPAmount"].ToString()).ToString();





                            }
                            dr.Close();
                            con.Close();
                        }


                        SqlCommand cmddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV4CHANEELNVR'", con);

                        con.Open();
                        SqlDataReader dr1 = cmddd.ExecuteReader();
                        while (dr1.Read())
                        {
                            exception4portpoe = dr1["S2IPDESCR"].ToString();
                            exception4portpoerate = decimal.Parse(dr1["S2IPAmount"].ToString()).ToString();

                            exception4portpoedesc = dr1["S2IPDESCR"].ToString();


                        }
                        dr1.Close();
                        con.Close();



                        SqlCommand cmdddd = new SqlCommand("SELECT * FROM IPCHANEEL2 where  CHANNELDESC = 'UNV8CHANEELNVR'", con);
                        con.Open();
                        SqlDataReader dr2 = cmdddd.ExecuteReader();
                        while (dr2.Read())
                        {
                            exception8portpoe = dr2["S2IPDESCR"].ToString();
                            exception8portpoerate = decimal.Parse(dr2["S2IPAmount"].ToString()).ToString();
                            exception8portpoedesc = dr2["S2IPDESCR"].ToString();

                        }
                        dr2.Close();
                        con.Close();




                        qty = (Convert.ToDouble(volume)).ToString();
                        qty1 = Convert.ToDouble(1).ToString();
                        qty2 = Convert.ToDouble(1).ToString();
                        qty4 = Convert.ToDouble(1).ToString();
                        qtyforexception4port = Convert.ToDouble(2).ToString();
                        qtyforexception8port = Convert.ToDouble(1).ToString();

                        qty5 = (Convert.ToDouble(volume) + Convert.ToDouble(volume)).ToString();
                        qty6 = Convert.ToDouble(400).ToString();

                        qty7 = (Convert.ToDouble(volume)).ToString();

                        amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                        amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                        amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                        amount4 = (1 * Convert.ToDouble(rate4)).ToString();
                        exception4portpoeamount = (2 * Convert.ToDouble(exception4portpoerate)).ToString();
                        exception8portpoeamount = (1 * Convert.ToDouble(exception8portpoerate)).ToString();
                        //amount4.Text = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4.Text)).ToString();
                        amount5 = ((Convert.ToDouble(volume) + Convert.ToDouble(volume)) * Convert.ToDouble(rate5)).ToString();
                        amount6 = (Convert.ToDouble(400) * Convert.ToDouble(rate6)).ToString();
                        amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();


                        subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount4) + Convert.ToDouble(exception4portpoeamount) + Convert.ToDouble(exception8portpoeamount) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();

                        cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                        // cameradvr8channel gst18%
                        ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                        // two tb gst 18%
                        twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                        //installationcharge gst 18%
                        installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                        // total gst18% amount
                        totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                        // calculating sgst9% 
                        sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                        //calculating cgst 9%
                        cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                        sgst9 = sgst9percentageamount.ToString();
                        cgst9 = cgst9percentageamount.ToString();

                        //  gst28% calculation

                        poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount2) * 28) / 100;
                        BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                        powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                        threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                        //total gst28% amount 

                        totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                        //calculating sgst14%
                        sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                        //calculating cgst14%
                        cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                        sgst14 = sgst14percentageamount.ToString();
                        cgst14 = cgst14percentageamount.ToString();
                        //Session["totalgst18amount"] = totalgst18amount;
                        //Session["totalgst28amount"] = totalgst28amount;

                        FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();


                    }
                }


            }


            else if (Convert.ToInt32(volume) <= 16)
            {

                if (volume == "5" || volume == "6" || volume == "7" || volume == "8")
                {
                    // 8 Chaneel

                    rate = double.Parse(price).ToString();
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV8CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 2.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 250.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (2 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (250 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "1" || volume == "2" || volume == "3" || volume == "4")
                {
                    //  4 chaneel



                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());

                    rate = price;
                    // using (SqlConnection con = new SqlConnection(connstrg))
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV4CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();

                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();

                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();

                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    qty3 = 1.ToString();
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 100.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (1 * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (100 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();
                }

                else if (volume == "9" || volume == "10" || volume == "11" || volume == "12" || volume == "13" || volume == "14" || volume == "15" || volume == "16")
                {

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ToString());
                    //16 chaneel
                    rate = price;
                    //  using (SqlConnection con = new SqlConnection(connstrg))
                    {

                        using (SqlCommand sqlCmd2 = new SqlCommand())
                        {
                            sqlCmd2.CommandText = "SELECT * FROM HDCHANEEL2 where  CHANNELDESC='UNV16CHANEELDVR'";
                            sqlCmd2.Connection = con;
                            con.Open();
                            SqlDataReader dr = sqlCmd2.ExecuteReader();
                            if (dr.Read())
                            {
                                Label10 = dr["CHANNELDESC"].ToString();
                                description1 = dr["CHANNELDESC"].ToString();
                                rate1 = double.Parse(dr["CHANNElAmount"].ToString()).ToString();


                                Label15 = dr["S1DESCR"].ToString();
                                description2 = dr["S1DESCR"].ToString();
                                rate2 = double.Parse(dr["S1Amount"].ToString()).ToString();

                                Label11 = dr["S2DESCR"].ToString();
                                description3 = dr["S2DESCR"].ToString();
                                rate3 = double.Parse(dr["S2Amount"].ToString()).ToString();


                                Label12 = dr["S3DESCR"].ToString();
                                description4 = dr["S3DESCR"].ToString();
                                rate4 = double.Parse(dr["S3Amount"].ToString()).ToString();


                                Label13 = dr["S4DESCR"].ToString();
                                description5 = dr["S4DESCR"].ToString();
                                rate5 = double.Parse(dr["S4Amount"].ToString()).ToString();

                                Label14 = dr["S5DESCR"].ToString();
                                description6 = dr["S5DESCR"].ToString();
                                rate6 = double.Parse(dr["S5Amount"].ToString()).ToString();

                                Label16 = dr["S6DESCR"].ToString();
                                description7 = dr["S6DESCR"].ToString();
                                rate7 = double.Parse(dr["S6Amount"].ToString()).ToString();


                            }
                            dr.Close();

                        }
                    }
                    qty = volume;

                    qty1 = 1.ToString();
                    qty2 = 1.ToString();
                    if (volume == Convert.ToInt32(13).ToString() || volume == Convert.ToInt32(14).ToString() || volume == Convert.ToInt32(15).ToString() || volume == Convert.ToInt32(16).ToString())
                    {
                        qty3 = 4.ToString();
                    }
                    else
                    {
                        qty3 = 3.ToString();
                    }
                    qty4 = (Convert.ToInt32(volume) + Convert.ToInt32(volume)).ToString();
                    qty5 = (Convert.ToDouble(volume)).ToString();
                    qty6 = 400.ToString();
                    qty7 = (Convert.ToDouble(volume)).ToString();

                    amount = (Convert.ToDouble(volume) * Convert.ToDouble(price)).ToString();
                    amount1 = (Convert.ToDouble(1) * Convert.ToDouble(rate1)).ToString();
                    amount2 = (Convert.ToDouble(1) * Convert.ToDouble(rate2)).ToString();
                    amount3 = (Convert.ToDouble(qty3) * Convert.ToDouble(rate3)).ToString();
                    amount4 = ((Convert.ToInt32(volume) + Convert.ToInt32(volume)) * Convert.ToDouble(rate4)).ToString();
                    amount5 = (Convert.ToDouble(volume) * Convert.ToDouble(rate5)).ToString();
                    amount6 = (400 * Convert.ToDouble(rate6)).ToString();
                    amount7 = (Convert.ToDouble(volume) * Convert.ToDouble(rate7)).ToString();

                    subtotal = Convert.ToDouble(Convert.ToDouble(amount) + Convert.ToDouble(amount1) + Convert.ToDouble(amount2) + Convert.ToDouble(amount3) + Convert.ToDouble(amount4) + Convert.ToDouble(amount5) + Convert.ToDouble(amount6) + Convert.ToDouble(amount7)).ToString();



                    double cameraamountgst18 = Convert.ToDouble(Convert.ToDouble(amount) * 18) / 100;

                    // cameradvr8channel gst18%
                    double ccameradvr8channelgst18 = Convert.ToDouble(Convert.ToDouble(amount1) * 18) / 100;

                    // two tb gst 18%
                    double twotbgst18 = Convert.ToDouble(Convert.ToDouble(amount2) * 18) / 100;

                    //installationcharge gst 18%
                    double installationcharge18 = Convert.ToDouble(Convert.ToDouble(amount7) * 18) / 100;

                    // total gst18% amount
                    double totalgst18amount = cameraamountgst18 + ccameradvr8channelgst18 + twotbgst18 + installationcharge18;

                    // calculating sgst9% 
                    double sgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;
                    //calculating cgst 9%
                    double cgst9percentageamount = Convert.ToDouble(totalgst18amount) / 2;

                    sgst9 = sgst9percentageamount.ToString();
                    cgst9 = cgst9percentageamount.ToString();

                    //  gst28% calculation

                    double poweradaptergst28amount = Convert.ToDouble(Convert.ToDouble(amount3) * 28) / 100;
                    double BNC28amount = Convert.ToDouble(Convert.ToDouble(amount4) * 28) / 100;
                    double powerconnectorsgst28amount = Convert.ToDouble(Convert.ToDouble(amount5) * 28) / 100;
                    double threeplusonegst28amount = Convert.ToDouble(Convert.ToDouble(amount6) * 28) / 100;

                    //total gst28% amount 

                    double totalgst28amount = poweradaptergst28amount + BNC28amount + powerconnectorsgst28amount + threeplusonegst28amount;

                    //calculating sgst14%
                    double sgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;
                    //calculating cgst14%
                    double cgst14percentageamount = Convert.ToDouble(totalgst28amount) / 2;


                    sgst14 = sgst14percentageamount.ToString();
                    cgst14 = cgst14percentageamount.ToString();
                    //Session["totalgst18amount"] = totalgst18amount;
                    //Session["totalgst28amount"] = totalgst28amount;

                    FixedAomount = Convert.ToDouble(Convert.ToDouble(totalgst18amount + totalgst28amount) + (Convert.ToDouble(subtotal))).ToString();

                }

                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
                // Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":[{\"Item\":\"" + product + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty + "\",\"rate\":\"" + price + "\",\"amount\":\"" + amount + "\"},{\"Item\":\"" + Label10 + "\",\"Description\":\"" + description1 + "\",\"quantity\":\"" + qty1 + "\",\"rate\":\"" + rate1 + "\",\"amount\":\"" + amount1 + "\"},{\"Item\":\"" + Label15 + "\",\"Description\":\"" + description2 + "\",\"quantity\":\"" + qty2 + "\",\"rate\":\"" + rate2 + "\",\"amount\":\"" + amount2 + "\"},{\"Item\":\"" + Label11 + "\",\"Description\":\"" + description3 + "\",\"quantity\":\"" + qty3 + "\",\"rate\":\"" + rate3 + "\",\"amount\":\"" + amount3 + "\"},{\"Item\":\"" + Label12 + "\",\"Description\":\"" + description4 + "\",\"quantity\":\"" + qty4 + "\",\"rate\":\"" + rate4 + "\",\"amount\":\"" + amount4 + "\"},{\"Item\":\"" + Label13 + "\",\"Description\":\"" + description5 + "\",\"quantity\":\"" + qty5 + "\",\"rate\":\"" + rate5 + "\",\"amount\":\"" + amount5 + "\"},{\"Item\":\"" + Label14 + "\",\"Description\":\"" + description6 + "\",\"quantity\":\"" + qty6 + "\",\"rate\":\"" + rate6 + "\",\"amount\":\"" + amount6 + "\"},{\"Item\":\"" + Label16 + "\",\"Description\":\"" + description7 + "\",\"quantity\":\"" + qty7 + "\",\"rate\":\"" + rate7 + "\",\"amount\":\"" + cameraamountgst18 + "\"}],\"amount\":[{\"key\":\"subtotal\",\"value\":" + subtotal + "},{\"key\":\"sgst9\",\"value\":" + sgst9 + "},{\"key\":\"cgst9%\",\"value\":" + cgst9 + "},{\"key\":\"sgst14%\",\"value\":" + sgst14 + "},{\"key\":\"cgst14%\",\"value\":" + cgst14 + "},{\"key\":\"totalamount\",\"value\":" + FixedAomount + "}]}");
            }

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        //string companyName = "Brihaspathi Technologies Pvt Ltd";
                        //int orderNo = 2303;





                        StringBuilder sb = new StringBuilder();
                        sb.Append("<h3 align='center' style='margin-bottom:5px;'><b>Estimate</b></h3>");

                        sb.Append("<table  border='0'>");
                        sb.Append("<tr style='border:none'>");
                        sb.Append("<td colspan='2'><h5 style='font-weight:bold;text-align:right;margin-bottom:50%'>Date:" + DateTime.Now.ToString("dd/MM/yyyy") + ".</h5></td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");


                        sb.Append("<table  border='0'>");
                        sb.Append("<tr style='border:none'>");
                        sb.Append("<td><h5 style='font-weight:bold'>Name:" + client_name + "</h5></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr style='border:none'>");
                        sb.Append("<td><h5 style='font-weight:bold'>Address:" + address + "</h5></td>");
                        sb.Append("</tr>");
                        sb.Append("<tr style='border:none'>");
                        sb.Append("<td><h5 style='font-weight:bold'>Email:" + email + "</h5></td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");



                        sb.Append("<table border='1' cellspacing='2' cellpadding='5'>");
                        sb.Append("<tr bgcolor='#cecece'>");

                        sb.Append("<th>");

                        sb.Append("Item");

                        sb.Append("</th>");

                        sb.Append("<th>");


                        sb.Append("Description");


                        sb.Append("</th>");
                        sb.Append("<th>");

                        sb.Append("Quantity");

                        sb.Append("</th>");


                        sb.Append("<th>");

                        sb.Append("Rate");

                        sb.Append("</th>");

                        sb.Append("<th>");

                        sb.Append("Amount");

                        sb.Append("</th>");
                        sb.Append("</tr>");






                        //ROW1
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'> " + product + " </td>");

                        sb.Append("<td style='font-size:9px'> " + product + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + volume + " </td>");
                        sb.Append("<td style='font-size:9px'> " + price + " </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'>  " + amount + "  </td>");

                        sb.Append("</tr>");

                        //ROW2
                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px;border: none;'>" + Label10 + " </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + description1 + "   </td>");

                        sb.Append("<td style='font-size:9px;border: none;'>  " + qty1 + "</td>");

                        sb.Append("<td style='font-size:9px;border: none;'>  " + rate1 + "  </td>");
                        sb.Append("<td style='font-size:9px;border: none;text-align:right'>  " + amount1 + "  </td>");

                        sb.Append("</tr>");
                        //ROW3

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + Label15 + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + description2 + "   </td>");

                        sb.Append("<td style='font-size:9px;width:30px'> " + qty2 + " </td>");

                        sb.Append("<td style='font-size:9px'> " + rate2 + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + amount2 + "   </td>");
                        sb.Append("</tr>");




                        //ROW4

                        if (Convert.ToInt32(volume) >= 21 || Convert.ToInt32(volume) >= 17)
                        {

                            sb.Append("<tr>");

                            sb.Append("<td style='font-size:9px'>" + exception4portpoe + "  </td>");

                            sb.Append("<td style='font-size:9px'> " + exception4portpoedesc + "   </td>");

                            sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception4port + " </td>");

                            sb.Append("<td style='font-size:9px'> " + exception4portpoerate + "   </td>");
                            sb.Append("<td style='font-size:9px;text-align:right'> " + exception4portpoeamount + "   </td>");
                            sb.Append("</tr>");
                        }
                        else
                        {

                        }


                        //ROW5
                        if (Convert.ToInt32(volume) >= 21 && Convert.ToInt32(volume) <= 32)

                        {

                            sb.Append("<tr>");

                            sb.Append("<td style='font-size:9px'>" + exception8portpoe + "  </td>");

                            sb.Append("<td style='font-size:9px'> " + exception8portpoedesc + "   </td>");

                            sb.Append("<td style='font-size:9px';width:30px'> " + qtyforexception8port + " </td>");

                            sb.Append("<td style='font-size:9px'> " + exception8portpoerate + "   </td>");
                            sb.Append("<td style='font-size:9px;text-align:right'> " + exception8portpoeamount + "   </td>");
                            sb.Append("</tr>");

                        }
                        else
                        {

                        }


                        //ROW6

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + Label12 + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + description4 + "   </td>");

                        sb.Append("<td style='font-size:9px'> " + qty4 + " </td>");

                        sb.Append("<td style='font-size:9px'> " + rate4 + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + amount4 + "   </td>");
                        sb.Append("</tr>");


                        //ROW6

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px;border: none;'>" + Label13 + "  </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + description5 + "   </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + qty5 + " </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + rate5 + "   </td>");
                        sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount5 + "   </td>");
                        sb.Append("</tr>");


                        //ROW5

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px'>" + Label14 + "  </td>");

                        sb.Append("<td style='font-size:9px'> " + description6 + "   </td>");

                        sb.Append("<td style='font-size:9px'> " + qty6 + " </td>");

                        sb.Append("<td style='font-size:9px'> " + rate6 + "   </td>");
                        sb.Append("<td style='font-size:9px;text-align:right'> " + amount6 + "   </td>");
                        sb.Append("</tr>");


                        //Installation charge

                        sb.Append("<tr>");

                        sb.Append("<td style='font-size:9px;border: none;'>" + Label16 + "  </td>");


                        sb.Append("<td style='font-size:9px;border: none;'> " + description7 + "   </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + qty7 + " </td>");

                        sb.Append("<td style='font-size:9px;border: none;'> " + rate7 + "   </td>");
                        sb.Append("<td style='font-size:9px;border: none;text-align:right'> " + amount7 + "   </td>");
                        sb.Append("</tr>");


                        sb.Append("</table>");



                        sb.Append("<table>");

                        sb.Append("<tr>");
                        sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SUB TOTAL</td>");
                        sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + subtotal + "</td>");

                        sb.Append("</tr>");






                        sb.Append("<tr>");
                        sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>SGST 9% </td>");
                        sb.Append("<td   style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst9 + "</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 9% </td>");
                        sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst9 + "</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>sgst14 %</td>");
                        sb.Append("<td  style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + sgst14 + "</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td  style='font-size:9px;padding-left:230px;font-weight:bold'>CGST 14 %</td>");
                        sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + cgst14 + "</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td style='font-size:9px;padding-left:230px;font-weight:bold'>Fixed Amount</td>");

                        sb.Append("<td style='font-size:9px;padding-left:0px;text-align:right;font-weight:bold'> " + FixedAomount + "</td>");

                        sb.Append("</tr>");

                        sb.Append("</table>");

                        sb.Append("<table>");
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append("<p style='font-size:4px'>");
                        sb.Append("<b style='font-size:20px'>Terms and Conditions</b>");
                        sb.Append("<h6>Payment Terms:70% advance with p.o and balance 30% on delivery of goods and installation</h6>");

                        sb.Append("<h6>Warranty:One Year</h6>");
                        sb.Append("<h6>Delivery:One Week from the date of P.O With  70% Advance payment</h6>");
                        sb.Append("<h6>Quotation validty:Two weeks from the </h6>");
                        sb.Append("<h6>Any Civil/Electrical Work will be done by you at your cost as per  our  specifications.</h6>");
                        sb.Append("<h6> Power Point/Supply at every location to be provided.</h6>");
                        sb.Append("<h6>Cable should be as per actual and extra charges during installation should be paid by customer.</h6>");
                        sb.Append("</p>");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");

                        StringReader sr = new StringReader(sb.ToString());




                        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            /*string path = Server.MapPath("Files");
                            string filename = path + "/" + "Quickquotation" + ".Pdf";*/

                                string z = client_name + "_" + DateTime.Now.ToString("dd/MM/yyyy");
                                string path = Server.MapPath("Files");
                                string filename = path + "/" + z + ".pdf";
                                i = "http://emp.bterp.in/files/" + z + ".pdf";


                              PdfWriter.GetInstance(pdfDoc, new FileStream(filename, FileMode.Create));
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            pdfDoc.Open();
                            htmlparser.Parse(sr);


                            string imagePath = Server.MapPath("topp-hyd.jpg") + "";

                            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);

                            image.Alignment = Element.ALIGN_TOP;

                            image.SetAbsolutePosition(30, 750);

                            image.ScaleToFit(550f, 550f);
                            image.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_TOP;

                            pdfDoc.Add(image);






                            PdfContentByte content = writer.DirectContent;
                            iTextSharp.text.Rectangle rectangle = new iTextSharp.text.Rectangle(pdfDoc.PageSize);
                            rectangle.Left += pdfDoc.LeftMargin;
                            rectangle.Right -= pdfDoc.RightMargin;
                            rectangle.Top -= pdfDoc.TopMargin;
                            rectangle.Bottom += pdfDoc.BottomMargin;
                           // content.SetColorStroke(BaseColor.BLACK);
                            //content.Rectangle(rectangle.Left, rectangle.Bottom, rectangle.Width, rectangle.Height);
                            content.Stroke();

                            pdfDoc.Close();



                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();




                            MailMessage mm = new MailMessage("trainee@brihaspathi.com",email);
                            mm.Subject = "Quotation";
                            mm.Body = "Quick Quotation";
                            mm.Attachments.Add(new Attachment(new MemoryStream(bytes),"Quotation.pdf"));

                            mm.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential();
                            NetworkCred.UserName = "trainee@brihaspathi.com";
                            NetworkCred.Password = "yourself123";
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            smtp.Send(mm);
                            Response.Write("{\"error\": false ,\"message\":\"Mail sent Successfully\"}");


                    }

                }
                }
            }
        

        #endregion

        #region localmethod

        public void sendmail(string email, string OTP, string subject)
        {
     
            MailMessage mail = new MailMessage("trainee@brihaspathi.com", email, subject, OTP);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.Credentials = new System.Net.NetworkCredential("trainee@brihaspathi.com", "yourself123");
            client.EnableSsl = true;
            client.Send(mail);

        }
  
        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        #endregion

       public void hr_tokens(string reason) 
        {        
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where deptid='19' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter<tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Notification obj = new Notification();
            obj.title = "Attendance Request";
            obj.body = "New Attendance Request Recieved";
            obj.image = "";
            obj.code = 4;
            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, obj);

        }      
    }
}

