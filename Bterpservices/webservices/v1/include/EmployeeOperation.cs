﻿using inventory.DataAccessLayer;
using System;
using System.Data;
using System.Web;
using inventory.webservices.v1.model;
using System.ComponentModel;
using System.Drawing;
using System.Net.Mail;
using System.Collections.Generic;
using inventory.InventoryHelper;

namespace inventory.webservices.v1.include
{
   class EmployeeOperation
    {
        public EmployeeItem getEmployee(int empID)
        {
            EmployeeItem employee = new EmployeeItem();

            DataSet ds = DataQueries.SelectCommon("select a.EmployeeId, a.DOJ, a.EmployeName, a.DeptId, a.cellno, a.Desigantion, a.CompanyEmail, b.Department from Employees a inner join Department b on a.DeptId=b.DeptId where a.EmployeeId='" + empID + "'");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                employee.id = empID;
                employee.name = ds.Tables[0].Rows[0]["EmployeName"].ToString().Trim();
                employee.phone = ds.Tables[0].Rows[0]["cellno"].ToString().Trim();
                employee.email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                employee.department = ds.Tables[0].Rows[0]["department"].ToString().Trim();
                employee.designation = ds.Tables[0].Rows[0]["Desigantion"].ToString().Trim();
                employee.doj = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim();
                employee.departmentID = Convert.ToInt32(ds.Tables[0].Rows[0]["DeptId"].ToString().Trim());

            }

            return employee;
        }
        
        public EmployeeItem authentication(int empID, string password, string token)
        {

           DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.DOJ,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department,c.password,C.OTP  from Employees a inner join Department b on a.DeptId=b.DeptId inner join register c on a.CompanyEmail=c.Admin where a.EmployeeId='"+empID+"' and c.password='"+password+"'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                updateToken(empID, token);
                return getEmployee(empID);
            }
            else
            {
                return new EmployeeItem();
            }
        }
        
        public void updateToken(int empID, string token)
        {
            InventoryHelper.Inventroy_Queries.UpdateCommon("update employees set token='" + token + "' where EmployeeId='" + empID + "'");
        }

        public Response profileimage(int empID)
        {

            DataSet ds = DataQueries.SelectCommon("select [EmpImage] from employees where EmployeeId='" + empID + "'");

            Response response = new Response();

            if (ds.Tables[0].Rows.Count > 0)
            {
                byte[] imgBytes = (byte[])ds.Tables[0].Rows[0]["EmpImage"];

                // If you want convert to a bitmap file
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(imgBytes);
                string imgString = Convert.ToBase64String(imgBytes);

                response.errorCode = 200;
                response.message = imgString;
             // Response.Write("{\"error\": false ,\"message\":\"Success\",\"Photo\":\"" + imgString + "\" }");

            }
            else
            {
                response.errorCode = 101;
                response.message = "Image not found";
            }
            return response;
        }

        public Response forgotPassword(int empID)
        {
            Response response = new Response();

            if (isValidEmployeeID(empID))
            {
                DataSet ds = DataQueries.SelectCommon("select a.EmployeeId,a.EmployeName,a.DeptId,a.cellno,a.Desigantion,a.CompanyEmail,b.Department,c.password,C.OTP  from Employees a inner join Department b on a.DeptId=b.DeptId inner join register c on a.CompanyEmail=c.Admin where a.EmployeeId='" + empID + "'");

                if (ds.Tables[0].Rows.Count != 0)
                {
                    string email = ds.Tables[0].Rows[0]["CompanyEmail"].ToString().Trim();
                    Random random = new Random();
                    int OTP = random.Next(100000, 999999);
                    DataQueries.InsertCommon("update register set otp='" + OTP + "' where name='" + empID + "'");
                    sendmail(email, "Your OTP To set password is '" + OTP + "' ", "OTP");
                   
                    response.errorCode = 200;
                    response.message = email;
                }
                else
                {
                    response.errorCode = 2;
                    response.message = "Email ID not available";
                }
            }
            else
            {
                response.errorCode = 1;
                response.message = "Employee ID not available";
            }

            return response;
        }

        public Response createPassword(int empID, string password)
        {
            Response response = new Response();

            if (isValidEmployeeID(empID))
            { 
                DataQueries.InsertCommon("update register set password='" + password + "' where name='" + empID + "'");
                response.errorCode = 200;
                response.message = "Password updated successfully";
            }
            else
            {
                response.errorCode = 1;
                response.message = "Employee ID not available";
            }

            return response;
        }

        public List<EmployeeItem> getEmployeeList()
        {
            List<EmployeeItem> employeeList = new List<EmployeeItem>();

            DataSet ds = DataQueries.SelectCommon("select [EmployeeId] from [attendance].[dbo].[Employees] where [status]='1' order by EmployeeId ");
            DataTable dt = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int empID = Convert.ToInt32(dt.Rows[i]["EmployeeId"].ToString().Trim());
                    employeeList.Add(getEmployee(empID));
                }
            }

            return employeeList;
        }

        private Boolean isValidEmployeeID(int empID)
        {
            DataSet ds = DataQueries.SelectCommon("select EmployeeId from Employees where EmployeeId='" + empID + "'");

            return (ds.Tables[0].Rows.Count > 0);
        }

        public Boolean isValidOTP(int empID, string otp)
        {
            DataSet ds = DataQueries.SelectCommon("select  otp from register where Name='" + empID + "'");

            string otpdb = ds.Tables[0].Rows[0]["otp"].ToString().Trim();
            return otp == otpdb;
        }

        public string getEmployeeToken(int empID)
        {
            DataSet ds = Inventroy_Queries.SelectCommon("select token from Employees where EmployeeId ='" + empID + "' and status='1'");

            string token = "";

            if (ds.Tables[0].Rows.Count > 0)
            {
                token = ds.Tables[0].Rows[0]["token"].ToString().Trim();
            }

            return token;
        }

        public string[] getHRTokens()
        {
            DataSet ds = Inventroy_Queries.SelectCommon("select token from Employees where deptid='19' and status='1'");

            //Empty array
            string[] tokens = new string[ds.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < ds.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                tokens[loopcounter] = ds.Tables[0].Rows[loopcounter]["token"].ToString();
            }

            return tokens;
        }

        private void sendmail(string email, string OTP, string subject)
        {

            MailMessage mail = new MailMessage("trainee@brihaspathi.com", email, subject, OTP);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.Credentials = new System.Net.NetworkCredential("trainee@brihaspathi.com", "yourself123");
            client.EnableSsl = true;
            client.Send(mail);

        }

    }
}
