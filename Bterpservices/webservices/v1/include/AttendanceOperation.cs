﻿using inventory.DataAccessLayer;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using inventory.InventoryHelper;
using inventory.webservices.v1.include;
using inventory.webservices.v1.model;
using System.Web.Script.Serialization;

namespace inventory.webservices.v1.include
{
    public class AttendanceOperation
    {

        public AttendanceRequest getAttendanceRequest(int empID)
        {
          
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee = operation.getEmployee(empID);

            AttendanceRequest request = new AttendanceRequest();
            request.employee = employee;

            string date = DateTime.Now.ToString("yyyy/MM/dd");
            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + empID + "' and convert(nvarchar(10),Time,126)='" + date + "'");

            if (ds.Tables[0].Rows.Count == 1)
            {

                string reason = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                string time = ds.Tables[0].Rows[0]["time"].ToString().Trim();
                int status = Convert.ToInt32(ds.Tables[0].Rows[0]["status"].ToString().Trim());
                double latitude = Convert.ToDouble(ds.Tables[0].Rows[0]["latitude"].ToString().Trim());
                double longitude = Convert.ToDouble(ds.Tables[0].Rows[0]["longitude"].ToString().Trim());
                string image = ds.Tables[0].Rows[0]["image"].ToString().Trim();

                request.reason = reason;
                request.time = time;
                request.image = image;
                request.status = status;
                request.latitude = latitude;
                request.longitude = longitude;
            }

            return request;
        }

        public AttendanceRequest setAttendanceRequest(int empID, string reason, string latitude, string longitude, string base64Image)
        {
            AttendanceRequest result = new AttendanceRequest();

            if (!isAlreadyRequest(empID))
            {
             
               /* string date = DateTime.Now.ToString("yyyy/MM/dd");
                string date12 = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");

                string filename = empID + "_" + date;
                byte[] bytes1 = Convert.FromBase64String(base64Image);
                File.WriteAllBytes(Server.MapPath("~/webservices/attendanceimages/") + filename + ".jpg", bytes1); //write to a temp location.
                string path = "http://emp.bterp.in/attendanceimages/" + filename + ".jpg";

                DataQueries.InsertCommon("insert into RequestAttendance (Reason, Time, Employee_Id, Latitude, Longitude, Markby, status, image) values ('" + reason + "','" + System.DateTime.Now.ToString() + "','" + empID + "','" + latitude + "','" + longitude + "','" + empID + "','0','" + path.ToString() + "')");

                result = getAttendanceRequest(empID);

                EmployeeOperation empOperation = new EmployeeOperation();
                EmployeeItem employee = result.employee;

                string[] tokens = empOperation.getHRTokens();

                NotificationOperation notiOperation = new NotificationOperation();
                NotificationItem notification = new NotificationItem();
                notification.title = "Attendance request receive";
                notification.body = employee.name + " send request to mark attendance.\n Reason: " + result.reason;
                notification.code = 2;

                notiOperation.sendMultipleNotification(tokens, notification);*/
            }
            else
            {
                result = getAttendanceRequest(empID);
            }

            return result;
        }

        public Response markAttendance(int empID, int hrID)
        {
            Response response = new Response();

            string date = DateTime.Now.ToString("yyyy/MM/dd");
            
            string date1 = DateTime.Now.ToString("yyyy-MM-dd");
            string aa = " 09:30";

            String a = date1 + aa;
            DataQueries.InsertCommon("insert into tblattendance (UserId,DateOfTransaction,OuiDoor) values ('" + empID + "','" + a + "','OutDoorEntry')");
            DataQueries.InsertCommon("update  RequestAttendance set status='1', markby='" + hrID + "' where Employee_Id='" + empID + "' and convert(nvarchar(10),Time,126)='" + date + "'");

            response.errorCode = 200;
            response.message = "Attendance marked successfully.";

            EmployeeOperation empOperation = new EmployeeOperation();
            string token = empOperation.getEmployeeToken(empID);

            NotificationOperation notiOperation = new NotificationOperation();
            NotificationItem notification = new NotificationItem();
            notification.title = "Attendance request status";
            notification.body = "Your attendance marked successfully";
            notification.code = 1;

            notiOperation.sendSingleNotification(token, notification);

            return response;
        }

        public List<AttendanceRequest> getRequestList()
        {
            List<AttendanceRequest> requestList = new List<AttendanceRequest>();

            string date = DateTime.Now.ToString("yyyy/MM/dd");
            DataSet ds = DataQueries.SelectCommon("select Employee_Id from RequestAttendance where convert(nvarchar(10),Time,126)='" + date + "'");
            DataTable dt = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int empID = Convert.ToInt32(dt.Rows[i]["EmployeeId"].ToString().Trim());
                    requestList.Add(getAttendanceRequest(empID));
                }
            }
            
            return requestList;
        }

        private bool isAlreadyRequest(int empID)
        {
            string date = DateTime.Now.ToString("yyyy/MM/dd");
            DataSet ds = DataQueries.SelectCommon("select * from RequestAttendance where Employee_Id='" + empID + "' and convert(nvarchar(10),Time,126)='" + date + "'");
            return ds.Tables[0].Rows.Count == 1;
        }
    }
}