﻿using System;
using inventory.webservices.v1.model;
using System.Web.Script.Serialization;
using System.Net;
using System.Text;
using System.IO;

namespace inventory.webservices.v1.include
{
    public class NotificationOperation
    {

        public void sendSingleNotification(string deviceId, NotificationItem noti)
        {

            var jsonData = new
            {
                to = deviceId,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendTopicNotification(string topic, NotificationItem noti)
        {

            var jsonData = new
            {
                to = "/topics/" + topic,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };
            
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendMultipleNotification(string[] registrationIds, NotificationItem noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }

        public void PushNotification(string json)
        {
            try
            {
                var applicationID = "AAAARIXWWH4:APA91bEgoIt92AN_r0d7fvKF_QV4HF0QeJ-EOPrCqr0aWWU3bryUY6LTOPk3ZAPQniuDc1ioMz18TmC_SWY8D-NE26OAbHc4DDi456gEnN-k7_4xvvY7C4pmBTZirPMDfNeIe5Qp_3tl";

                var senderId = "294303193214";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";
                
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                string str = ex.Message;
            }

        }
    }
}