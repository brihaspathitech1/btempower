﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.webservices.v1.model
{
    public class EmployeeItem
    {
        public int id { get; set; }

        public string name { get; set; }

        public string designation { get; set; }

        public string phone { get; set; }

        public string email { get; set; }

        public string dob { get; set; }

        public string doj { get; set; }

        public string department { get; set; }

        public int departmentID { get; set; }
    }
}