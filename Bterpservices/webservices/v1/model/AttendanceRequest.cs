﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace inventory.webservices.v1.model
{
    public class AttendanceRequest
    {
        public EmployeeItem employee { get; set; }

        public string reason { get; set; }

        public string image { get; set; }

        public string time { get; set; }

        public double latitude { get; set; }

        public double longitude { get; set; }

        public int status { get; set; }
    }
}