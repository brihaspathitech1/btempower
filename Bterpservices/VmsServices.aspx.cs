﻿using System;
using System.Web;
using System.Web.UI;
using inventory.DataAccessLayer;
using System.IO;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using json;
using JsonServices;
using JsonServices.Web;
using Newtonsoft.Json;
using System.Net;
using inventory.webservices.v1.model;
using inventory.webservices.v1.include;
using inventory.InventoryHelper;
using System.Net.Mail;
using System.Net.Mime;
using RestSharp;


namespace Bterpservices
{
    public partial class VmsServices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.HttpMethod == "POST")
            {
                try
                {

                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "login":
                            login();
                            break;
                        case "addvisitor":
                            addvisitor();
                            break;
                        case "vmsbaner":
                            vmsbanner();
                            break;
                        case "department":
                            department();
                            break;
                        case "employees":
                            employees();
                            break;

                        case "visitor_list":
                            visitor_list();
                            break;
                        case "Recentvisitor_list":
                            Recentvisitor_list();
                            break;

                        case "visitor_qrscan":
                            visitor_qrscan();
                            break;

                        case "dayreport":
                            dayreport();
                            break;

                        case "twodates":
                            twodates();
                            break;
                        case "empnumber":
                            empnumber();
                            break;

                        case "courieroutward":
                            courieroutward();
                            break;
                        case "SaveOTP":
                            saveotp();
                            break;
                        case "verify_otp":
                            verify_otp();
                            break;
                        case "resendotp":
                            resend_otp();
                            break;
                        case "CourierInward":
                            CourierInward();
                            break;
                        case "outwarddisplay":
                            outwarddisplay();
                            break;
                        case "Inwarddisplay":
                            Inwarddisplay();
                            break;
                        case "employeenames":
                            employeenames();
                            break;
                        case "visitorinlist":
                            visitorscheckinlist();
                            break;
                        case "visitorscheckout":
                            visitorscheckout();
                            break;
                        case "databindbasedonmobile":
                            datafillbasedonmobile();
                            break;
                        case "notifylist":
                            notifylist();
                            break;
                        case "notifytosupport":
                            notifytosupport();
                            break;
                        case "rejectednotify":
                            notifytosupportifrejected();
                            break;
                        case "visitorrating":
                            ratingupdate();
                            break;
                        case "cardiddisplay":
                            cardiddisplay();
                            break;
                        case "listdisplay":
                            visitorslistdisplay();
                            break;

                        case "mailtest":
                            mailtest();
                            break;
                        case "token":
                            token();
                            break;
                        case "bn":
                            timelapsenotification();
                            break;
                        case "datewisevisitorslist":
                            datewisevisitorslist();
                            break;
                        case "addvisit":
                            addvisit();
                            break;
                        case "signatureim":
                            signatureim();
                            break;
                        case "displayvisitors":
                            displayvisitors();
                            break;
                        case "addvisittest":
                            addvisittest();
                            break;
                        case "signatureimtest":
                            signatureimtest();
                            break;
                        case "displayvisitorstest":
                            displayvisitorstest();
                            break;
                        case "visitorinlisttest":
                            visitorscheckinlisttest();
                            break;
                        case "visitorscheckouttest":
                            visitorscheckouttest();
                            break;
                        case "notifylisttest":
                            notifylisttest();
                            break;
                        case "notifytosupporttest":
                            notifytosupporttest();
                            break;
                        case "rejectednotifytest":
                            notifytosupportifrejectedtest();
                            break;
                        case "testnoti":
                            testnoti();
                            break;
                        case "vtest":
                            vtest();
                            break;
                        case "signatureimdemo":
                            signatureimdemo();
                            break;
                        case "addvisitdemo":
                            addvisitdemo();
                            break;
                        case "visitorscheckoutdemo":
                            visitorscheckoutdemo();
                            break;
                        case "trendytv":
                            trendytv();
                            break;
                        case "trendyreg":
                            trendyreg();
                            break;
                        case "pinverify":
                            pinverify();
                            break;
                        case "addcomments":
                            addcomments();
                            break;
                        case "commentsdisplay":
                            commentsdisplay();
                            break;
                        case "trendylikes":
                            trendylikes();
                            break;
                        case "views":
                            views();
                            break;
                        case "viewscount":
                            viewscount();
                            break;
                        case "forgotpin":
                            forgotpin();
                            break;
                        case "likedvideos":
                            likedvideos();
                            break;
                        case "history":
                            history();
                            break;
                        case "profile":
                            profile();
                            break;
                        case "recentvideos":
                            recentvideos();
                            break;
                        case "shortfilms":
                            shortfilms();
                            break;
                        case "webseries":
                            webseries();
                            break;
                        case "interviews":
                            interviews();
                            break;
                        case "askquestions":
                            askquestions();
                            break;
                        case "recentviewlist":
                            recentviewlist();
                            break;
                    }


                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }
        public void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string ptime = DateTime.Now.ToString("hh:mm");
            string time = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select CONVERT(VARCHAR(5),date,108) as intime from visitors where outtime is null order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                time = ds.Tables[0].Rows[0]["intime"].ToString().Trim();


            }

            TimeSpan dt = Convert.ToDateTime(ptime) - Convert.ToDateTime(time);
            int minutes = (int)dt.TotalMinutes;
            int hr = (int)dt.TotalHours;
            string d = Convert.ToString(minutes);
            //Notification notification1 = new Notification();
            //notification1.title = d ;
            //notification1.body = "msg";
            //notification1.code = 13;
            //test_token(notification1);


        }

        protected void timelapsenotification()
        {
            string ptime = DateTime.Now.ToString("hh:mm");
            string time = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select CONVERT(VARCHAR(5),date,108) as intime from visitors where  id='5137' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                time = ds.Tables[0].Rows[0]["intime"].ToString().Trim();


            }

            TimeSpan dt = Convert.ToDateTime(ptime) - Convert.ToDateTime(time);
            int minutes = (int)dt.TotalMinutes;
            int hr = (int)dt.TotalHours;
            string d = Convert.ToString(minutes);
            string h = Convert.ToString(hr);
            Response.Write(h);
        }
        protected void token()
        {
            string token = Request.Params["token"].ToString();
            string empid = Request.Params["empid"].ToString();
            DataQueries.InsertCommon1("insert into token(empid,token) values ('" + empid + "','" + token + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }



        protected void login()
        {
            string uname = Request.Params["username"].ToString().Trim();
            string password = Request.Params["password"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon1("select * from AdminLogin where username='" + uname + "' and password='" + password + "' and role='reception'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string userid = ds.Tables[0].Rows[0]["id"].ToString().Trim();
                string role = ds.Tables[0].Rows[0]["role"].ToString().Trim();
                string email = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"id\":\"" + userid + "\",\"email\":\"" + email + "\",\"mobile\":\"" + mobile + "\",\"role\":" + role + "}");
            }
        }



        private double newvid()
        {

            DataSet ds1 = DataQueries.SelectCommon1("select isnull(max(vid),0) from Visitors ");
            double vid = 100;
            if (Convert.ToString(ds1.Tables[0].Rows[0][0]) != "")
            {
                vid = double.Parse(ds1.Tables[0].Rows[0][0].ToString());
                vid = vid + 1;
            }
            return vid;

        }
        private double newvidtest()
        {

            DataSet ds1 = DataQueries.SelectCommon1("select isnull(max(vid),0) from Visitorstest ");
            double vid = 100;
            if (Convert.ToString(ds1.Tables[0].Rows[0][0]) != "")
            {
                vid = double.Parse(ds1.Tables[0].Rows[0][0].ToString());
                vid = vid + 1;
            }
            return vid;

        }

        private double card()
        {
            string no = string.Empty;
            double cardid = 0;
            string date = string.Empty;

            DataSet ds11 = DataQueries.SelectCommon1("select top 1 convert(varchar,date,105) as date from visitors order by id desc");
            if (ds11.Tables[0].Rows.Count > 0)
            {
                date = ds11.Tables[0].Rows[0]["date"].ToString().Trim();

            }

            if (date != DateTime.Now.ToString("dd-MM-yyy"))
            {
                cardid = 1;
            }

            else
            {

                DataSet ds1 = DataQueries.SelectCommon1("select top 1 cardid from visitors order by id desc");
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    no = ds1.Tables[0].Rows[0]["cardid"].ToString().Trim();

                }
                double d = Convert.ToDouble(no);

                if (d < 10)
                {
                    cardid = d;
                    cardid = cardid + 1;
                }
                else
                {
                    DataSet ds111 = DataQueries.SelectCommon1("select  cardid from cardids order by cardid asc");
                    if (ds111.Tables[0].Rows.Count > 0)
                    {
                        no = ds111.Tables[0].Rows[0]["cardid"].ToString().Trim();


                    }
                    double dd = Convert.ToDouble(no);
                    cardid = dd;
                    DataQueries.DeleteCommon1("delete from cardids where cardid='" + no + "'");
                }
            }

            return cardid;

        }

        protected void vmsbanner()
        {
            DataSet ds = DataQueries.SelectCommon1("select id,content from tblBanner");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void addvisitor()
        {
            //string empid = string.Empty;
            //string empmobile = string.Empty;
            //double vid = newvid();


            //string Name = Request.Params["Name"].ToString().Trim();
            //string Mobile = Request.Params["Mobile"].ToString().Trim();
            //string Email = Request.Params["Email"].ToString().Trim();
            //string Address = Request.Params["Address"].ToString().Trim();
            //string WhoomToMeet = Request.Params["WhoomToMeet"].ToString();
            //string Purpose = Request.Params["Purpose"].ToString().Trim();
            ////string department = Request.Params["department"].ToString().Trim();
            ////string asset = Request.Params["asset"].ToString().Trim();
            //double cardid = card();
            //string Photo = Request.Params["Photo"].ToString().Trim();
            //string signatureimage = Request.Params["signature"].ToString();
            //string base644 = signatureimage;

            //byte[] bytes11 = Convert.FromBase64String(base644);
            //File.WriteAllBytes(Server.MapPath("~/sign/") + vid + ".jpg", bytes11); //write to a temp location.
            //string pathh = "http://emp.bterp.in/sign/" + vid + ".jpg";
            //string base64 = Photo;
            //byte[] bytes1 = Convert.FromBase64String(base64);
            //File.WriteAllBytes(Server.MapPath("~/Captures/") + vid + ".jpg", bytes1); //write to a temp location.
            //string path = "http://emp.bterp.in/Captures/" + vid + ".jpg";
            //String result = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt");

            //DataSet dss = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + WhoomToMeet + "'");
            //if (dss.Tables[0].Rows.Count > 0)
            //{
            //    empmobile = dss.Tables[0].Rows[0]["mobile"].ToString().Trim();
            //    empid = dss.Tables[0].Rows[0]["empid"].ToString().Trim();
            //}

            //string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            //SqlConnection conn1 = new SqlConnection(connstrg1);
            //String insert = "insert into Visitors(Name, Mobile, Email, Address, Photo, Purpose, date, WhoomToMeet, vid, status, status1,cardid,empid,cpid,signatureimage)values('" + Name.Replace("'", "''") + "', '" + Mobile.Replace("'", "''") + "', '" + Email.Replace("'", "''") + "', '" + Address.Replace("'", "''") + "','" + path + "', '" + Purpose.Replace("'", "''") + "', '" + result.Replace("'", "''") + "', '" + WhoomToMeet + "', '" + vid + "', 'arrive','1','" + cardid + "','" + empid + "','1','" + pathh + "')";
            //SqlCommand comm1 = new SqlCommand(insert, conn1);
            //conn1.Open();
            //comm1.ExecuteNonQuery();
            //conn1.Close();

            //DataSet ds = DataQueries.SelectCommon1("select vid,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,signatureimage from Visitors where  vid='" + vid + "'");
            //if (ds.Tables[0].Rows.Count == 1)
            //{
            //    string visitorid = ds.Tables[0].Rows[0]["vid"].ToString().Trim();

            //    string intime = ds.Tables[0].Rows[0]["intime"].ToString().Trim();
            //    string date = ds.Tables[0].Rows[0]["date"].ToString().Trim();
            //    string photoo = ds.Tables[0].Rows[0]["signatureimage"].ToString().Trim();

            //    Response.Write("{\"error\": false ,\"message\":\"Success\",\"visitorid\":\"" + visitorid + "\",\"intime\":\"" + intime + "\",\"date\":" + date + ",\"signatureimage\":\"" + photoo + "\"}");
            //}

            ////to display notification in btempower app
            //int empID = Convert.ToInt32(empid);
            //EmployeeOperation operation = new EmployeeOperation();
            //EmployeeItem employee1 = operation.getEmployee(empID);
            //Notification notification1 = new Notification();
            //notification1.title = "Visitor ";
            //notification1.body = Name + " has visited to meet you regarding " + Purpose;
            //notification1.code = 13;
            //hr_tokens(empid, notification1);
            //hr_tok(empid, notification1);
            //visitnotify(empid, notification1);
            ////sending sms
            //WebRequest request = null;
            //HttpWebResponse response = null;
            //String userid = "BTRAK";
            //String passwd = "841090";
            //string smstext = "Dear " + employee1.name + ", This is to inform you that " + Name + " has visited to meet you regarding " + Purpose + " from " + Address + "";
            //String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + empmobile + "&text=" + smstext + "";
            //request = WebRequest.Create(url);
            //response = (HttpWebResponse)request.GetResponse();


        }

        protected void mailtest()
        {
            string vid = Request.Params["vid"].ToString();
            string photo = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select photo from Visitors where  vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                photo = ds.Tables[0].Rows[0]["photo"].ToString().Trim();
            }
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("dotnettrainee1@brihaspathi.com", "visitor");
            mail.To.Add("dotnettrainee1@brihaspathi.com");

            mail.IsBodyHtml = true;
            mail.Subject = "Imagetest";
            string mailbody = "<br/><img src=" + photo + ">";
            //string mailbody = "<br/><img src=" +photo ;
            mail.Body = "" + mailbody + "";

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new System.Net.NetworkCredential("dotnettrainee1@brihaspathi.com", "prathyusha@bt");
            smtp.EnableSsl = true;
            smtp.Send(mail);

        }

        private void visitnotify(string employeeID, Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from tokenstbl where empid='" + employeeID + "'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
                Message msg = new Message();
                msg.sendMultipleNotification(arrvalues, notification);
            }
            //return arrvalues;



        }


        private void hr_tokens(string employeeID, Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='" + employeeID + "' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }

        private void hr_tok(string employeeID, Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='" + employeeID + "' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();
            }
            //return arrvalues;


            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        protected void visitorscheckinlist()
        {
            DataSet ds = DataQueries.SelectCommon1("select name,photo,vid from visitors where outtime is null and CONVERT(varchar(50),(CONVERT(datetime,date,110)),105)='" + DateTime.Now.ToString("dd-MM-yyyy") + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void visitorscheckout()
        {
            string vid = Request.Params["vid"].ToString();
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update Visitors set outtime='" + DateTime.Now.ToString("MM-dd-yyy hh:mm:ss tt") + "',Status='Sign Off' where vid='" + vid + "'";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            string name = string.Empty;
            string mob = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select name,mobile from visitors where vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                mob = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
            }
            string cid = string.Empty;
            DataSet dss = DataQueries.SelectCommon1("select cardid from visitors where vid='" + vid + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                cid = dss.Tables[0].Rows[0]["cardid"].ToString().Trim();
            }
            DataQueries.InsertCommon1("insert into cardids(vid,cardid) values ('" + vid + "', '" + cid + "')");
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Dear " + name + ", Thank you for visiting  Brihaspathi Technologies Pvt Ltd. It was a pleasure meeting you .";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mob + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();


            //var client = new RestClient("https://api.myvaluefirst.com/psms/servlet/psms.JsonEservice");
            //var requestt = new RestRequest(Method.POST);
            //requestt.AddHeader("postman-token", "kjbbf104232321f4400100-4xmBRIHASPATI");
            //requestt.AddHeader("cache-control", "no-cache");
            //requestt.AddHeader("content-type", "application/json");
            //requestt.AddParameter("application/json", "{\"@VER\":\"1.2\", \"USER\":{\"@USERNAME\":\" brihaspatiWA\",\"@PASSWORD\":\"brih9876\",\"@UNIXTIMESTAMP\":\"\"}, \"DLR\":{\"@URL\":\"\"},\"SMS\":[ {\"@UDH\": \"0\",\"@CODING\": \"1\",\"@TEXT\": \"\",\"@TEMPLATEINFO\": \"188379~" + name + "~" + " our stall . For any queries contact us at 9989993242 from Brihaspathi Technologies Pvt Ltd" + "\",\"@PROPERTY\": \"0\",\"@ID\": \"2\", \"ADDRESS\": [{\"@FROM\": \"919268622222\",\"@TO\": \" " + mob + "\",\"@SEQ\": \"1\",\"@TAG\": \"some clientside random data\"}]}]}", ParameterType.RequestBody);
            //IRestResponse responsee = client.Execute(requestt);
            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }

        protected void visitorscheckoutdemo()
        {
            string vid = Request.Params["vid"].ToString();
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update Visitors set outtime='" + DateTime.Now.ToString("MM-dd-yyy hh:mm:ss tt") + "',Status='Sign Off' where vid='" + vid + "'";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            string name = string.Empty;
            string mob = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select name,mobile from visitors where vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                mob = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
            }
            string cid = string.Empty;
            DataSet dss = DataQueries.SelectCommon1("select cardid from visitors where vid='" + vid + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                cid = dss.Tables[0].Rows[0]["cardid"].ToString().Trim();
            }
            DataQueries.InsertCommon1("insert into cardids(vid,cardid) values ('" + vid + "', '" + cid + "')");

            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }
        protected void visitorscheckinlisttest()
        {
            DataSet ds = DataQueries.SelectCommon1("select name,photo,vid from visitorstest where outtime is null and CONVERT(varchar(50),(CONVERT(datetime,date,110)),105)='" + DateTime.Now.ToString("dd-MM-yyyy") + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void visitorscheckouttest()
        {
            string vid = Request.Params["vid"].ToString();
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update Visitorstest set outtime='" + DateTime.Now.ToString("MM-dd-yyy hh:mm:ss tt") + "',Status='Sign Off' where vid='" + vid + "'";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            string name = string.Empty;
            string mob = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select name,mobile from visitorstest where vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                mob = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
            }

            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Dear " + name + ", Thank you for visiting Brihaspathi Technologies. It was a pleasure meeting you .";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mob + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }

        protected void department()
        {
            DataSet ds = DataQueries.SelectCommon1("select dept from tblDepartment");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"departments\": " + JSONresult + "}";
            Response.Write(result);
        }



        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }



        protected void employees()
        {
            string name = Request.Params["dname"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select e.name from tblEmployee e inner join tblDepartment d on e.dept=d.id where d.dept='" + name + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"employees\": " + JSONresult + "}";
            Response.Write(result);
        }
        protected void empnumber()
        {
            string mobile = string.Empty;
            string id = string.Empty;
            string name = Request.Params["WhoomToMeet"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select empid,mobile from tblEmployee where name='" + name + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                mobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
                id = ds.Tables[0].Rows[0]["empid"].ToString().Trim();
            }

            Response.Write("{\"error\": false,\"mobile\":" + mobile + ",\"empid\":" + id + "}");

        }


        protected void visitor_list()
        {
            DataSet ds = DataQueries.SelectCommon1("select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset from Visitors where CONVERT(varchar(50),(CONVERT(datetime,date,110)),105)=CONVERT(varchar(50),(CONVERT(datetime,GETDATE(),110)),105) ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void Recentvisitor_list()
        {
            DataSet ds = DataQueries.SelectCommon1("with cte as ( select *,(right(CONVERT(VARCHAR, date, 120), 8)) as date1,(right(CONVERT(VARCHAR, GETDATE(), 120), 8)) as presenttime,CAST((GETDATE()-date) as time(0))'recent' from Visitors )select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset  from cte where  (datediff(SECOND,date1,presenttime)/3600)<=3 and year(date)=year(getdate()) and month(date)=month(date) and day(date)=day(getdate()) ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void visitor_qrscan()
        {
            string vid = Request.Params["qr_visitorid"].ToString();

            DataSet ds = DataQueries.SelectCommon1("select outtime from visitors where vid='" + vid + "' and outtime is not null");
            if (ds.Tables[0].Rows.Count == 1)
            {
                Response.Write("{\"error\": true ,\"message\":\"Visitor already exited\"}");
            }
            else
            {

                string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
                SqlConnection conn1 = new SqlConnection(connstrg1);
                String insert = "update Visitors set outtime='" + DateTime.Now.ToString("MM-dd-yyy hh:mm:ss") + "',Status='Sign Off' where vid='" + vid + "'";
                SqlCommand comm1 = new SqlCommand(insert, conn1);
                conn1.Open();
                comm1.ExecuteNonQuery();
                conn1.Close();
                Response.Write("{\"error\": false ,\"message\":\"success\"}");
            }
        }



        protected void dayreport()
        {
            string date = Request.Params["Date"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset from Visitors where REPLACE((convert(varchar,(convert(datetime,date,110)),111)), '/', '-')='" + date + "' "); //yyyy-MM-dd
                                                                                                                                                                                                                                                                                                                                                                                                                                     //   DataSet ds = DataQueries.SelectCommon("select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,'vms.bterp.in'+Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset from Visitors where CONVERT(varchar(50),(CONVERT(datetime,date,110)),105)='"+date+"'"); //dd-MM-yyyy
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        protected void twodates()
        {
            string StartDate = Request.Params["StartDate"].ToString();
            string EndDate = Request.Params["EndDate"].ToString();
            //   DataSet ds = DataQueries.SelectCommon("select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,'vms.bterp.in'+Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset from Visitors where CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) between '"+StartDate+"' and '"+EndDate+"'"); //dd-MM-yyyy
            DataSet ds = DataQueries.SelectCommon1("select vid,Name,Mobile,Email,Address,WhoomToMeet,Purpose,department,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,Photo as image,substring( (CONVERT(varchar,outtime)),12,19) as outtime,asset from Visitors where REPLACE((convert(varchar,(convert(datetime,date,110)),111)), '/', '-') between '" + StartDate + "' and '" + EndDate + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        protected void saveotp()
        {
            string mobile = Request.Params["mobile"].ToString();
            string mb = string.Empty;
            DataSet ds = DataQueries.SelectCommon1("select mobile from visitors where mobile='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                mb = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
            }

            if (mobile == mb)
            {
                Response.Write("{\"error\": false ,\"msg\":\"alreadyexists\"}");
            }
            else


            {
                Random randomno = new Random();
                int OTPno = randomno.Next(1000, 9999);
                sendOTP(mobile, OTPno);
                DataQueries.InsertCommon1("insert into otp(mobile,otp) values('" + mobile + "','" + OTPno + "')");
                Response.Write("{\"error\": false ,\"msg\":\"otpverification\"}");

            }

        }

        public void sendOTP(string mobile, int OTP)
        {
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Welcome to Brihaspathi Technologies..! Your OTP is :" + OTP.ToString();
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
        }


        protected void verify_otp()
        {
            string mobile = Request.Params["mobile"].ToString();
            string otp = Request.Params["otp"].ToString();
            DataSet ds = DataQueries.SelectCommon1("Select * from otp where mobile='" + mobile + "' and otp='" + otp + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("{\"error\":\"false\",\"message\":\"Mobile number verified successfully\"}");
            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"Failed\"}");
            }
        }
        protected void resend_otp()
        {
            string id = string.Empty;
            string mobile = Request.Params["mobile"].ToString();
            DataSet ds = DataQueries.SelectCommon1("Select * from otp where mobile='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = ds.Tables[0].Rows[0]["id"].ToString();
            }
            Random randomno = new Random();
            int OTPno = randomno.Next(1000, 9999);
            sendOTP(mobile, OTPno);
            DataQueries.UpdateCommon1("Update otp set otp='" + OTPno + "' where id='" + id + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void employeenames()
        {
            string id = Request.Params["empname"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select name from tblEmployee where name like '%" + id + "%' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"name\": " + JSONresult + "}");
        }
        //Courier Services

        protected void courieroutward()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string center = Request.Params["Couriercentre"].ToString();
            string trackno = Request.Params["Ctrackno"].ToString();
            string rcperson = Request.Params["RcvPerson"].ToString();
            string toaddress = Request.Params["Toaddress"].ToString();
            string fromadd = Request.Params["Fromaddress"].ToString();
            string sndperson = Request.Params["Sndperson"].ToString();
            string description = Request.Params["Description"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            DataQueries.InsertCommon1("insert into CourierOutward(date,CourierCentre,Ctrackno,RcvPerson,ToAddress,FromAddress,SndPerson,Description,mobile) values('" + date.Replace("'", "''") + "','" + center.Replace("'", "''") + "','" + trackno.Replace("'", "''") + "','" + rcperson.Replace("'", "''") + "','" + toaddress.Replace("'", "''") + "','" + fromadd.Replace("'", "''") + "','" + sndperson.Replace("'", "''") + "','" + description.Replace("'", "''") + "','" + mobile.Replace("'", "''") + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string link = string.Empty;
            if (center == "DTDC")
            {
                link = "http://www.dtdc.in/tracking/shipment-tracking.asp ";
            }
            else if (center == "The Professional couriers")
            {
                link = "http://www.tpcindia.com/multiple-tracking.aspx";
            }

            string smstext = "Dear " + rcperson + " We have sent you the courier from " + center + " .you can Check your courier Status using this link " + link + " .your courier tracking id is " + trackno + " ";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();

        }
        protected void datafillbasedonmobile()
        {
            string mobile = Request.Params["mobile"].ToString();
            string mob = string.Empty;
            string name = string.Empty;
            string email = string.Empty;
            string empid = string.Empty;
            string empname = string.Empty;
            string purpose = string.Empty;
            string purposee = string.Empty;
            DataSet dss = DataQueries.SelectCommon1("select mobile,empid from advancevisitor where mobile='" + mobile + "' order by id desc");
            if (dss.Tables[0].Rows.Count > 0)
            {
                mob = dss.Tables[0].Rows[0]["mobile"].ToString().Trim();
                empid = dss.Tables[0].Rows[0]["empid"].ToString().Trim();
            }
            DataSet d = DataQueries.SelectCommon1("select name from tblemployee where empid='" + empid + "' order by id desc");
            if (d.Tables[0].Rows.Count > 0)
            {

                empname = d.Tables[0].Rows[0]["name"].ToString().Trim();
            }
            if (mob == mobile)
            {
                DataSet dsss = DataQueries.SelectCommon1("select vname,email,purpose from advancevisitor where mobile='" + mobile + "' order by id desc");
                if (dsss.Tables[0].Rows.Count > 0)
                {
                    string vname = dsss.Tables[0].Rows[0]["vname"].ToString();
                    string emaill = dsss.Tables[0].Rows[0]["email"].ToString();
                    purpose = dsss.Tables[0].Rows[0]["purpose"].ToString();
                    Response.Write("{\"error\": false ,\"name\":\"" + vname + "\",\"email\":\"" + emaill + "\",\"purpose\":\"" + purpose + "\",\"whoomtomeet\":\"" + empname + "\"}");

                }

            }
            else
            {
                DataSet ds = DataQueries.SelectCommon1("select name,email,purpose from visitors where mobile='" + mobile + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    email = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                    name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                    purposee = ds.Tables[0].Rows[0]["purpose"].ToString().Trim();

                }
                Response.Write("{\"error\": false ,\"name\":\"" + name + "\",\"email\":\"" + email + "\",\"purpose\":\"" + purposee + "\",\"whoomtomeet\":\"" + empname + "\"}");
            }

        }


        protected void CourierInward()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string center = Request.Params["CourierCentre"].ToString();
            string trackno = Request.Params["Ctrackno"].ToString();
            string Whoom = Request.Params["Whom"].ToString();
            string fromadd = Request.Params["Fromaddress"].ToString();
            string toaddress = Request.Params["Toaddress"].ToString();
            string Rcvperson = Request.Params["RcvPerson"].ToString();
            string description = Request.Params["Description"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            DataQueries.InsertCommon1("insert into CourierInward(date,CourierCentre,Ctrackno,Whom,FromAddress,ToAddress,RcvPerson,Description,mobile) values('" + date + "','" + center.Replace("'", "''") + "','" + trackno.Replace("'", "''") + "','" + Whoom.Replace("'", "''") + "','" + fromadd.Replace("'", "''") + "','" + toaddress.Replace("'", "''") + "','" + Rcvperson.Replace("'", "''") + "','" + description.Replace("'", "''") + "','" + mobile + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";

            string smstext = "Dear " + Whoom + " your courier received succesfully from " + center + " ";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);

            response = (HttpWebResponse)request.GetResponse();

        }


        protected void outwarddisplay()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from CourierOutward");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void Inwarddisplay()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from CourierInward");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }



        protected void notifylist()
        {
            string empid = Request.Params["empid"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            if (empid == "729")
            {
                DataSet ds = DataQueries.SelectCommon1("select whoomtomeet as name,statusby as purpose,photo,cpid from visitors where cpid='0' order by id desc");
                DataTable table = ds.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }


            else
            {
                DataSet ds = DataQueries.SelectCommon1("select vid,name,purpose,address,photo,mobile,email,WhoomToMeet,cpid from visitors where empid='" + empid + "' and convert(varchar,date,105)='" + date + "' and outtime is null and cpid='1' order by id desc");
                DataTable table = ds.Tables[0];
                string JSONresult = DataTableToJsonWithJsonNet(table);
                Response.Write(JSONresult);
            }

        }
        protected void notifylisttest()
        {
            string empid = Request.Params["empid"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");

            DataSet ds = DataQueries.SelectCommon1("select vid,name,purpose,address,photo,mobile,email,WhoomToMeet,cpid from visitorstest where empid='" + empid + "' and convert(varchar,date,105)='" + date + "' and outtime is null and cpid='1' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }

        protected void notifytosupport()
        {
            string empid = Request.Params["empid"].ToString();
            string msg = Request.Params["message"].ToString();
            string option = Request.Params["option"].ToString();
            string vid = Request.Params["vid"].ToString();

            int empID = Convert.ToInt32(empid);
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);

            Notification notification1 = new Notification();
            notification1.title = employee1.name;
            notification1.body = msg + "  " + option;
            notification1.code = 19;
            supp_token(notification1);
            supp(notification1);
            DataQueries.UpdateCommon1("Update visitors set cpid='0',statusby='" + msg + "',optionn='" + option + "' where vid='" + vid + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");


        }
        protected void notifytosupporttest()
        {
            string empid = Request.Params["empid"].ToString();
            string msg = Request.Params["message"].ToString();
            string option = Request.Params["option"].ToString();
            string vid = Request.Params["vid"].ToString();

            int empID = Convert.ToInt32(empid);
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);

            Notification notification1 = new Notification();
            notification1.title = employee1.name;
            notification1.body = msg + "  " + option;
            notification1.code = 19;
            supp_token(notification1);
            supp(notification1);
            DataQueries.UpdateCommon1("Update visitorstest set cpid='0',statusby='" + msg + "',optionn='" + option + "' where vid='" + vid + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");


        }

        private void supp_token(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='729' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;



            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }
        private void supp(Notification notification)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from Employees where Employeeid='729' and status='1'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();
            }
            //return arrvalues;



            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }


        //cpid : after approving or rejecting whether to meet visitor ,no need to display the data
        // statusby : the message whether they want to meet the visitor or not is stored in statusby column



        protected void notifytosupportifrejected()
        {
            string vid = Request.Params["vid"].ToString();
            string empid = Request.Params["empid"].ToString();
            string msg = Request.Params["message"].ToString();
            string name = Request.Params["name"].ToString();
            string id = string.Empty;
            int empID = Convert.ToInt32(empid);
            string vname = string.Empty;
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);

            Notification notification1 = new Notification();
            notification1.title = employee1.name;
            notification1.body = msg;
            notification1.code = 19;
            supp_token(notification1);
            supp(notification1);
            DataSet d = DataQueries.SelectCommon1("select empid from tblemployee where name='" + name + "' order by id desc");
            if (d.Tables[0].Rows.Count > 0)
            {

                id = d.Tables[0].Rows[0]["empid"].ToString().Trim();
            }

            DataSet dd = DataQueries.SelectCommon1("select name from visitors where vid='" + vid + "' order by id desc");
            if (dd.Tables[0].Rows.Count > 0)
            {

                vname = dd.Tables[0].Rows[0]["name"].ToString().Trim();
            }
            Notification notification2 = new Notification();
            notification2.title = employee1.name + " wants you to meet " + vname; ;
            //notification2.body = "";
            notification2.code = 19;
            hr_tokens(id, notification2);
            hr_tok(id, notification2);
            DataQueries.UpdateCommon1("Update visitors set cpid='0',statusby='" + msg + "' where vid='" + vid + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");
        }
        protected void notifytosupportifrejectedtest()
        {
            string vid = Request.Params["vid"].ToString();
            string empid = Request.Params["empid"].ToString();
            string msg = Request.Params["message"].ToString();
            string name = Request.Params["name"].ToString();
            string id = string.Empty;
            int empID = Convert.ToInt32(empid);
            string vname = string.Empty;
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);

            Notification notification1 = new Notification();
            notification1.title = employee1.name;
            notification1.body = msg;
            notification1.code = 19;
            supp_token(notification1);
            supp(notification1);
            DataSet d = DataQueries.SelectCommon1("select empid from tblemployee where name='" + name + "' order by id desc");
            if (d.Tables[0].Rows.Count > 0)
            {

                id = d.Tables[0].Rows[0]["empid"].ToString().Trim();
            }

            DataSet dd = DataQueries.SelectCommon1("select name from visitorstest where vid='" + vid + "' order by id desc");
            if (dd.Tables[0].Rows.Count > 0)
            {

                vname = dd.Tables[0].Rows[0]["name"].ToString().Trim();
            }
            Notification notification2 = new Notification();
            notification2.title = employee1.name + " wants you to meet " + vname; ;
            //notification2.body = "";
            notification2.code = 13;
            hr_tokens(id, notification2);
            hr_tok(id, notification2);
            DataQueries.UpdateCommon1("Update visitorstest set cpid='0',statusby='" + msg + "' where vid='" + vid + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");
        }

        protected void ratingupdate()
        {
            string vid = Request.Params["vid"].ToString();
            string rating = Request.Params["rating"].ToString();
            string sugg = Request.Params["suggestion"].ToString();
            DataQueries.UpdateCommon1("update visitors set rating='" + rating + "',suggestion='" + sugg + "' where vid='" + vid + "'");

            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }

        protected void displaynotiftosupport()
        {
            DataSet ds = DataQueries.SelectCommon1("select whoomtomeet,name,statusby from visitors where cpid='0' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void cardiddisplay()
        {
            string vid = Request.Params["vid"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select cardid from visitors where vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["cardid"].ToString().Trim();
                Response.Write("{\"error\": false ,\"cardid\":\"" + id + "\"}");

            }



        }
        protected void visitorslistdisplay()
        {
            //DataSet ds = DataQueries.SelectCommon1("SELECT *,(right(CONVERT(VARCHAR, cast(date as datetime), 120), 8)) as in_time,(right(CONVERT(VARCHAR, cast(outtime as datetime), 120), 8)) as out_time,CAST((cast(outtime as datetime)-cast(date as datetime)) as time(0)) 'timeDifferance' FROM Visitors  order by id desc");
            DataSet ds = DataQueries.SelectCommon1("SELECT *,CAST((cast(outtime as datetime)-cast(date as datetime)) as time(0)) 'intime' FROM Visitors  order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void datewisevisitorslist()
        {
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select * from visitors where year(date)=year('" + date + "') and month(date)=month('" + date + "') and day(date)=day('" + date + "') ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void addvisit()
        {
            string empid = string.Empty;
            string empmobile = string.Empty;
            double vid = newvid();


            string Name = Request.Params["Name"].ToString().Trim();
            string Mobile = Request.Params["Mobile"].ToString().Trim();
            string Email = Request.Params["Email"].ToString().Trim();
            string Address = Request.Params["Address"].ToString().Trim();
            string WhoomToMeet = Request.Params["WhoomToMeet"].ToString();
            string Purpose = Request.Params["Purpose"].ToString().Trim();
            //string department = Request.Params["department"].ToString().Trim();
            //string asset = Request.Params["asset"].ToString().Trim();
            double cardid = card();
            string Photo = Request.Params["Photo"].ToString().Trim();

            string base64 = Photo;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/Captures/") + vid + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/Captures/" + vid + ".jpg";
            String result = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt");

            DataSet dss = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + WhoomToMeet + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                empmobile = dss.Tables[0].Rows[0]["mobile"].ToString().Trim();
                empid = dss.Tables[0].Rows[0]["empid"].ToString().Trim();
            }

            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "insert into Visitors(Name, Mobile, Email, Address, Photo, Purpose, date, WhoomToMeet, vid, status, status1,cardid,empid,cpid)values('" + Name.Replace("'", "''") + "', '" + Mobile.Replace("'", "''") + "', '" + Email.Replace("'", "''") + "', '" + Address.Replace("'", "''") + "','" + path + "', '" + Purpose.Replace("'", "''") + "', '" + result.Replace("'", "''") + "', '" + WhoomToMeet + "', '" + vid + "', 'arrive','1','" + cardid + "','" + empid + "','1')";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            DataSet ds = DataQueries.SelectCommon1("select vid,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,signatureimage from Visitors where  vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string visitorid = ds.Tables[0].Rows[0]["vid"].ToString().Trim();

                string intime = ds.Tables[0].Rows[0]["intime"].ToString().Trim();
                string date = ds.Tables[0].Rows[0]["date"].ToString().Trim();
                //string photoo = ds.Tables[0].Rows[0]["signatureimage"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"visitorid\":\"" + visitorid + "\",\"intime\":\"" + intime + "\",\"date\":" + date + "\"}");
            }

            //to display notification in btempower app




        }

        protected void addvisitdemo()
        {
            string empid = string.Empty;
            string empmobile = string.Empty;
            double vid = newvid();


            string Name = Request.Params["Name"].ToString().Trim();
            string Mobile = Request.Params["Mobile"].ToString().Trim();
            string Email = Request.Params["Email"].ToString().Trim();
            string Address = Request.Params["Address"].ToString().Trim();
            // string WhoomToMeet = Request.Params["WhoomToMeet"].ToString();
            string Purpose = Request.Params["Purpose"].ToString().Trim();
            //string department = Request.Params["department"].ToString().Trim();
            //string asset = Request.Params["asset"].ToString().Trim();
            double cardid = card();
            string Photo = Request.Params["Photo"].ToString().Trim();

            string base64 = Photo;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/Captures/") + vid + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/Captures/" + vid + ".jpg";
            String result = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt");

            //DataSet dss = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + WhoomToMeet + "'");
            //if (dss.Tables[0].Rows.Count > 0)
            //{
            //    empmobile = dss.Tables[0].Rows[0]["mobile"].ToString().Trim();
            //    empid = dss.Tables[0].Rows[0]["empid"].ToString().Trim();
            //}

            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "insert into Visitors(Name, Mobile, Email, Address, Photo, Purpose, date, vid, status, status1,cardid,empid,cpid)values('" + Name.Replace("'", "''") + "', '" + Mobile.Replace("'", "''") + "', '" + Email.Replace("'", "''") + "', '" + Address.Replace("'", "''") + "','" + path + "', '" + Purpose.Replace("'", "''") + "', '" + result.Replace("'", "''") + "', '" + vid + "', 'arrive','1','" + cardid + "','" + empid + "','1')";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            DataSet ds = DataQueries.SelectCommon1("select vid,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,signatureimage from Visitors where  vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string visitorid = ds.Tables[0].Rows[0]["vid"].ToString().Trim();

                string intime = ds.Tables[0].Rows[0]["intime"].ToString().Trim();
                string date = ds.Tables[0].Rows[0]["date"].ToString().Trim();
                //string photoo = ds.Tables[0].Rows[0]["signatureimage"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"visitorid\":\"" + visitorid + "\",\"intime\":\"" + intime + "\",\"date\":" + date + "\"}");
            }

            //to display notification in btempower app

        }


        protected void addvisittest()
        {
            string empid = string.Empty;
            string empmobile = string.Empty;
            double vid = newvidtest();


            string Name = Request.Params["Name"].ToString().Trim();
            string Mobile = Request.Params["Mobile"].ToString().Trim();
            string Email = Request.Params["Email"].ToString().Trim();
            string Address = Request.Params["Address"].ToString().Trim();
            string WhoomToMeet = Request.Params["WhoomToMeet"].ToString();
            string Purpose = Request.Params["Purpose"].ToString().Trim();
            //string department = Request.Params["department"].ToString().Trim();
            //string asset = Request.Params["asset"].ToString().Trim();
            double cardid = 1;
            string Photo = Request.Params["Photo"].ToString().Trim();

            string base64 = Photo;
            byte[] bytes1 = Convert.FromBase64String(base64);
            File.WriteAllBytes(Server.MapPath("~/Capturestest/") + vid + ".jpg", bytes1); //write to a temp location.
            string path = "http://emp.bterp.in/Capturestest/" + vid + ".jpg";
            String result = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt");

            DataSet dss = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + WhoomToMeet + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                empmobile = dss.Tables[0].Rows[0]["mobile"].ToString().Trim();
                empid = dss.Tables[0].Rows[0]["empid"].ToString().Trim();
            }

            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "insert into Visitorstest(Name, Mobile, Email, Address, Photo, Purpose, date, WhoomToMeet, vid, status, status1,cardid,empid,cpid)values('" + Name.Replace("'", "''") + "', '" + Mobile.Replace("'", "''") + "', '" + Email.Replace("'", "''") + "', '" + Address.Replace("'", "''") + "','" + path + "', '" + Purpose.Replace("'", "''") + "', '" + result.Replace("'", "''") + "', '" + WhoomToMeet + "', '" + vid + "', 'arrive','1','" + cardid + "','" + empid + "','1')";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            DataSet ds = DataQueries.SelectCommon1("select vid,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime,signatureimage from Visitorstest where  vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                string visitorid = ds.Tables[0].Rows[0]["vid"].ToString().Trim();

                string intime = ds.Tables[0].Rows[0]["intime"].ToString().Trim();
                string date = ds.Tables[0].Rows[0]["date"].ToString().Trim();
                //string photoo = ds.Tables[0].Rows[0]["signatureimage"].ToString().Trim();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"visitorid\":\"" + visitorid + "\",\"intime\":\"" + intime + "\",\"date\":" + date + "\"}");
            }

            //to display notification in btempower app
            int empID = Convert.ToInt32(empid);
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);
            Notification notification1 = new Notification();
            notification1.title = "Visitor ";
            notification1.body = Name + " has visited to meet you regarding " + Purpose;
            notification1.code = 19;
            hr_tokens(empid, notification1);
            hr_tok(empid, notification1);
            visitnotify(empid, notification1);
            //sending sms
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Dear " + employee1.name + ", This is to inform you that " + Name + " has visited to meet you regarding " + Purpose + " from " + Address + "";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + empmobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();



        }

        protected void signatureim()
        {
            string vid = Request.Params["vid"].ToString();
            string signat = Request.Params["signature"].ToString();
            string base644 = signat;

            byte[] bytes11 = Convert.FromBase64String(base644);
            File.WriteAllBytes(Server.MapPath("~/sign/") + vid + ".jpg", bytes11); //write to a temp location.
            string pathh = "http://emp.bterp.in/sign/" + vid + ".jpg";
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update visitors set signatureimage ='" + pathh + "' where vid='" + vid + "' ";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            message(vid);
            Response.Write("{\"error\": false ,\"message\":\"success\"}");


        }


        protected void signatureimdemo()
        {
            string vid = Request.Params["vid"].ToString();
            string signat = Request.Params["signature"].ToString();
            string base644 = signat;

            byte[] bytes11 = Convert.FromBase64String(base644);
            File.WriteAllBytes(Server.MapPath("~/sign/") + vid + ".jpg", bytes11); //write to a temp location.
            string pathh = "http://emp.bterp.in/sign/" + vid + ".jpg";
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update visitors set signatureimage ='" + pathh + "' where vid='" + vid + "' ";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            messagedemo(vid);
            // Response.Write("{\"error\": false ,\"message\":\"success\"}");



        }


        protected void messagedemo(string vid)
        {
            string empid = string.Empty;
            string empmobile = string.Empty;
            string Name = string.Empty;
            string mobile = string.Empty;
            string purpose = string.Empty;
            string whomtomeet = string.Empty;
            string address = string.Empty;
            DataSet dss = DataQueries.SelectCommon1("select * from visitors where vid='" + vid + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                Name = dss.Tables[0].Rows[0]["name"].ToString();
                mobile = dss.Tables[0].Rows[0]["mobile"].ToString();

            }
            //DataSet ds = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + whomtomeet + "'");
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    empmobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
            //    empid = ds.Tables[0].Rows[0]["empid"].ToString().Trim();
            //}


            //int empID = Convert.ToInt32(empid);
            //EmployeeOperation operation = new EmployeeOperation();
            //EmployeeItem employee1 = operation.getEmployee(empID);
            //Notification notification1 = new Notification();
            //notification1.title = "Visitor ";
            //notification1.body = Name + " has visited to meet you regarding " + purpose;
            //notification1.code = 19;
            //visitnotify(empid, notification1);
            //hr_tokens(empid, notification1);
            //hr_tok(empid, notification1);
            //sending sms
            var client = new RestClient("https://api.myvaluefirst.com/psms/servlet/psms.JsonEservice");
            var requestt = new RestRequest(Method.POST);
            requestt.AddHeader("postman-token", "kjbbf104232321f4400100-4xmBRIHASPATI");
            requestt.AddHeader("cache-control", "no-cache");
            requestt.AddHeader("content-type", "application/json");
            requestt.AddParameter("application/json", "{\"@VER\":\"1.2\", \"USER\":{\"@USERNAME\":\" brihaspatiWA\",\"@PASSWORD\":\"brih9876\",\"@UNIXTIMESTAMP\":\"\"}, \"DLR\":{\"@URL\":\"\"},\"SMS\":[ {\"@UDH\": \"0\",\"@CODING\": \"1\",\"@TEXT\": \"\",\"@TEMPLATEINFO\": \"188379~" + Name + "~" + " our stall . For any queries contact us at 9989993242 from Brihaspathi Technologies Pvt Ltd" + "\",\"@PROPERTY\": \"0\",\"@ID\": \"2\", \"ADDRESS\": [{\"@FROM\": \"919268622222\",\"@TO\": \" " + mobile + "\",\"@SEQ\": \"1\",\"@TAG\": \"some clientside random data\"}]}]}", ParameterType.RequestBody);
            IRestResponse responsee = client.Execute(requestt);
            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }
        protected void message(string vid)
        {
            string empid = string.Empty;
            string empmobile = string.Empty;
            string Name = string.Empty;
            string mobile = string.Empty;
            string purpose = string.Empty;
            string whomtomeet = string.Empty;
            string address = string.Empty;
            DataSet dss = DataQueries.SelectCommon1("select * from visitors where vid='" + vid + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                Name = dss.Tables[0].Rows[0]["name"].ToString();
                mobile = dss.Tables[0].Rows[0]["mobile"].ToString();
                purpose = dss.Tables[0].Rows[0]["purpose"].ToString();
                whomtomeet = dss.Tables[0].Rows[0]["WhoomToMeet"].ToString();
                address = dss.Tables[0].Rows[0]["address"].ToString();
            }
            DataSet ds = DataQueries.SelectCommon1("select mobile,empid from tblEmployee where name='" + whomtomeet + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                empmobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
                empid = ds.Tables[0].Rows[0]["empid"].ToString().Trim();
            }


            int empID = Convert.ToInt32(empid);
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee1 = operation.getEmployee(empID);
            Notification notification1 = new Notification();
            notification1.title = "Visitor ";
            notification1.body = Name + " has visited to meet you regarding " + purpose;
            notification1.code = 19;
            visitnotify(empid, notification1);
            hr_tokens(empid, notification1);
            hr_tok(empid, notification1);
            //sending sms
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Dear " + employee1.name + ", This is to inform you that " + Name + " has visited to meet you regarding " + purpose + " from " + address + "";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + empmobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();

        }
        protected void signatureimtest()
        {
            string vid = Request.Params["vid"].ToString();
            string signat = Request.Params["signature"].ToString();
            string base644 = signat;

            byte[] bytes11 = Convert.FromBase64String(base644);
            File.WriteAllBytes(Server.MapPath("~/signtest/") + vid + ".jpg", bytes11); //write to a temp location.
            string pathh = "http://emp.bterp.in/signtest/" + vid + ".jpg";
            string connstrg1 = ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg1);
            String insert = "update visitorstest set signatureimage ='" + pathh + "' where vid='" + vid + "' ";
            SqlCommand comm1 = new SqlCommand(insert, conn1);
            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            Response.Write("{\"error\": false ,\"message\":\"success\"}");


        }
        protected void displayvisitors()
        {
            string vid = Request.Params["vid"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select *,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime from visitors where vid='" + vid + "' order by id desc");

            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }
        protected void displayvisitorstest()
        {
            string vid = Request.Params["vid"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select *,CONVERT(varchar(50),(CONVERT(datetime,date,110)),105) as date,substring( (CONVERT(varchar,date)),12,19) as intime from visitorstest where vid='" + vid + "' order by id desc");

            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }


        protected void testnoti()
        {
            string empid = Request.Params["empid"].ToString();
            Notification notification1 = new Notification();
            notification1.title = "testtoken";
            notification1.body = "token testing";
            notification1.code = 19;
            su(notification1, empid);
        }

        private void su(Notification notification, string empid)
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from tokenstbl where empid='" + empid + "'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;
            Message msg = new Message();
            msg.sendMultipleNotification(arrvalues, notification);
        }



        public void vtest()
        {
            DataSet tokens = Inventroy_Queries.SelectCommon("select * from tokenstbl where empid=739");
            Notification notification1 = new Notification();
            notification1.title = "testtoken";
            notification1.body = "token testing";
            notification1.image = "http://emp.bterp.in/Captures/98.jpg";
            notification1.code = 19;
            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
                Message msg = new Message();
                msg.sendMultiNotification(arrvalues, notification1);


            }

            //return arrvalues;
        }

        protected void trendytv()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select v.*,l.status as st from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid ) select *,case when (st is null or st=1)  then '1' else '0' end status from cte");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);


        }

        protected void trendyreg()
        {
            string name = Request.Params["name"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string email = Request.Params["email"].ToString();
            string pin = Request.Params["pin"].ToString();
            //string image = Request.Params["image"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendyregistration where pin='" + pin + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write("{\"error\": true ,\"message\":\"pin already exists\"}");
            }
            else
            {
                DataQueries.InsertCommon("insert into trendyregistration(name,email,mobile,pin) values ('" + name + "','" + email + "','" + mobile + "','" + pin + "')");
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");

            }
        }
        protected void pinverify()
        {
            string pin = Request.Params["pin"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendyregistration where pin='" + pin + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["id"].ToString();
                string name = ds.Tables[0].Rows[0]["name"].ToString();
                string email = ds.Tables[0].Rows[0]["email"].ToString();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"id\":\"" + id + "\",\"name\":\"" + name + "\",\"mobile\":\"" + mobile + "\",\"email\":\"" + email + "\"}");

            }
        }
        protected void addcomments()
        {
            string vid = Request.Params["videoid"].ToString();
            string id = Request.Params["cid"].ToString();
            string comment = Request.Params["comment"].ToString();
            DataQueries.InsertCommon("insert into trendycomments(videoid,cid,comment) values ('" + vid + "','" + id + "','" + comment + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");


        }
        protected void commentsdisplay()
        {
            string vid = Request.Params["videoid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select c.comment,r.name from trendycomments c inner join trendyregistration r on c.cid=r.id where c.videoid='" + vid + "'");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void trendylikes()
        {
            string vid = Request.Params["videoid"].ToString();
            string id = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendylikes where vid='" + vid + "' and cid='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataQueries.InsertCommon("delete from trendylikes where vid='" + vid + "' and cid='" + id + "'");
                //  DataQueries.InsertCommon("insert into trendylikes(vid,cid,status) values ('" + vid + "','" + id + "','1')");

                Response.Write("{\"error\": false ,\"status\":\"1\"}");
            }
            else
            {
                DataQueries.InsertCommon("insert into trendylikes(vid,cid,status) values ('" + vid + "','" + id + "','0')");
                Response.Write("{\"error\": false ,\"status\":\"0\"}");
            }
        }

        protected void views()
        {
            string vid = Request.Params["videoid"].ToString();
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendyviews where vid='" + vid + "' and cid='" + cid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {

            }
            else
            {
                DataQueries.InsertCommon("insert into trendyviews(vid,cid) values ('" + vid + "','" + cid + "')");
            }
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void viewscount()
        {
            string vid = Request.Params["videoid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select count(id) as count from trendyviews where vid='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string count = ds.Tables[0].Rows[0]["count"].ToString();
                Response.Write("{\"error\": false ,\"count\":\"" + count + "\"}");
            }
            else
            {
                Response.Write("{\"error\": false ,\"count\":\"0\"}");
            }
        }

        protected void forgotpin()
        {
            string mobile = Request.Params["mobile"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendyregistration where mobile='" + mobile + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string pin = ds.Tables[0].Rows[0]["pin"].ToString();
                WebRequest request = null;
                HttpWebResponse response = null;
                String userid = "BTRAK";
                String passwd = "841090";
                string smstext = "Your pin is " + pin;
                String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
                request = WebRequest.Create(url);
                response = (HttpWebResponse)request.GetResponse();
                Response.Write("{\"error\": false ,\"message\":\" pin sent\"}");
            }
            else
            {
                Response.Write("{\"error\": true ,\"message\":\"not registered\"}");
            }
        }
        protected void likedvideos()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select v.id,v.image,v.url,v.name,l.status,v.description from trendytv v inner join trendylikes l on v.id=l.vid  where l.cid='" + cid + "'");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);

        }
        protected void history()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select distinct v.id,v.image,v.url,v.name,tl.status as st,v.description from trendytv v inner join trendyviews l on v.id=l.vid left join trendylikes tl on v.id=tl.vid where l.cid='" + cid + "') select * , case when st is null then '1' else '0' end status from cte");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void profile()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select * from trendyregistration where id='" + cid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString();
                string email = ds.Tables[0].Rows[0]["email"].ToString();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                string image = ds.Tables[0].Rows[0]["image"].ToString();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"email\":\"" + email + "\",\"mobile\":\"" + mobile + "\",\"image\":\"" + image + "\"}");


            }
        }
        protected void recentvideos()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select top 3  v.id,v.name,v.image,v.url,l.status as st,v.description from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid ) select *,case when (st is null or st=1)  then '1' else '0' end status from cte order by id desc");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void shortfilms()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select  v.id,v.name,v.image,v.url,l.status as st,v.description from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid where v.category=3) select *,case when (st is null or st=1)  then '1' else '0' end status from cte order by id desc");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void webseries()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select  v.id,v.name,v.image,v.url,l.status as st,v.description from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid where v.category=4) select *,case when (st is null or st=1)  then '1' else '0' end status from cte order by id desc");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void interviews()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select  v.id,v.name,v.image,v.url,l.status as st,v.description from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid where v.category=5) select *,case when (st is null or st=1)  then '1' else '0' end status from cte order by id desc");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);
        }
        protected void askquestions()
        {
            string cid = Request.Params["cid"].ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select  v.id,v.name,v.image,v.url,l.status as st,v.description from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid where v.category=6) select *,case when (st is null or st=1)  then '1' else '0' end status from cte order by id desc");
            DataTable dt = ds.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);


        }
        protected void recentviewlist()
        {
            string vid = Request.Params["vid"].ToString();
            string cid = Request.Params["cid"].ToString();
            string category = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select category from trendytv where id='" + vid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                category = ds.Tables[0].Rows[0]["category"].ToString();
            }
            DataSet d = DataQueries.SelectCommon("with cte as (select v.*,l.status as st from trendytv v left join (select status,vid from trendylikes where cid='" + cid + "') l on v.id=l.vid where v.category='" + category + "') select *,case when (st is null or st=1)  then '1' else '0' end status from cte");
            DataTable dt = d.Tables[0];
            string json = DataTableToJsonWithJsonNet(dt);
            Response.Write(json);

        }
    }

}




