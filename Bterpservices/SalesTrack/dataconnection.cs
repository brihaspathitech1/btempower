﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

namespace inventory.SalesTrack
{
    public class dataconnection
    {
        public dataconnection()
        { }
        public static SqlConnection GetConnection()
        {
            String connstr= ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString;
            SqlConnection connection;
            connection = new SqlConnection(connstr);
            return connection;
        }
    }
}
