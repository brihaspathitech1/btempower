﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Bterpservices
{
    public partial class otp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bind1();
        }
        protected void bind1()
        {

            SqlConnection conw = new SqlConnection(ConfigurationManager.ConnectionStrings["UndiConnectionString"].ConnectionString);
            //SqlCommand cmd = new SqlCommand("select *,cast(a.mrp as float)-cast(a.price as float)-cast(a.distamount as float)-cast(a.dealamount as float) as amt,(d.name) as mname,(d.address) as maddress,(d.mobile) as mmobile from Addproduct a inner join DistributorLogin d on a.shopname=d.shopname where a.status='1' and d.role='merchant' order by a.id desc", conw);
            SqlCommand cmd = new SqlCommand("select * from otp order by id desc", conw);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conw.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;

                grdProduct_Details.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";

            }


        }

    }
}