﻿using System;
using System.Web;
using System.Web.UI;
using inventory.DataAccessLayer;
using System.IO;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using json;
using JsonServices;
using JsonServices.Web;
using Newtonsoft.Json;
using System.Net;
using inventory.webservices.v1.model;
using inventory.webservices.v1.include;
using inventory.InventoryHelper;
using System.Net.Mail;
using System.Net.Mime;

namespace Bterpservices
{
    public partial class seoservices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.HttpMethod == "POST")
            {
                try
                {

                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "addleads":
                            addleads();
                            break;
                        case "displayleads":
                            displayleads();
                            break;
                        case "displayleadsdate":
                            displayleadsdate();
                            break;
                        case "seorequiremnttypes":
                            seorequiremnttypes();
                            break;
                        case "assignlead":
                            assignlead();
                            break;
                        case "empleads":
                            empleads();
                            break;
                        case "leadstatus":
                            leadstatus();
                            break;
                        case "leadquotation":
                            leadquotation();
                            break;
                        //case "leadclose":
                        //    leadclose();
                        //    break;
                        //case "leadreject":
                        //    leadreject();
                        //    break;
                        case "department1":
                            department1();
                            break;
                        case "employees1":
                            employees1();
                            break;
                        case "workassign":
                            workassign();
                            break;
                        case "stocknotification":
                            stocknotification();
                            break;
                        case "acceptedleads":
                            acceptedleads();
                            break;
                        case "supportleads":
                            supportleads();
                            break;
                        case "tecnicianlist":
                            tecnicianlist();
                            break;
                        case "assigntechnican":
                            assigntechnican();
                            break;
                        case "daywiseleadcount":
                            daywiseleadcount();
                            break;
                        case "daywiseempcount":
                            daywiseempcount();
                            break;
                        case "monthwisecount":
                            monthempcount();
                            break;
                        case "monthwiseleadcount":
                            monthwiseleadcount();
                            break;
                        case "empleadsdatewise":
                            empleadsdatewise();
                            break;
                        case "tes":
                            tes();
                            break;
                        case "sendimages":
                            sendimages();
                            break;


                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }


        public void addleads()
        {

            //string empname = string.Empty;
            // string dept = Request.Params["dept"].ToString();
            string empid = Request.Params["empid"].ToString();
            string name = Request.Params["name"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            string email = Request.Params["email"].ToString();
            string description = Request.Params["description"].ToString();
            string location = Request.Params["location"].ToString();
            string requirement = Request.Params["requirement"].ToString();
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            int employeeid = Convert.ToInt32(empid);
            //DataSet ds = DataQueries.SelectCommon1("select name from tblemployee where empid='" + empid + "'");
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    empid = ds.Tables[0].Rows[0]["name"].ToString().Trim();
            //}
            DataQueries.InsertCommon3("insert into lead(name,mobile1,email,remarks,address,additionalinfo,date,leadby) values ('" + name + "','" + mobile + "','" + email + "','" + description + "','" + location + "','" + requirement + "','" + date + "','" + empid + "')");
            EmployeeOperation operation = new EmployeeOperation();
            EmployeeItem employee = operation.getEmployee(employeeid);


            Notification notification1 = new Notification();
            notification1.title = employee.name + "Added a New Lead";
            notification1.body = "";
            notification1.code = 13;
            test_token(notification1);
            test_tok(notification1);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");


        }

        private void test_token(Notification notification)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from employees where employeeid =' 698 '");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }



        private void test_tok(Notification notification)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from employees where employeeid =' 698 '");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }
        //display leads datewise
        public void displayleads()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from seoservicesleads where seodate='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void displayleadsdate()
        {
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select * from seoservicesleads where year(seodate)=year('" + date + "') and month(seodate)=month('" + date + "') and day(seodate)=day('" + date + "')");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public void seorequiremnttypes()
        {
            string name = Request.Params["name"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select name from leadrequirment where name like '%" + name + "%' ");
            DataTable dt = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(dt);
            string result = "{\"Leadtype\": " + JSONresult + "}";
            Response.Write(result);

        }
        public void assignlead()
        {
            string empid = string.Empty;
            string dept = Request.Params["department"].ToString();
            string empname = Request.Params["name"].ToString();
            string id = Request.Params["id"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataSet ds = DataQueries.SelectCommon("select EmployeeId from Employees where EmployeName= '" + empname + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                empid = ds.Tables[0].Rows[0]["EmployeeId"].ToString().Trim();
            }
            DataQueries.UpdateCommon1("update seoservicesleads set assign='" + empname + "',empid='" + empid + "',vpsirdate='" + date + "' where id='" + id + "'");

            Notification notification1 = new Notification();
            notification1.title = "VP Sir Assigned new lead";

            notification1.body = "";
            notification1.code = 13;
            test_token11(notification1, empid);
            test_token1111(notification1, empid);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        private void test_token11(Notification notification, string empid)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from Employees where Employeeid ='" + empid + "'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }

        private void test_token1111(Notification notification, string empid)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from Employees where Employeeid ='" + empid + "'");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }
        protected void empleads()
        {
            string empid = Request.Params["empid"].ToString();
            DataSet ds = DataQueries.SelectCommon3("select * from lead where leadby='" + empid + "' or forwardto='" + empid + "' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }

        protected void empleadsdatewise()
        {
            string date = Request.Params["date"].ToString();
            string empid = Request.Params["empid"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select * from seoservicesleads where empid='" + empid + "' and  year(seodate)=year('" + date + "') and month(seodate)=month('" + date + "') and day(seodate)=day('" + date + "') ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void leadstatus()
        {
            string assign = string.Empty;
            string leadid = Request.Params["id"].ToString();
            string type = Request.Params["type"].ToString();
            string response = Request.Params["reason"].ToString();

            //string meetingresponse = Request.Params["meetingresponse"].ToString();
            if (type == "call")
            {
                string call = string.Empty;
                string requirement = string.Empty;

                DataQueries.UpdateCommon1("update seoservicesleads set call='" + response + "' where id='" + leadid + "'");
                DataSet ds = DataQueries.SelectCommon1("select call,description,assign from seoservicesleads where id='" + leadid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    call = ds.Tables[0].Rows[0]["call"].ToString().Trim();
                    requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
                    assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
                }
                Notification notification1 = new Notification();
                notification1.title = "lead status from" + assign;

                notification1.body = requirement + " Requirement call status " + call;
                notification1.code = 13;
                test_token(notification1);
                test_tok(notification1);
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }
            else if (type == "meeting")
            {
                string meeting = string.Empty;
                string requirement = string.Empty;
                DataQueries.UpdateCommon1("update seoservicesleads set meeting='" + response + "' where id='" + leadid + "'");
                DataSet ds = DataQueries.SelectCommon1("select meeting,description,assign from seoservicesleads where id='" + leadid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    meeting = ds.Tables[0].Rows[0]["meeting"].ToString().Trim();
                    requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
                    assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
                }
                Notification notification1 = new Notification();
                notification1.title = "lead status from" + assign;

                notification1.body = requirement + "Requirement Meeting status " + meeting;
                notification1.code = 13;
                test_token(notification1);
                test_tok(notification1);
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }


            else if (type == "Reject")
            {


                string requirement = string.Empty;
                string rejectre = string.Empty;
                string empid = string.Empty;

                //string leadid = Request.Params["id"].ToString();
                //string reject = Request.Params["reject"].ToString();
                DataQueries.UpdateCommon1("update seoservicesleads set selectstatus='Reject', reason='" + response + "' where id='" + leadid + "'");
                DataSet ds = DataQueries.SelectCommon1("select description,reason, empname,assign from seoservicesleads where id='" + leadid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {

                    requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
                    rejectre = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                    empid = ds.Tables[0].Rows[0]["empname"].ToString().Trim();
                    assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
                }

                Notification notification1 = new Notification();
                notification1.title = "lead status from" + assign;

                notification1.body = "reason for " + requirement + "Requirement reject  " + rejectre;
                notification1.code = 13;
                test_token(notification1);
                test_tok(notification1);
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }
            else if (type == "Success")
            {
                string requirement = string.Empty;
                string successre = string.Empty;
                string empid = string.Empty;

                //string leadid = Request.Params["id"].ToString();
                //string reject = Request.Params["reject"].ToString();
                DataQueries.UpdateCommon1("update seoservicesleads set reason='" + response + "',selectstatus='Success' where id='" + leadid + "'");
                DataSet ds = DataQueries.SelectCommon1("select description,reason, empname,assign from seoservicesleads where id='" + leadid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {

                    requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
                    successre = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                    empid = ds.Tables[0].Rows[0]["empname"].ToString().Trim();
                    assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
                }
                Notification notification1 = new Notification();
                notification1.title = "lead status from" + assign;

                notification1.body = requirement + " Requirement success " + successre;
                notification1.code = 13;
                test_token(notification1);
                test_tok(notification1);
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");


            }
            else if (type == "Pending")
            {
                string requirement = string.Empty;
                string pendingre = string.Empty;
                string empid = string.Empty;

                //string leadid = Request.Params["id"].ToString();
                //string reject = Request.Params["reject"].ToString();
                DataQueries.UpdateCommon1("update seoservicesleads set reason='" + response + "',selectstatus='Pending' where id='" + leadid + "'");
                DataSet ds = DataQueries.SelectCommon1("select description,reason, empname,assign from seoservicesleads where id='" + leadid + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {

                    requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
                    pendingre = ds.Tables[0].Rows[0]["reason"].ToString().Trim();
                    empid = ds.Tables[0].Rows[0]["empname"].ToString().Trim();
                    assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
                }

                Notification notification1 = new Notification();
                notification1.title = "lead status from" + assign;

                notification1.body = "reason for" + requirement + " Requirement pending " + pendingre;
                notification1.code = 13;
                test_token(notification1);
                test_tok(notification1);
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");


            }


        }


        //protected void leadreject()
        //{

        //    string requirement = string.Empty;
        //    string rejectre = string.Empty;
        //    string empid = string.Empty;
        //    string assign = string.Empty;

        //    string leadid = Request.Params["id"].ToString();
        //    string reject = Request.Params["reject"].ToString();
        //    DataQueries.UpdateCommon1("update seoservicesleads set reject='" + reject + "' where id='" + leadid + "'");
        //    DataSet ds = DataQueries.SelectCommon1("select description,reject, empname,assign from seoservicesleads where id='" + leadid + "'");
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {

        //        requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
        //        rejectre = ds.Tables[0].Rows[0]["reject"].ToString().Trim();
        //        empid = ds.Tables[0].Rows[0]["empname"].ToString().Trim();
        //        assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();
        //    }
        //    Notification notification1 = new Notification();
        //    notification1.title = "lead status from" + assign;

        //    notification1.body = "reason for " + requirement + "Requirement Reject  " + rejectre;
        //    notification1.code = 13;
        //    test_tokencall(notification1);
        //    test_token11(notification1, empid);
        //    Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        //}
        protected void leadquotation()
        {
            string leadid = Request.Params["id"].ToString();
            //string quotation = Request.Params["quotation"].ToString();
            string estno = Request.Params["estno"].ToString();
            string quotationprice = Request.Params["quotationprice"].ToString();
            string d = "EST-" + estno;

            DataQueries.UpdateCommon1("update seoservicesleads set quotation='sent',estno='" + d + "',quotationprice='" + quotationprice + "' where id='" + leadid + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        //protected void leadclose()
        //{
        //    string leadid = Request.Params["id"].ToString();
        //    string statustype = Request.Params["statustype"].ToString();
        //    string assign = string.Empty;

        //    if (statustype == "Success")
        //    {
        //        string requirement = string.Empty;
        //        DataQueries.UpdateCommon1("update seoservicesleads set selectstatus='Success' where id='" + leadid + "'");
        //        DataSet ds = DataQueries.SelectCommon1("select description,assign from seoservicesleads where id='" + leadid + "'");
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {

        //            requirement = ds.Tables[0].Rows[0]["description"].ToString().Trim();
        //            assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();

        //        }
        //        Notification notification1 = new Notification();
        //        notification1.title = "lead status from" + assign;

        //        notification1.body = requirement + "Requirement Success";
        //        notification1.code = 13;
        //        test_tokencall(notification1);
        //        Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        //    }
        //    else if (statustype == "Pending")
        //    {
        //        string requirement1 = string.Empty;
        //        DataQueries.UpdateCommon1("update seoservicesleads set selectstatus='Pending' where id='" + leadid + "'");
        //        DataSet ds = DataQueries.SelectCommon1("select description,assign from seoservicesleads where id='" + leadid + "'");
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {

        //            requirement1 = ds.Tables[0].Rows[0]["description"].ToString().Trim();
        //            assign = ds.Tables[0].Rows[0]["assign"].ToString().Trim();

        //        }
        //        Notification notification1 = new Notification();
        //        notification1.title = "lead status from" + assign;

        //        notification1.body = requirement1 + "Requirement Pending";
        //        notification1.code = 13;
        //        test_tokencall(notification1);
        //        Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        //    }

        //}
        protected void workassign()
        {
            string empid = string.Empty;
            string dept = Request.Params["department"].ToString();
            string empname = Request.Params["name"].ToString();
            string id = Request.Params["id"].ToString();
            string text = Request.Params["workdescription"].ToString();
            string estno = Request.Params["estno"].ToString();
            string quotationprice = Request.Params["quotationprice"].ToString();
            DataSet ds = DataQueries.SelectCommon("select EmployeeId from Employees where EmployeName= '" + empname + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                empid = ds.Tables[0].Rows[0]["EmployeeId"].ToString().Trim();
            }
            DataQueries.UpdateCommon1("update seoservicesleads set workdescription='" + text + "',stockemp='" + empname + "' where id='" + id + "'");
            Notification notification1 = new Notification();
            notification1.title = "VP Sir";

            notification1.body = text;
            notification1.code = 13;
            test_token11(notification1, empid);
            test_token1111(notification1, empid);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }

        protected void stocknotification()
        {
            string empid = string.Empty;

            string id = Request.Params["id"].ToString();
            string text = Request.Params["description"].ToString();
            DataQueries.UpdateCommon1("update seoservicesleads set stockres='" + text + "' where id='" + id + "'");
            Notification notification1 = new Notification();
            notification1.title = "Notification From Stock";

            notification1.body = text;
            notification1.code = 13;
            test_token(notification1);
            test_tok(notification1);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }

        protected void acceptedleads()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from seoservicesleads where selectstatus='Success'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void supportleads()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from seoservicesleads where [stockres] is not null");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void tecnicianlist()
        {
            DataSet ds = DataQueries.SelectCommon1("select * from Employees where [Desigantion]='Technical Engineer' and status=1");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void assigntechnican()
        {

            string empid = string.Empty;
            string description = string.Empty;

            string empname = Request.Params["name"].ToString();
            string id = Request.Params["id"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataSet ds = DataQueries.SelectCommon("select EmployeeId from Employees where EmployeName= '" + empname + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                empid = ds.Tables[0].Rows[0]["EmployeeId"].ToString().Trim();
            }
            DataQueries.UpdateCommon1("update seoservicesleads set technician='" + empname + "',technicianid='" + empid + "' where id='" + id + "'");
            DataSet ds1 = DataQueries.SelectCommon1("select description from seoservicesleads where id='" + id + "'");
            if (ds1.Tables[0].Rows.Count > 0)
            {
                description = ds1.Tables[0].Rows[0]["description"].ToString().Trim();
            }

            Notification notification1 = new Notification();
            notification1.title = "From Support";


            notification1.body = "Assigned" + description;
            notification1.code = 13;
            test_token11(notification1, empid);
            test_token1111(notification1, empid);
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }


        protected void daywiseleadcount()
        {

            DataSet ds = DataQueries.SelectCommon1("select count(dept) as count  from seoservicesleads where dept='51' and seodate='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["count"].ToString().Trim();
                Response.Write("{\"error\": false ,\"count\":\"" + id + "\"}");

            }

        }

        protected void daywiseempcount()
        {
            // DataSet ds = DataQueries.SelectCommon1("select cast(count(s.dept) as varchar) as count,e.EmployeName from seoservicesleads s inner join employees e on s.empname=e.EmployeeId where dept='51' and seodate='" + DateTime.Now.ToString("yyyy-MM-dd") + "' group by dept,EmployeName ");

            DataSet ds = DataQueries.SelectCommon1("with cte as(select * from seoservicesleads where seodate='" + DateTime.Now.ToString("yyyy-MM-dd") + "') select cast(count(s.dept) as varchar) as count,e.EmployeName from cte s right join employees e  on e.EmployeeId=s.empname where e.DeptId=51 and e.status=1  group by EmployeeId,EmployeName,dept  ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void monthempcount()
        {

            string date = Request.Params["Date"].ToString();
            //  DataSet ds = DataQueries.SelectCommon1("select cast(count(s.dept) as varchar) as count,e.EmployeName from seoservicesleads s inner join employees e on s.empname=e.EmployeeId where dept='51' and year(seodate)=year('" + date+"') and month(seodate)=month('"+date+ "') group by dept,EmployeName "); //yyyy-MM-dd
            DataSet ds = DataQueries.SelectCommon1("with cte as(select * from seoservicesleads where year(seodate)=year('" + date + "') and month(seodate)=month('" + date + "')) select cast(count(s.dept) as varchar) as count,e.EmployeName from cte s right join employees e  on e.EmployeeId=s.empname where e.DeptId=51 and e.status=1  group by EmployeeId,EmployeName,dept  ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void monthwiseleadcount()
        {
            string date = Request.Params["Date"].ToString();
            DataSet ds = DataQueries.SelectCommon1("select count(dept) as count  from seoservicesleads where dept='51' and year(seodate)=year('" + date + "') and month(seodate)=month('" + date + "')");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["count"].ToString().Trim();
                Response.Write("{\"error\": false ,\"count\":\"" + id + "\"}");

            }
        }

        protected void department1()
        {
            DataSet ds = DataQueries.SelectCommon("select Department as dept from Department where DeptId in(14,15,16,18,21,20,34,41,43,44,46)");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"departments\": " + JSONresult + "}";
            Response.Write(result);
        }
        protected void employees1()
        {
            string name = Request.Params["dname"].ToString();
            DataSet ds = DataQueries.SelectCommon("select e.EmployeName as name  from Employees e  inner join Department d on d.deptid=e.deptid where d.Department='" + name + "' and status='1'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"employees\": " + JSONresult + "}";
            Response.Write(result);
        }

        private void test(Notification notification)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from employees where employeeid =' 1 '");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }
        private void testtt(Notification notification)
        {
            DataSet tokens = DataQueries.SelectCommon1("select * from employees where employeeid =' 1 '");

            //Empty array
            string[] arrvalues = new string[tokens.Tables[0].Rows.Count];

            //loopcounter
            for (int loopcounter = 0; loopcounter < tokens.Tables[0].Rows.Count; loopcounter++)
            {
                //assign dataset values to array
                arrvalues[loopcounter] = tokens.Tables[0].Rows[loopcounter]["token1"].ToString();
            }
            //return arrvalues;

            Message msg = new Message();
            msg.sendNotification(arrvalues, notification);
        }

        public void tes()
        {
            Notification notification1 = new Notification();
            notification1.title = "Added a New Lead";
            notification1.body = "";
            notification1.code = 13;
            test(notification1);
            testtt(notification1);
        }
        public void Testmail()
        {
            string name = Request.Params["name"].ToString();
            string mail = Request.Params["mail"].ToString();
            DataSet ds = DataQueries.SelectCommon1("inser into testemail(name,email) values('" + name + "','" + mail + "')");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);

            //Response.Write(result);


        }
        public void sendimages()
        {
            DataSet ds = DataQueries.SelectCommon1("select  top 10 Name,Mobile,Email,designation,Photo from visitors");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


    }
}


